package com.cwt;

import android.util.Log;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;

import java.util.Arrays;

/**
 * Created by Fariz Alemuda on 04-Sep-16.
 */
public class FeatureExtraction {
    public double[][] rawData;
    protected int fftFreqSamplingInput;
    private static int indexAx = 0;
    private static int indexAy = 1;
    private static int indexAz = 2;
    private static int indexGx = 3;
    private static int indexGy = 4;
    private static int indexGz = 5;

    public FeatureExtraction(){
        rawData = null;
    }

    public FeatureExtraction(double[][] koalaData){
        rawData = new double[koalaData.length][koalaData[0].length];
        rawData = koalaData;
    }

    public void setData(double[][] koalaData) {
        rawData = new double[koalaData.length][koalaData[0].length];
        rawData = koalaData;
        //Log.d("AHOY",String.format("%d %d",koalaData.length,koalaData[0].length));
    }

    public void setFFTSamplingFreq(int fftFreqSampling){
        fftFreqSamplingInput = fftFreqSampling;
    }

    private void checkObject(){
        if(rawData==null){
            throw new NullPointerException("No Data! Please call the setData(double[][] input) instead");
        }
    }

    // MATH OPERATIONS ARE HERE

    public double getMean(double[] input) {
        double mean;
        double sum = 0;
        int n = input.length;

        for (int i = 0; i < n; i++) {
            sum = sum + input[i];
        }
        mean = sum / n;
        return mean;
    }

    public double[] getMean2D(){
        checkObject();
        double [] output = getMean2D(rawData);
        return output;
    }

    public double[] getMean2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];

        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getMean(temp2);
        }
        return output;
    }

    public double getVariance(double[] input) {

        double variance = 0;
        double mean = getMean(input);
        double delta = 0;

        int n = input.length;

        for (int i = 0; i < n; i++) {
            delta = input[i] - mean;
            variance = variance + (delta * delta);
        }

        variance = variance / (n - 1);
        return variance;
    }

    public double[] getVariance2D(){
        checkObject();
        double output[] = getVariance2D(rawData);
        return output;
    }

    public double[] getVariance2D(double[][] input){

        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getVariance(temp2);
        }
        return output;
    }

    public double getVariance2(double[] input) {

        double variance = 0;
        double mean = getMean(input);
        double delta = 0;

        int n = input.length;

        for (int i = 0; i < n; i++) {
            delta = input[i] - mean;
            variance = variance + (delta * delta);
        }

        variance = variance / n;
        return variance;
    }

    public double getCovariance(double[] input1, double[] input2) {

        double cov = 0;
        double mean1 = getMean(input1);
        double mean2 = getMean(input2);

        if(input1.length!=input2.length){
            throw new IllegalArgumentException("Two input matrices should have same dimension");
        }

        for (int i = 0; i < input1.length; i++) {
            cov += (input1[i] - mean1) * (input2[i] - mean2);
        }

        return cov / (input1.length - 1);
    }
    public double[]  getCovariance2D(){
        checkObject();
        double[] output = new double[6];
        output[0] = getCovariance(rawData[indexAx],rawData[indexAy]);
        output[1] = getCovariance(rawData[indexAx],rawData[indexAz]);
        output[2] = getCovariance(rawData[indexAy],rawData[indexAz]);
        output[3] = getCovariance(rawData[indexGx],rawData[indexGy]);
        output[4] = getCovariance(rawData[indexGx],rawData[indexGz]);
        output[5] = getCovariance(rawData[indexGy],rawData[indexGz]);

        return output;
    }

    public double getCorrelation(double[] input1, double[] input2){

        double cov = getCovariance(input1,input2);
        double standardDeviation1 = getStandardDeviation(input1);
        double standardDeviation2 = getStandardDeviation(input2);

        if(input1.length!=input2.length){
            throw new IllegalArgumentException("Two input matrices should have same dimension");
        }

        double correl = cov / (standardDeviation1 * standardDeviation2);

        return correl;
    }

    public double[] getCorrelation2D(){
        checkObject();
        double[] output = new double[6];
        output[0] = getCorrelation(rawData[indexAx],rawData[indexAz]);
        output[1] = getCorrelation(rawData[indexAx],rawData[indexAz]);
        output[2] = getCorrelation(rawData[indexAy],rawData[indexAz]);
        output[3] = getCorrelation(rawData[indexGx],rawData[indexGy]);
        output[4] = getCorrelation(rawData[indexGx],rawData[indexGz]);
        output[5] = getCorrelation(rawData[indexGy],rawData[indexGz]);
        return output;
    }

    public double getZeroCrossingRate(double[] input) {

        double zcr = 0;

        for (int i = 0; i < (input.length - 1); i++) {
            if ((input[i] * input[i + 1]) < 0)
                zcr++;
        }
        zcr = zcr / input.length;
        return zcr;
    }

    public double[] getZeroCrossingRate2D(){
        checkObject();
        double[] output = getZeroCrossingRate2D(rawData);
        return output;
    }

    public double[] getZeroCrossingRate2D(double[][] input){

        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;
        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);
        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getZeroCrossingRate(temp2);
        }
        return output;
    }

    public double getStandardDeviation(double[] input) {

        double standardDeviation = 0;
        double variance = getVariance(input);

        standardDeviation = Math.sqrt(variance);
        return standardDeviation;
    }

    public double[] getStandardDeviation2D(){
        checkObject();
        double[] output = getStandardDeviation2D(rawData);
        return output;
    }

    public double[] getStandardDeviation2D(double[][] input){

        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getStandardDeviation(temp2);
        }
        return output;
    }

    public double getStandardDeviation2(double[] input) {

        double standardDeviation = 0;
        double variance = getVariance2(input);

        standardDeviation = Math.sqrt(variance);
        return standardDeviation;
    }

    public double getRootMeanSquare(double[] input) {

        double rms = 0;

        for (int i = 0; i < input.length; i++) {
            rms += Math.pow(input[i], 2);
        }

        return (double)Math.pow(rms/input.length, 0.5);
    }

    public double[] getRootMeanSquare2D(){
        checkObject();
        double[] output = getRootMeanSquare2D(rawData);
        return output;
    }

    public double[] getRootMeanSquare2D(double[][] input){

        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getRootMeanSquare(temp2);
        }
        return output;
    }

    public double getSkewness(double[] input) {

        double skew = 0;
        double mean = getMean(input);
        double std = getStandardDeviation2(input);

        for (int i = 0; i < input.length; i++) {
            skew = skew + Math.pow(input[i] - mean, 3);
        }

        skew = skew / (input.length * Math.pow(std,3));
        return skew;
    }

    public double[] getSkewness2D(){
        checkObject();
        double[] output = getSkewness2D(rawData);
        return output;
    }

    public double[] getSkewness2D(double[][] input){

        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getSkewness(temp2);
        }
        return output;
    }

    public double getKurtosis(double[] input) {

        double kurt = 0;
        double mean = getMean(input);
        double std = getStandardDeviation2(input);

        for (int i = 0; i < input.length; i++) {
            kurt = kurt + Math.pow(input[i] - mean, 4);
        }

        kurt = kurt / (input.length * Math.pow(std,4));

        return kurt;
    }

    public double[] getKurtosis2D(){
        checkObject();
        double[] output = getKurtosis2D(rawData);
        return output;
    }

    public double[] getKurtosis2D(double[][] input){

        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getKurtosis(temp2);
        }
        return output;
    }

    public double getMeanAbsDev(double[] input) {

        double mad = 0;
        double mean = getMean(input);

        for (int i = 0; i < input.length; i++) {
            mad += Math.abs(input[i] - mean);
        }

        return mad/input.length;
    }

    public double[] getMeanAbsDev2D(){
        checkObject();
        double[] output = getMeanAbsDev2D(rawData);
        return output;
    }

    public double[] getMeanAbsDev2D(double[][] input){

        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getMeanAbsDev(temp2);
        }
        return output;
    }

    // function if you don't have imaginary vector
    private double[] fft_magnitude(double[] inputReal, boolean DIRECT) {
        // - n is the dimension of the problem
        // - nu is its logarithm in base e
        int n = inputReal.length;
        int nClosest = getClosestPowerofTwo(n);

        // Since, we don't have imagener, just generate the empty vector
        double[] inputImag = new double[nClosest];

        // If n is a power of 2, then ld is an integer (_without_ decimals)
        double ld = Math.log(nClosest) / Math.log(2.0);

        // Here I check if n is a power of 2. If exist decimals in ld, I quit
        // from the function returning null.
        if (((int) ld) - ld != 0) {
            System.out.println("The number of elements is not a power of 2.");
            return null;
        }

        // Declaration and initialization of the variables
        // ld should be an integer, actually, so I don't lose any information in
        // the cast
        int nu = (int) ld;
        int n2 = nClosest / 2;
        int nu1 = nu - 1;
        double[] xReal = new double[nClosest];
        double[] xImag = new double[nClosest];
        double tReal, tImag, p, arg, c, s;

        // Here I check if I'm going to do the direct transform or the inverse
        // transform.
        double constant;
        if (DIRECT)
            constant = -2 * Math.PI;
        else
            constant = 2 * Math.PI;

        // I don't want to overwrite the input arrays, so here I copy them. This
        // choice adds \Theta(2n) to the complexity.
        for (int i = 0; i < nClosest; i++) {
            xReal[i] = inputReal[i];
            xImag[i] = inputImag[i];
        }

        // First phase - calculation
        int k = 0;
        for (int l = 1; l <= nu; l++) {
            while (k < nClosest) {
                for (int i = 1; i <= n2; i++) {
                    p = bitreverseReference(k >> nu1, nu);
                    // direct FFT or inverse FFT
                    arg = constant * p / nClosest;
                    c = Math.cos(arg);
                    s = Math.sin(arg);
                    tReal = xReal[k + n2] * c + xImag[k + n2] * s;
                    tImag = xImag[k + n2] * c - xReal[k + n2] * s;
                    xReal[k + n2] = xReal[k] - tReal;
                    xImag[k + n2] = xImag[k] - tImag;
                    xReal[k] += tReal;
                    xImag[k] += tImag;
                    k++;
                }
                k += n2;
            }
            k = 0;
            nu1--;
            n2 /= 2;
        }

        // Second phase - recombination
        k = 0;
        int r;
        while (k < nClosest) {
            r = bitreverseReference(k, nu);
            if (r > k) {
                tReal = xReal[k];
                tImag = xImag[k];
                xReal[k] = xReal[r];
                xImag[k] = xImag[r];
                xReal[r] = tReal;
                xImag[r] = tImag;
            }
            k++;
        }

        // Here I have to mix xReal and xImag to have an array (yes, it should
        // be possible to do this stuff in the earlier parts of the code, but
        // it's here to readibility).
        double[] newArray = new double[xReal.length];
        double radice = 1 / Math.sqrt(nClosest);
        for (int i = 0; i < newArray.length; i++) {

            // I Modified Original Program so that it will only produce fft magnitude array.
            newArray[i] = (double)Math.pow((Math.pow(xReal[i] * radice, 2) + Math.pow(xImag[i] * radice, 2)),0.5);
        }
        return newArray;
    }

    // function if you have imaginary vector
    private double[] fft_magnitude(double[] inputReal, double[] inputImag, boolean DIRECT) {
        // - n is the dimension of the problem
        // - nu is its logarithm in base e
        int n = inputReal.length;
        int nClosest = getClosestPowerofTwo(n);

        // If n is a power of 2, then ld is an integer (_without_ decimals)
        double ld = Math.log(nClosest) / Math.log(2.0);

        // Here I check if n is a power of 2. If exist decimals in ld, I quit
        // from the function returning null.
        if (((int) ld) - ld != 0) {
            System.out.println("The number of elements is not a power of 2.");
            return null;
        }

        // Declaration and initialization of the variables
        // ld should be an integer, actually, so I don't lose any information in
        // the cast
        int nu = (int) ld;
        int n2 = nClosest / 2;
        int nu1 = nu - 1;
        double[] xReal = new double[nClosest];
        double[] xImag = new double[nClosest];
        double tReal, tImag, p, arg, c, s;

        // Here I check if I'm going to do the direct transform or the inverse
        // transform.
        double constant;
        if (DIRECT)
            constant = -2 * Math.PI;
        else
            constant = 2 * Math.PI;

        // I don't want to overwrite the input arrays, so here I copy them. This
        // choice adds \Theta(2n) to the complexity.
        for (int i = 0; i < nClosest; i++) {
            xReal[i] = inputReal[i];
            xImag[i] = inputImag[i];
        }

        // First phase - calculation
        int k = 0;
        for (int l = 1; l <= nu; l++) {
            while (k < nClosest) {
                for (int i = 1; i <= n2; i++) {
                    p = bitreverseReference(k >> nu1, nu);
                    // direct FFT or inverse FFT
                    arg = constant * p / nClosest;
                    c = Math.cos(arg);
                    s = Math.sin(arg);
                    tReal = xReal[k + n2] * c + xImag[k + n2] * s;
                    tImag = xImag[k + n2] * c - xReal[k + n2] * s;
                    xReal[k + n2] = xReal[k] - tReal;
                    xImag[k + n2] = xImag[k] - tImag;
                    xReal[k] += tReal;
                    xImag[k] += tImag;
                    k++;
                }
                k += n2;
            }
            k = 0;
            nu1--;
            n2 /= 2;
        }

        // Second phase - recombination
        k = 0;
        int r;
        while (k < nClosest) {
            r = bitreverseReference(k, nu);
            if (r > k) {
                tReal = xReal[k];
                tImag = xImag[k];
                xReal[k] = xReal[r];
                xImag[k] = xImag[r];
                xReal[r] = tReal;
                xImag[r] = tImag;
            }
            k++;
        }

        // Here I have to mix xReal and xImag to have an array (yes, it should
        // be possible to do this stuff in the earlier parts of the code, but
        // it's here to readibility).
        double[] newArray = new double[xReal.length];
        double radice = 1 / Math.sqrt(nClosest);
        for (int i = 0; i < newArray.length; i++) {

            // I Modified Original Program so that it will only produce fft magnitude array.
            newArray[i] = (double)Math.pow((Math.pow(xReal[i] * radice, 2) + Math.pow(xImag[i] * radice, 2)),0.5);
        }
        return newArray;
    }

    private int bitreverseReference(int j, int nu) {
        int j2;
        int j1 = j;
        int k = 0;
        for (int i = 1; i <= nu; i++) {
            j2 = j1 / 2;
            k = 2 * k + j1 - 2 * j2;
            j1 = j2;
        }
        return k;
    }

    private int getClosestPowerofTwo(int input){

        // loga (b) = c
        // a^c = b
        // b = input

        double cDouble = Math.log10(input)/Math.log10(2);
        int cInt = (int) cDouble;

        int nClosest = (int) Math.pow(2,cInt);

        return nClosest;
    }

    /*public double getFFTMaxFreq(double[] inputReal, double freqSampling) {

        double[] fftVector = fft_magnitude(inputReal,true);

        double max = 0;
        double max_freq = 0;
        int n = inputReal.length;

        // get the closest number
        int nClosest = getClosestPowerofTwo(n);

        double n_data = 2 * nClosest;
        double fft_resolution = freqSampling/n_data;

        //I only check until half spectrum ~5Hz
        for (int i = 0; i < fftVector.length/2; i++) {
            if (fftVector[i] > max) {
                max = fftVector[i];
                max_freq = fft_resolution * i;
            }
        }
        return max_freq;
    }*/

    public double getFFTMaxFreq(double[] inputReal, double freqSampling) {

        double[] fftVector = fft_magnitude(inputReal,true);

        double max = 0;
        double max_freq = 0;
        int n = inputReal.length;

        // get the closest number
        int nClosest = getClosestPowerofTwo(n);

        double n_data = 2 * nClosest;
        double fft_resolution = freqSampling/n_data;

        //I only check until half spectrum ~5Hz
        for (int i = 0; i < fftVector.length/2; i++) {
            if (fftVector[i] > max) {
                max = fftVector[i];
                max_freq = fft_resolution * i;
            }
        }
        return max_freq;
    }

    public double getFFTMaxFreq(double[] inputReal, double[] inputImag, double freqSampling) {

        double[] fftVector = fft_magnitude(inputReal,inputImag,true);

        double max = 0;
        double max_freq = 0;
        int n = inputReal.length;

        // get the closest number
        int nClosest = getClosestPowerofTwo(n);

        double n_data = 2 * nClosest;
        double fft_resolution = freqSampling/n_data;

        //I only check until half spectrum ~5Hz
        for (int i = 0; i < fftVector.length/2; i++) {
            if (fftVector[i] > max) {
                max = fftVector[i];
                max_freq = fft_resolution * i;
            }
        }
        return max_freq;
    }

    public double[] getFFTMaxFreq2D(){
        checkObject();
        double[] output = getFFTMaxFreq2D(rawData, fftFreqSamplingInput);
        return output;
    }

    public double[] getFFTMaxFreq2D(double[][] inputReal, double freqSampling){

        int col = 0;
        int row = 0;

        col = inputReal.length;
        row = inputReal[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(inputReal,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getFFTMaxFreq(temp2,freqSampling);
        }
        return output;
    }

    public double getFFTEnergy(double[] inputReal) {

        double energy = 0;
        double[] fftVector = fft_magnitude(inputReal,true);

        for (int i = 0; i < fftVector.length; i++) {
            energy += Math.pow(fftVector[i], 2);
        }

        return energy/fftVector.length;
    }

    public double getFFTEnergy(double[] inputReal, double[] inputImag) {

        double energy = 0;
        double[] fftVector = fft_magnitude(inputReal,inputImag,true);

        for (int i = 0; i < fftVector.length; i++) {
            energy += Math.pow(fftVector[i], 2);
        }

        return energy/fftVector.length;
    }

    public double[] getFFTEnergy2D(){
        checkObject();
        double[] output = getFFTEnergy2D(rawData);
        return output;
    }

    public double[] getFFTEnergy2D(double[][] inputReal){

        int col = 0;
        int row = 0;

        col = inputReal.length;
        row = inputReal[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(inputReal,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getFFTEnergy(temp2);
        }
        return output;
    }

    public double getFFTEntropy(double[] inputReal) {

        double entropy = 0;
        double[] fftVector = fft_magnitude(inputReal,true);

        for (int i = 0; i < fftVector.length; i++) {
            entropy += Math.log(fftVector[i])*fftVector[i];
        }

        return -entropy;
    }

    public double getFFTEntropy(double[] inputReal, double[] inputImag) {

        double entropy = 0;
        double[] fftVector = fft_magnitude(inputReal,inputImag,true);

        for (int i = 0; i < fftVector.length; i++) {
            entropy += Math.log(fftVector[i])*fftVector[i];
        }

        return -entropy;
    }

    public double[] getFFTEntropy2D(){
        checkObject();
        double[] output = getFFTEntropy2D(rawData);
        return output;
    }

    public double[] getFFTEntropy2D(double[][] inputReal){

        int col = 0;
        int row = 0;

        col = inputReal.length;
        row = inputReal[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(inputReal,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getFFTEntropy(temp2);
        }
        return output;
    }

    public double[] getPoly(int inputOrder, double[] inputX, double[] inputY){

        WeightedObservedPoints obs = new WeightedObservedPoints();
        PolynomialCurveFitter fitter = PolynomialCurveFitter.create(inputOrder);

        double[] output; // = new double[inputOrder+1];

        for(int a = 0; a < 30; a++){
            obs.add(inputX[a],inputY[a]);
        }
        output = fitter.fit(obs.toList());

        return output;
    }

    private int getPermutations(int input){

        int permutation = 1;
        if(input != 0){
            for (int i = 1; i<= input; i++) {
                permutation = permutation * i;
            }
        }
        else{
            throw new IllegalArgumentException("N and K, can not be zero!");
        }
        return permutation;
    }

    private int getCombinations(int n, int k){

        int num = 1;
        int denum = 1;
        int combination;
        if( n !=0 && k != 0 ){
            num = getPermutations(n);
            denum = getPermutations(k) * getPermutations(n-k);

            combination = num / denum;
        }
        else{
            throw new IllegalArgumentException("N and K, can not be zero!");
        }
        return combination;
    }

    private static double[][] reshape(double[][] A, int m, int n) {

        int origM = A.length;
        int origN = A[0].length;
        if(origM*origN != m*n){
            throw new IllegalArgumentException("New matrix must be of same area as matix A");
        }
        double[][] B = new double[m][n];
        double[] A1D = new double[A.length * A[0].length];

        int index = 0;
        for(int i = 0;i<A.length;i++){
            for(int j = 0;j<A[0].length;j++){
                A1D[index++] = A[i][j];
            }
        }

        index = 0;
        for(int i = 0;i<n;i++){
            for(int j = 0;j<m;j++){
                B[j][i] = A1D[index++];
            }

        }
        return B;
    }

    private double errorRate(double[] constants, double[] testTime, double[] testSet) {
        int polyOrder = constants.length - 1;
        int n = testSet.length;
        double[] trainingSet = new double[n];

        double delta = 0;
        double errorRate = 0;
        for (int i = 0; i < n; i++) {
            for (int a = 0; a <= polyOrder; a++) {
                trainingSet[i] = trainingSet[i] + constants[a] * Math.pow(testTime[i], a);
            }
        }

        for (int i = 0; i < n; i++) {
            delta = Math.abs(testSet[i] - trainingSet[i]);
            errorRate = errorRate + Math.pow(delta, 2);
        }

        errorRate = 0.5 * errorRate;
        return errorRate;
    }
}
