/**
 * Thingnergy Android API
 * Copyright (c) 2015 Chun-Ting Ding <thingnergy@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Some open source application is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thingnergy.motiondatademo;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.thingnergy.api.TNGDevices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DevicesListActivity extends Activity implements AdapterView.OnItemClickListener {
    private final static String TAG = DevicesListActivity.class.getSimpleName();

    private ArrayList<TNGDevices> devices;
    private List<Map<String, String>> listItems = new ArrayList<Map<String, String>>();
    private SimpleAdapter adapter;
    private Map<String, String> map = null;
    private ListView listView;
    private String DEVICE_NAME = "name";
    private String DEVICE_ADDRESS = "address";
    private String DEVICE_RSSI = "rssi";
    public static final int RESULT_CODE = 31;
    public final static String EXTRA_DEVICE_ADDRESS = "EXTRA_DEVICE_ADDRESS";
    public final static String EXTRA_DEVICE_NAME = "EXTRA_DEVICE_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_devices_list);

       // getActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Device");
        Log.i(TAG, "setDisplayHomeAsUpEnabled");

        listView = (ListView) findViewById(R.id.listView);

        devices = (ArrayList<TNGDevices>) MainActivity.mDevice;

        for (int i = 0, size = devices.size(); i < size; i++) {
            TNGDevices d = devices.get(i);
            map = new HashMap<String, String>();
            map.put(DEVICE_NAME, d.getDevice().getName());
            map.put(DEVICE_ADDRESS, d.getDevice().getAddress());
            map.put(DEVICE_RSSI, String.valueOf(d.getRssi()));
            listItems.add(map);
        }

        adapter = new SimpleAdapter(getApplicationContext(), listItems,
                R.layout.list_item, new String[] { "name", "address", "rssi" },
                new int[] { R.id.deviceName, R.id.deviceAddr, R.id.deviceDistance });
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_devices_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,
                            int position, long id) {
        HashMap<String, String> hashMap = (HashMap<String, String>) listItems
                .get(position);
        String addr = hashMap.get(DEVICE_ADDRESS);
        String name = hashMap.get(DEVICE_NAME);

        Intent intent2 = new Intent();
        intent2.putExtra(EXTRA_DEVICE_ADDRESS, addr);
        intent2.putExtra(EXTRA_DEVICE_NAME, name);
        setResult(RESULT_CODE, intent2);
        finish();
    }
}
