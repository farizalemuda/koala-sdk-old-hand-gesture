/**
 * Thingnergy Android API
 * Copyright (c) 2015 Chun-Ting Ding <thingnergy@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Some open source application is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.thingnergy.api;

import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for
 * demonstration purposes.
 */
public class TNGGattAttributes {
	private static HashMap<String, String> attributes = new HashMap<String, String>();

    public static String THINGNERGY_MOTNIO_SERVICE_UUID                      = "eb371600-347c-fe94-1600-8295a1e42b09";
    public static String THINGNERGY_MOTION_MEASUREMENT_CHARACTERISTIC_UUID   = "eb371601-347c-fe94-1600-8295a1e42b09";
    public static String THINGNERGY_MOTION_PARAM_CHANGE_CHARACTERISTIC_UUID  = "eb371602-347c-fe94-1600-8295a1e42b09";

	static {
		// Thingnergy Raw Data (Motion) Services.
		attributes.put(THINGNERGY_MOTNIO_SERVICE_UUID,
				"Motion Sensor Raw Data Service");
		// Thingnergy Raw Data (Motion)  Characteristics.
		attributes.put(THINGNERGY_MOTION_MEASUREMENT_CHARACTERISTIC_UUID, "Motion Data Notification");
		attributes.put(THINGNERGY_MOTION_PARAM_CHANGE_CHARACTERISTIC_UUID, "Motion Param Configuration");
	}

	public static String lookup(String uuid, String defaultName) {
		String name = attributes.get(uuid);
		return name == null ? defaultName : name;
	}
}
