package com.cwt;

/**
 * Created by Fariz Alemuda on 04-Sep-16.
 */
public class GestureRecognition {

    private double[] standardDeviationInput;
    private double[] meanInput;
    private double[] varianceInput;
    private double[] zeroCrossingRateInput;
    private double[] rootMeanSquareInput;
    private double[] skewnessInput;
    private double[] kurtosisInput;
    private double[] meanAbsDevInput;
    private double[] fftMaxFreqInput;
    private double[] fftEnergyInput;
    private double[] fftEntropyInput;
    private double[] covarianceInput;
    private double[] correlationInput;
    private double covarianceAxAy;
    private double covarianceAxAz;
    private double covarianceAyAz;
    private double covarianceGxGy;
    private double covarianceGxGz;
    private double covarianceGyGz;
    private double correlationAxAy;
    private double correlationAxAz;
    private double correlationAyAz;
    private double correlationGxGy;
    private double correlationGxGz;
    private double correlationGyGz;

    private static final int indexAx = 0;
    private static final int indexAy = 1;
    private static final int indexAz = 2;
    private static final int indexGx = 3;
    private static final int indexGy = 4;
    private static final int indexGz = 5;
    private static final int indexTotalAcc = 6;
    private static final int indexTotalGyro = 7;
    private static final int indexRoll = 8;
    private static final int indexPitch = 9;

    public final static int LOGISTIC_REGRESSION = 1;
    public final static int DECISION_TREE = 2;

    public void setFeatures(
            double[] standardDeviation,
            double[] mean,
            double[] variance,
            double[] zeroCrossingRate,
            double[] rootMeanSquare,
            double[] skewness,
            double[] kurtosis,
            double[] meanAbsDev,
            double[] fftMaxFreq,
            double[] fftEnergy,
            double[] fftEntropy,
            double[] covariance,
            double[] correlation){
        standardDeviationInput = standardDeviation;
        meanInput = mean;
        varianceInput = variance;
        zeroCrossingRateInput = zeroCrossingRate;
        rootMeanSquareInput = rootMeanSquare;
        skewnessInput = skewness;
        kurtosisInput = kurtosis;
        meanAbsDevInput = meanAbsDev;
        fftMaxFreqInput = fftMaxFreq;
        fftEnergyInput = fftEnergy;
        fftEntropyInput = fftEntropy;
        covarianceInput = covariance;
        correlationInput = correlation;

        covarianceAxAy=covarianceInput[0];
        covarianceAxAz=covarianceInput[1];
        covarianceAyAz=covarianceInput[2];
        covarianceGxGy=covarianceInput[3];
        covarianceGxGz=covarianceInput[4];
        covarianceGyGz=covarianceInput[5];

        correlationAxAy=correlationInput[0];
        correlationAxAz=correlationInput[1];
        correlationAyAz=correlationInput[2];
        correlationGxGy=correlationInput[3];
        correlationGxGz=correlationInput[4];
        correlationGyGz=correlationInput[5];
    }
    public String getGesture(){
        String gesture = getGesture(LOGISTIC_REGRESSION);
        return gesture;
    }
    public String getGesture(int MODE){
        String gesture = "NonGesture";

        if(MODE == DECISION_TREE){
            // Gesture Dependent

            if(standardDeviationInput[indexGx] <= 76.593348 && standardDeviationInput[indexGz] <= 93.237947 &&
                    standardDeviationInput[indexGx] <= 55.425057 && standardDeviationInput[indexGz] <= 75.135865
                    && meanInput[indexAz] <= 0.994662){
                gesture = "NonGesture";
            } else if(standardDeviationInput[indexGx] <= 76.593348 && standardDeviationInput[indexGz] <= 93.237947&&
                    standardDeviationInput[indexGx] > 55.425057 && meanAbsDevInput[indexTotalGyro] <= 69.567196 &&
                    rootMeanSquareInput[indexPitch] > 33.073482&&rootMeanSquareInput[indexAy] > 0.334938){
                gesture = "NonGesture";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] <= 0.387354 &&
                    meanInput[indexGy] <= 12.778261 && covarianceAyAz <= 0.095319 &&
                    skewnessInput[indexPitch] <= 0.153641 && rootMeanSquareInput[indexAz] <= 1.039161 &&
                    covarianceAyAz <= 0.057119 && varianceInput[indexTotalAcc] > 0.020668 &&
                    kurtosisInput[indexPitch] > 1.276395 && meanInput[indexAx] > 0.010176){
                gesture = "NonGesture";
            }
            else if(standardDeviationInput[indexGx] <= 76.593348 && standardDeviationInput[indexGz] > 93.237947 &&
                    skewnessInput[indexAx] <= 0.035834 && rootMeanSquareInput[indexGx] <= 44.444757 &&
                    meanAbsDevInput[indexRoll] > 8.312668 && covarianceAyAz > -0.005807 && correlationAyAz <= 0.655768 &&
                    skewnessInput[indexGz] > -0.502112 && skewnessInput[indexRoll] > 0.068124 &&
                    fftEntropyInput[indexTotalGyro] > -34322.92192&&skewnessInput[indexGx] <= 1.830247&&
                    kurtosisInput[indexGy] <= 5.709443){
                gesture = "Left";
            }else if(standardDeviationInput[indexGx] <= 76.593348 && standardDeviationInput[indexGz] > 93.237947 &&
                    skewnessInput[indexAx] <= 0.035834 && rootMeanSquareInput[indexGx] <= 44.444757 &&
                    meanAbsDevInput[indexRoll] > 8.312668 && covarianceAyAz > -0.005807 && correlationAyAz <= 0.655768 &&
                    skewnessInput[indexGz] > -0.502112 && skewnessInput[indexRoll] > 0.068124 &&
                    fftEntropyInput[indexTotalGyro] > -34322.92192&&skewnessInput[indexGx] <= 1.830247&&
                    kurtosisInput[indexGy] > 5.709443 && skewnessInput[indexRoll] <= 1.401151 &&
                    varianceInput[indexTotalAcc] <= 0.031649 && zeroCrossingRateInput[indexGz] <= 0.077778 ){
                gesture = "Left";
            } else if(standardDeviationInput[indexGx] <= 76.593348&&standardDeviationInput[indexGz] > 93.237947 &&
                    skewnessInput[indexAx] > 0.035834 && varianceInput[indexTotalAcc] <= 0.042708 &&
                    standardDeviationInput[indexRoll] > 17.042407 && meanAbsDevInput[indexTotalGyro] > 64.73226 &&
                    skewnessInput[indexAz] > -1.442599 && kurtosisInput[indexGz] <= 3.899442){
                gesture = "Right";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] <= -14.730157 &&
                    meanAbsDevInput[indexTotalGyro] <= 86.35199 && fftEnergyInput[indexRoll] <= 1707.445906 &&
                    covarianceGxGz > -3889.296797&& correlationAyAz <= 0.722671 &&
                    kurtosisInput[indexGz] <= 5.99867&&rootMeanSquareInput[indexPitch] <= 41.562042 &&
                    skewnessInput[indexGx] <= 0.551435 && skewnessInput[indexTotalGyro] <= 0.862854 &&
                    correlationGxGz <= 0.253764 && zeroCrossingRateInput[indexAy] <= 0.066667 &&
                    kurtosisInput[indexAy] <= 2.783564){
                gesture = "VLR";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] <= -14.730157 &&
                    meanAbsDevInput[indexTotalGyro] > 86.35199 && skewnessInput[indexAy] > 0.648244 &&
                    fftEntropyInput[indexTotalGyro] <= -5133.576737 && zeroCrossingRateInput[indexGx] > 0.011111 &&
                    kurtosisInput[indexGx] <= 3.917665){
                gesture = "CounterClockwise";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] <= 12.26912 && skewnessInput[indexAy] > 0.304717 &&
                    meanInput[indexGy] <= 5.982971 && skewnessInput[indexAz] <= 0.490973 &&
                    correlationGxGz <= -0.494222 && standardDeviationInput[indexGz] > 14.368733&&
                    fftEntropyInput[indexGz] > -15245.9792&&meanAbsDevInput[indexAz] > 0.148751){
                gesture = "Down";
            }else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] <= 0.387354 &&
                    meanInput[indexGy] <= 12.778261 && covarianceAyAz <= 0.095319 &&
                    skewnessInput[indexPitch] > 0.153641 && meanInput[indexGy] > -0.425975 &&
                    meanAbsDevInput[indexPitch] > 16.396075 && zeroCrossingRateInput[indexAz] <= 0.005556 &&
                    meanInput[indexTotalGyro] > 74.217028 && zeroCrossingRateInput[indexGy] <= 0.15 &&
                    meanAbsDevInput[indexAz] > 0.197641	){
                gesture = "VRL";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] <= 0.387354 &&
                    meanInput[indexGy] <= 12.778261 && covarianceAyAz > 0.095319 &&
                    standardDeviationInput[indexAy] <= 0.512012 && rootMeanSquareInput[indexAy] > 0.342188 &&
                    covarianceGxGz <= -5834.471216 && skewnessInput[indexGy] > -0.436775 &&
                    zeroCrossingRateInput[indexGy] <= 0.094444 && meanAbsDevInput[indexPitch] <= 37.723173 &&
                    meanInput[indexAx] > 0.041268){
                gesture = "CrossClockwise";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] <= 0.387354 &&
                    meanInput[indexGy] <= 12.778261 && covarianceAyAz > 0.095319 &&
                    standardDeviationInput[indexAy] > 0.512012 && rootMeanSquareInput[indexPitch] <= 51.915827 &&
                    zeroCrossingRateInput[indexAy] <= 0.027778 && rootMeanSquareInput[indexGx] > 107.517849 &&
                    covarianceGxGz <= -1053.363513 && correlationGxGz <= -0.567152 &&
                    meanInput[indexRoll] <= 9.438284 && standardDeviationInput[indexGx] > 109.138161 &&
                    meanAbsDevInput[indexAz] > 0.231353){
                gesture = "CrossClockwise";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] <= 0.387354 &&
                    meanInput[indexGy] <= 12.778261 && covarianceAyAz > 0.095319 &&
                    standardDeviationInput[indexAy] <= 0.512012 && rootMeanSquareInput[indexAy] > 0.342188 &&
                    covarianceGxGz > -5834.471216 && skewnessInput[indexPitch] <= -0.432247 &&
                    skewnessInput[indexGz] > -0.797763 && skewnessInput[indexGx] <= 0.573135 &&
                    kurtosisInput[indexTotalGyro] <= 2.451218){
                gesture = "CrossCounterClockwise";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] <= 0.387354 &&
                    meanInput[indexGy] > 12.778261 && meanAbsDevInput[indexTotalGyro] <= 80.078069 &&
                    meanAbsDevInput[indexAy] > 0.27035 && rootMeanSquareInput[indexGy] <= 75.166286 &&
                    standardDeviationInput[indexRoll] <= 28.623696 && meanInput[indexGx] > -21.518071 &&
                    correlationGxGz > -0.656297 && meanAbsDevInput[indexAz] > 0.192157 &&
                    standardDeviationInput[indexPitch] > 23.308224 && zeroCrossingRateInput[indexGy] <= 0.1	){
                gesture = "VRL";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] <= 0.387354 &&
                    meanInput[indexGy] > 12.778261 && kurtosisInput[indexRoll] > 3.580275 &&
                    correlationGyGz <= 0.372149 && fftEnergyInput[indexGx] > 1244.770324 &&
                    meanAbsDevInput[indexAz] > 0.247296 && rootMeanSquareInput[indexGz] > 87.972084 &&
                    covarianceGyGz > -4510.612434 && skewnessInput[indexTotalGyro] <= 0.541768){
                gesture = "Clockwise";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] > 0.387354 &&
                    kurtosisInput[indexTotalAcc] > 2.409843 && varianceInput[indexAy] <= 0.182693 &&
                    kurtosisInput[indexTotalAcc] <= 4.067612 && standardDeviationInput[indexTotalAcc] > 0.341643){
                gesture = "Up";
            } else if(standardDeviationInput[indexGx] > 76.593348 && meanInput[indexGy] > -14.730157 &&
                    standardDeviationInput[indexRoll] > 12.26912 && varianceInput[indexAz] > 0.387354 &&
                    kurtosisInput[indexTotalAcc] > 2.409843 && varianceInput[indexAy] <= 0.182693 &&
                    kurtosisInput[indexTotalAcc] > 4.067612 && fftEnergyInput[indexPitch] <= 3608.328235 &&
                    fftEnergyInput[indexTotalAcc] > 1.080474){
                gesture = "Up";
            }

            //Gesture Independent

            if(meanAbsDevInput[indexGx] <= 35.375688 && kurtosisInput[indexTotalGyro] <= 2.171149 &&
                    fftEnergyInput[indexAz] > 0.64605 && rootMeanSquareInput[indexGz] > 49.212087 &&
                    skewnessInput[indexRoll] <= 0.040385 && standardDeviationInput[indexAz] > 0.025823 &&
                    standardDeviationInput[indexTotalGyro] <= 97.927618 && rootMeanSquareInput[indexGx] <= 43.522962 &&
                    skewnessInput[indexGz] > -0.67981 && skewnessInput[indexAz] <= 1.20665 &&
                    skewnessInput[indexAx] > 0.149457 && kurtosisInput[indexGx] <= 4.271051 &&
                    meanInput[indexAx] > -0.250845 ){
                gesture = "Right"; //Right (1105.0/4.0)
            } else if(meanAbsDevInput[indexGx] <= 35.375688 && kurtosisInput[indexTotalGyro] <= 2.171149 &&
                    fftEnergyInput[indexAz] > 0.64605 && rootMeanSquareInput[indexGz] > 49.212087 &&
                    skewnessInput[indexRoll] > 0.040385 && fftEnergyInput[indexGy] <= 4313.999938 &&
                    meanInput[indexPitch] > -1.926303 && meanInput[indexAy] <= 0.272602 &&
                    skewnessInput[indexAx] <= -0.15364 && meanInput[indexAx] <= 0.353453 &&
                    meanAbsDevInput[indexRoll] > 7.317385 ){
                gesture = "Left"; //Left (845.0/1.0)
            } else if(meanAbsDevInput[indexGx] <= 35.375688 && kurtosisInput[indexTotalGyro] > 2.171149 &&
                    fftEnergyInput[indexTotalAcc] > 0.891119 && fftMaxFreqInput[indexGy] <= 4.6875 &&
                    rootMeanSquareInput[indexGz] <= 76.644181 && zeroCrossingRateInput[indexGz] <= 0.216667 &&
                    meanInput[indexGx] <= 14.123705 && fftMaxFreqInput[indexRoll] <= 0 &&
                    fftEntropyInput[indexAy] > -17.621002 && meanInput[indexGx] > -20.845922 &&
                    meanInput[indexGz] <= 16.882239 && rootMeanSquareInput[indexAz] <= 0.970086 &&
                    correlationGxGy <= 0.85822 && meanInput[indexTotalAcc] > 0.940287 &&
                    meanInput[indexGz] <= -16.869058 && meanInput[indexAz] <= 0.925257){
                gesture ="NonGesture"; //NonGesture (207.0)
            } else if(meanAbsDevInput[indexGx] <= 35.375688 && kurtosisInput[indexTotalGyro] > 2.171149 &&
                    fftEnergyInput[indexTotalAcc] > 0.891119 && fftMaxFreqInput[indexGy] <= 4.6875 &&
                    rootMeanSquareInput[indexGz] <= 76.644181 && zeroCrossingRateInput[indexGz] <= 0.216667 &&
                    meanInput[indexGx] <= 14.123705 && fftMaxFreqInput[indexRoll] <= 0 &&
                    fftEntropyInput[indexAy] > -17.621002 && meanInput[indexGx] > -20.845922 &&
                    meanInput[indexGz] <= 16.882239 && rootMeanSquareInput[indexAz] <= 0.970086 &&
                    correlationGxGy <= 0.85822 && meanInput[indexTotalAcc] > 0.940287 &&
                    meanInput[indexGz] > -16.869058 && meanInput[indexGx] <= 5.606503){
                gesture ="NonGesture"; //NonGesture (3760.97)
            } else if(meanAbsDevInput[indexGx] <= 35.375688 && kurtosisInput[indexTotalGyro] > 2.171149 &&
                    fftEnergyInput[indexTotalAcc] > 0.891119 && fftMaxFreqInput[indexGy] <= 4.6875 &&
                    rootMeanSquareInput[indexGz] <= 76.644181 && zeroCrossingRateInput[indexGz] <= 0.216667 &&
                    meanInput[indexGx] <= 14.123705 && fftMaxFreqInput[indexRoll] > 0 &&
                    meanInput[indexPitch] <= 2.973228 && covarianceAyAz <= 0.047554 &&
                    meanInput[indexAx] > -0.084973){
                gesture = "NonGesture"; // NonGesture (267.0/1.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] <= -43.967458 &&
                    correlationAxAy <= -0.780643 && skewnessInput[indexAz] <= 0.078268 &&
                    zeroCrossingRateInput[indexAx] <= 0.033333 && fftMaxFreqInput[indexGx] > 0	){
                gesture = "Right"; //Right (252.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] <= -43.967458 &&
                    correlationAxAy <= -0.780643 && skewnessInput[indexAz] > 0.078268 &&
                    kurtosisInput[indexGz] <= 3.604646 && standardDeviationInput[indexPitch] > 27.561641 &&
                    covarianceAxAy <= -0.052143 && rootMeanSquareInput[indexTotalAcc] > 1.10911){
                gesture = "Left"; //Left (324.0/4.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] <= -43.967458 &&
                    correlationAxAy > -0.780643 && meanInput[indexGy] > -90.600035 &&
                    fftMaxFreqInput[indexRoll] <= 0 && covarianceAxAy <= 0.137516 &&
                    standardDeviationInput[indexGz] > 18.114411 && fftEnergyInput[indexAx] > 0.061899 &&
                    zeroCrossingRateInput[indexGx] > 0.016667 && meanInput[indexTotalAcc] <= 1.455671 &&
                    meanInput[indexAz] <= 0.555444	){
                gesture = "NonGesture"; //NonGesture (404.0/3.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] <= 0.036361 && varianceInput[indexAy] <= 0.23042 &&
                    skewnessInput[indexAy] <= 0.442824 && meanAbsDevInput[indexGz] <= 44.45629 &&
                    skewnessInput[indexAz] <= 0.485034 && rootMeanSquareInput[indexAx] <= 0.417304 &&
                    skewnessInput[indexGx] <= 0.834848 && fftEnergyInput[indexGx] > 497.936885 &&
                    kurtosisInput[indexGx] <= 3.256786 && skewnessInput[indexPitch] <= -1.417126 &&
                    correlationGxGz > -0.968614 && fftEntropyInput[indexTotalGyro] > -26051.86424 &&
                    standardDeviationInput[indexAz] <= 0.707874 && rootMeanSquareInput[indexAz] > 0.891787 &&
                    correlationGyGz > -0.733358	){
                gesture = "Up"; //Up (227.0/1.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] <= 0.036361 && varianceInput[indexAy] <= 0.23042 &&
                    skewnessInput[indexAy] <= 0.442824 &&  meanAbsDevInput[indexGz] <= 44.45629 &&
                    skewnessInput[indexAz] <= 0.485034 && rootMeanSquareInput[indexAx] <= 0.417304 &&
                    skewnessInput[indexGx] <= 0.834848 && fftEnergyInput[indexGx] > 497.936885 &&
                    kurtosisInput[indexGx] <= 3.256786 && skewnessInput[indexPitch] > -1.417126 &&
                    zeroCrossingRateInput[indexAy] <= 0.094444 && meanInput[indexGx] <= 35.369661	){
                gesture = "Up"; //Up (1587.0/2.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] <= 0.036361 && varianceInput[indexAy] <= 0.23042 &&
                    skewnessInput[indexAy] > 0.442824 && meanInput[indexGy] > 0.537745 &&
                    rootMeanSquareInput[indexAy] <= 0.415532 && kurtosisInput[indexRoll] > 4.045553 &&
                    correlationAyAz > -0.225607 && standardDeviationInput[indexTotalGyro] > 50.450801 &&
                    zeroCrossingRateInput[indexGz] <= 0.116667 && zeroCrossingRateInput[indexGy] > 0.094444){
                gesture = "Clockwise"; //Clockwise (341.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] <= 0.036361 && varianceInput[indexAy] > 0.23042 &&
                    meanInput[indexGy] <= -8.135605 && correlationGyGz <= 0.569084 &&
                    covarianceAyAz > 0.090794 && standardDeviationInput[indexGy] <= 75.132159 &&
                    meanInput[indexAx] <= 0.367881 && zeroCrossingRateInput[indexGy] > 0.044444 &&
                    meanInput[indexGz] <= 19.533115 && meanInput[indexGy] <= -9.310404	){
                gesture ="VLR"; //VLR (333.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] <= 0.036361 && varianceInput[indexAy] > 0.23042 &&
                    meanInput[indexGy] > -8.135605 && standardDeviationInput[indexAz] > 0.344931 &&
                    standardDeviationInput[indexTotalGyro] <= 95.710318 && meanAbsDevInput[indexRoll] <= 54.083085 &&
                    skewnessInput[indexGz] > -1.175854 && rootMeanSquareInput[indexAy] <= 0.75434 &&
                    standardDeviationInput[indexGz] > 32.028992 && fftEnergyInput[indexGz] <= 9222.249181 &&
                    correlationGyGz > -0.332567 && skewnessInput[indexRoll] <= 1.198784 &&
                    meanAbsDevInput[indexAy] > 0.391433 && zeroCrossingRateInput[indexAy] <= 0.05	){
                gesture = "VRL"; //VRL (367.0/8.0)
            } else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz <= -0.776238 &&
                    rootMeanSquareInput[indexAx] <= 0.384231 && kurtosisInput[indexPitch] <= 4.184314 &&
                    skewnessInput[indexTotalGyro] > -0.045624 && skewnessInput[indexPitch] > -0.543533 &&
                    correlationAyAz <= 0.288874 && kurtosisInput[indexTotalGyro] <= 2.676791 &&
                    meanInput[indexAz] <= 0.840814 && zeroCrossingRateInput[indexGz] > 0.016667 &&
                    kurtosisInput[indexGy] > 2.880799 && meanInput[indexAy] > 0.225683	){
                gesture = "Down"; //Down (1500.0/7.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] <= -6.353378 && rootMeanSquareInput[indexGy] <= 124.73523 &&
                    skewnessInput[indexPitch] <= -0.168498 && meanAbsDevInput[indexAy] <= 0.31634 &&
                    skewnessInput[indexAz] <= 0.86791 && meanAbsDevInput[indexTotalGyro] > 46.463689 &&
                    kurtosisInput[indexAz] <= 6.749307 && meanInput[indexAx] > -0.229616 &&
                    meanInput[indexGy] <= -10.563744 && meanInput[indexAz] > 0.489718 &&
                    standardDeviationInput[indexGz] > 46.16576 && covarianceGxGy > -642.814622 &&
                    zeroCrossingRateInput[indexAx] > 0.016667 && zeroCrossingRateInput[indexAy] <= 0.033333	){
                gesture = "CounterClockwise"; //CounterClockwise (431.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz <= -0.776238 &&
                    rootMeanSquareInput[indexAx] <= 0.384231 && kurtosisInput[indexPitch] <= 4.184314 &&
                    skewnessInput[indexTotalGyro] > -0.045624 && skewnessInput[indexPitch] > -0.543533 &&
                    correlationAyAz <= 0.288874 && kurtosisInput[indexTotalGyro] <= 2.676791 &&
                    meanInput[indexAz] <= 0.840814 && zeroCrossingRateInput[indexGz] > 0.016667 &&
                    kurtosisInput[indexGy] > 2.880799 && meanInput[indexAy] > 0.225683	){
                gesture = "Down"; //Down (1500.0/7.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] <= -6.353378 && rootMeanSquareInput[indexGy] <= 124.73523 &&
                    skewnessInput[indexPitch] <= -0.168498 && meanAbsDevInput[indexAy] <= 0.31634 &&
                    skewnessInput[indexAz] <= 0.86791 && meanAbsDevInput[indexTotalGyro] > 46.463689 &&
                    kurtosisInput[indexAz] <= 6.749307 && meanInput[indexAx] > -0.229616 &&
                    meanInput[indexGy] <= -10.563744 && meanInput[indexAz] > 0.489718 &&
                    standardDeviationInput[indexGz] > 46.16576 && covarianceGxGy > -642.814622 &&
                    zeroCrossingRateInput[indexAx] > 0.016667 && zeroCrossingRateInput[indexAy] <= 0.033333){
                gesture = "CounterClockwise"; //CounterClockwise (431.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] <= -6.353378 && rootMeanSquareInput[indexGy] <= 124.73523 &&
                    skewnessInput[indexPitch] <= -0.168498 && meanAbsDevInput[indexAy] > 0.31634 &&
                    correlationGxGy <= -0.590209 && standardDeviationInput[indexRoll] > 54.76169 &&
                    meanInput[indexPitch] > 11.310287){
                gesture = "CounterClockwise"; //CounterClockwise (287.0/1.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] <= -6.353378 && rootMeanSquareInput[indexGy] <= 124.73523 &&
                    skewnessInput[indexPitch] <= -0.168498 && meanAbsDevInput[indexAy] > 0.31634 &&
                    correlationGxGy > -0.590209 && meanInput[indexGy] > -55.254491 &&
                    correlationGyGz <= 0.592113 && rootMeanSquareInput[indexGz] > 38.17135 &&
                    correlationAyAz > 0.35789 && skewnessInput[indexAx] <= 0.676137 &&
                    covarianceGyGz > -1165.583843 && kurtosisInput[indexGx] <= 3.200205 &&
                    skewnessInput[indexGx] > -0.369595 && standardDeviationInput[indexAz] <= 0.804208 &&
                    standardDeviationInput[indexAy] <= 0.693513 && skewnessInput[indexRoll] > -0.107734 &&
                    standardDeviationInput[indexTotalAcc] > 0.315317 && zeroCrossingRateInput[indexGy] > 0.066667 &&
                    fftEnergyInput[indexTotalAcc] <= 2.535438 && fftEnergyInput[indexPitch] > 711.834279 &&
                    fftEntropyInput[indexGy] > -23892.7532 && correlationGxGy > -0.518136 &&
                    kurtosisInput[indexGz] <= 4.959288 && kurtosisInput[indexTotalAcc] <= 3.484965 &&
                    zeroCrossingRateInput[indexGx] <= 0.061111 && skewnessInput[indexGz] > -0.944493){
                gesture = "CrossCounterClockwise"; //CrossCounterClockwise (809.0/5.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] <= -6.353378 && rootMeanSquareInput[indexGy] <= 124.73523 &&
                    skewnessInput[indexPitch] > -0.168498 && rootMeanSquareInput[indexGz] > 92.239225 &&
                    varianceInput[indexAz] <= 0.263052 && standardDeviationInput[indexGx] > 90.060883 &&
                    meanInput[indexAx] <= 0.184298 && skewnessInput[indexAx] <= 0.492806 &&
                    covarianceAxAy <= 0.028291 && fftEnergyInput[indexAy] > 0.064878 &&
                    zeroCrossingRateInput[indexAy] <= 0.016667){
                gesture = "CounterClockwise"; //CounterClockwise (242.0/1.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] <= -6.353378 && rootMeanSquareInput[indexGy] > 124.73523	){
                gesture = "Clockwise"; //Clockwise (189.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] <= 29.693625 && meanInput[indexGz] <= 6.767273 &&
                    fftEnergyInput[indexAz] <= 0.881486 && meanInput[indexAx] <= 0.189886 &&
                    kurtosisInput[indexTotalGyro] <= 2.742747 && fftEnergyInput[indexTotalAcc] <= 1.730972	){
                gesture = "Down"; //Down (376.0/2.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz <= 0.546707 &&
                    meanInput[indexGy] <= 38.091999 && rootMeanSquareInput[indexAz] > 0.776838 &&
                    meanInput[indexGy] > 1.917606 && kurtosisInput[indexTotalGyro] <= 1.555341 &&
                    skewnessInput[indexGz] > -1.297878 && kurtosisInput[indexAy] > 2.088747 &&
                    zeroCrossingRateInput[indexAz] <= 0.016667 && zeroCrossingRateInput[indexAy] <= 0.061111){
                gesture = "Clockwise"; //Clockwise (234.0/2.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz <= 0.546707 &&
                    meanInput[indexGy] <= 38.091999 && rootMeanSquareInput[indexAz] > 0.776838 &&
                    meanInput[indexGy] > 1.917606 && kurtosisInput[indexTotalGyro] > 1.555341 &&
                    skewnessInput[indexTotalGyro] <= -0.35431 && correlationGxGz <= -0.05258){
                gesture = "Clockwise"; //Clockwise (123.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz <= 0.546707 &&
                    meanInput[indexGy] <= 38.091999 && rootMeanSquareInput[indexAz] > 0.776838 &&
                    meanInput[indexGy] > 1.917606 && kurtosisInput[indexTotalGyro] > 1.555341 &&
                    skewnessInput[indexTotalGyro] > -0.35431 && standardDeviationInput[indexPitch] <= 63.12243 &&
                    meanAbsDevInput[indexTotalGyro] <= 91.740969 && kurtosisInput[indexGx] <= 3.752255 &&
                    skewnessInput[indexAx] <= 0.527361 && meanInput[indexPitch] > 7.902417 &&
                    correlationGxGz <= 0.544859 && zeroCrossingRateInput[indexGz] <= 0.055556 &&
                    covarianceGyGz > -301.451247 && skewnessInput[indexAy] > 0.117653 &&
                    meanInput[indexGz] > -13.498264 && rootMeanSquareInput[indexAz] > 0.84432 &&
                    fftEnergyInput[indexGz] > 649.521149 && skewnessInput[indexTotalAcc] > -0.018205 &&
                    meanInput[indexAy] > 0.276771){
                gesture = "VRL"; //VRL (473.0/1.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz <= 0.546707 &&
                    meanInput[indexGy] > 38.091999 && skewnessInput[indexGz] <= 0.315284 &&
                    fftMaxFreqInput[indexAz] <= 0 && fftMaxFreqInput[indexAx] <= 0 &&
                    rootMeanSquareInput[indexAz] > 0.865255){
                gesture = "CrossClockwise"; //CrossClockwise (254.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz <= 0.546707 &&
                    meanInput[indexGy] > 38.091999 && skewnessInput[indexGz] > 0.315284 &&
                    fftEnergyInput[indexGx] > 4228.232184){
                gesture = "Clockwise"; //Clockwise (322.0/1.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz > 0.546707 &&
                    meanInput[indexAx] <= 0.222607 && correlationAyAz <= 0.902179 &&
                    rootMeanSquareInput[indexAy] > 0.412459 && fftEntropyInput[indexAy] <= 19.434848 &&
                    meanInput[indexAy] > 0.160727 && skewnessInput[indexGx] <= 0.508727 &&
                    meanInput[indexGy] <= -0.525453 && skewnessInput[indexRoll] > 0.926893 &&
                    standardDeviationInput[indexGy] <= 49.267588 && fftEnergyInput[indexAy] > 0.219803 &&
                    rootMeanSquareInput[indexRoll] > 14.839869 && meanInput[indexAx] > -0.203847 &&
                    fftEntropyInput[indexAx] <= 28.259122){
                gesture = "CrossCounterClockwise"; //CrossCounterClockwise (161.0/2.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz > 0.546707 &&
                    meanInput[indexAx] <= 0.222607 && correlationAyAz <= 0.902179 &&
                    rootMeanSquareInput[indexAy] > 0.412459 && fftEntropyInput[indexAy] <= 19.434848 &&
                    meanInput[indexAy] > 0.160727 && skewnessInput[indexGx] <= 0.508727 &&
                    meanInput[indexGy] > -0.525453 && meanAbsDevInput[indexAy] <= 0.734319 &&
                    skewnessInput[indexTotalGyro] > -0.36312 && fftMaxFreqInput[indexGx] <= 0 &&
                    fftMaxFreqInput[indexGx] > 0 && skewnessInput[indexAy] > -0.511869 &&
                    fftEntropyInput[indexRoll] <= -34149.56903 && fftEntropyInput[indexRoll] > -34149.56903 &&
                    zeroCrossingRateInput[indexGx] <= 0.05 && kurtosisInput[indexTotalGyro] > 1.635161 &&
                    meanInput[indexGx] > -30.70666 && correlationGxGy <= 0.053559 &&
                    meanInput[indexGz] > 4.427338){
                gesture = "CrossClockwise"; //CrossClockwise (188.0/1.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz > 0.546707 &&
                    meanInput[indexAx] <= 0.222607 && correlationAyAz <= 0.902179 &&
                    rootMeanSquareInput[indexAy] > 0.412459 && fftEntropyInput[indexAy] <= 19.434848 &&
                    meanInput[indexAy] > 0.160727 &&  skewnessInput[indexGx] <= 0.508727 &&
                    meanInput[indexGy] > -0.525453 && meanAbsDevInput[indexAy] <= 0.734319 &&
                    skewnessInput[indexTotalGyro] > -0.36312 && fftMaxFreqInput[indexGx] <= 0 &&
                    fftMaxFreqInput[indexGx] > 0 && skewnessInput[indexAy] > -0.511869 &&
                    fftEntropyInput[indexRoll] <= -34149.56903 && fftEntropyInput[indexRoll] > -34149.56903 &&
                    zeroCrossingRateInput[indexGx] <= 0.05 && kurtosisInput[indexTotalGyro] > 1.635161 &&
                    meanInput[indexGx] > -30.70666 && correlationGxGy > 0.053559){
                gesture = "CrossClockwise"; //CrossClockwise (788.0/1.0)
            }else if(meanAbsDevInput[indexGx] > 35.375688 && meanInput[indexRoll] > -43.967458 &&
                    meanInput[indexAy] > 0.036361 && correlationGxGz > -0.776238 &&
                    meanInput[indexGy] > -6.353378 && meanInput[indexAy] <= 1.116396 &&
                    rootMeanSquareInput[indexGz] > 29.693625 && correlationAyAz > 0.546707 &&
                    meanInput[indexAx] > 0.222607 && meanAbsDevInput[indexGz] > 45.210181 &&
                    meanAbsDevInput[indexGy] <= 74.426135 && skewnessInput[indexGx] <= 1.031812 &&
                    correlationGyGz <= 0.658676 && fftEnergyInput[indexAz] > 0.437326 &&
                    covarianceGxGy > -5802.698135 && meanInput[indexGz] <= 27.102619 &&
                    fftEnergyInput[indexAy] <= 1.066029 && meanInput[indexRoll] <= -8.919908 &&
                    skewnessInput[indexAz] <= 0.40718 && fftEnergyInput[indexTotalAcc] > 1.171946 &&
                    fftEnergyInput[indexTotalGyro] <= 44110.70128 && meanAbsDevInput[indexTotalAcc] > 0.29192 &&
                    correlationGyGz <= 0.596753	){
                gesture = "Clockwise"; //Clockwise (440.0)
            }

            //Gesture Hybrid Model
            if(meanAbsDevInput[indexGx] <= 40.975142 &&
                    standardDeviationInput[indexGz] <= 90.961075 && kurtosisInput[indexTotalGyro] <= 2.001946 &&
                    varianceInput[indexAz] <= 0.005663 && rootMeanSquareInput[indexGz] > 40.179999 &&
                    standardDeviationInput[indexAz] <= 0.025823 && correlationGyGz > 0.170204){
                gesture = "Left";       //Left (320.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] <= 90.961075 &&
                    kurtosisInput[indexTotalGyro] <= 2.001946 && varianceInput[indexAz] <= 0.005663 &&
                    rootMeanSquareInput[indexGz] > 40.179999 && standardDeviationInput[indexAz] > 0.025823 &&
                    zeroCrossingRateInput[indexGy] > 0.072222 && meanAbsDevInput[indexAy] <= 0.108688 &&
                    skewnessInput[indexAx] > 0.042708 && meanInput[indexGz] > -36.559931 &&
                    zeroCrossingRateInput[indexGx] <= 0.15){
                gesture = "Right";          //Right (299.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] <= 90.961075 &&
                    kurtosisInput[indexTotalGyro] <= 2.001946 && varianceInput[indexAz] > 0.005663 &&
                    meanAbsDevInput[indexGz] > 7.316226 && fftEntropyInput[indexAz] > -5.775179 &&
                    zeroCrossingRateInput[indexGy] <= 0.127778){
                gesture = "NonGesture"; //NonGesture (526.0) &&
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] <= 90.961075 &&
                    kurtosisInput[indexTotalGyro] > 2.001946 && fftEnergyInput[indexTotalAcc] <= 0.887498 &&
                    skewnessInput[indexTotalGyro] > 0.669655 && zeroCrossingRateInput[indexAx] <= 0.194444 &&
                    meanInput[indexPitch] <= 12.431497 && covarianceGxGz <= 1201.988716 &&
                    meanInput[indexGy] <= 14.981291 && standardDeviationInput[indexPitch] <= 27.922705 &&
                    meanAbsDevInput[indexGx] <= 31.709794){
                gesture = "NonGesture";             //NonGesture (289.0/1.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 &&
                    standardDeviationInput[indexGz] <= 90.961075 && kurtosisInput[indexTotalGyro] > 2.001946 &&
                    fftEnergyInput[indexTotalAcc] > 0.887498 && rootMeanSquareInput[indexGx] <= 43.278476 &&
                    meanAbsDevInput[indexGz] <= 30.40336 &&  meanInput[indexAz] <= 0.967737 &&
                    zeroCrossingRateInput[indexAy] <= 0.366667 && meanInput[indexGx] <= 7.392544 &&
                    fftEntropyInput[indexAy] > -17.734042){
                gesture = "NonGesture";             //NonGesture (15772.91/3.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] <= 90.961075 &&
                    kurtosisInput[indexTotalGyro] > 2.001946 && fftEnergyInput[indexTotalAcc] > 0.887498 &&
                    rootMeanSquareInput[indexGx] <= 43.278476 && meanAbsDevInput[indexGz] > 30.40336 &&
                    meanAbsDevInput[indexTotalAcc] > 0.032776 && meanInput[indexAz] <= 0.886723 &&
                    fftEnergyInput[indexRoll] > 104.178312 && meanAbsDevInput[indexGy] > 11.628333){
                gesture = "NonGesture";         //NonGesture (1982.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] <= 0.0212 &&
                    rootMeanSquareInput[indexTotalGyro] <= 166.982355 && varianceInput[indexAz] > 0.001432 &&
                    meanAbsDevInput[indexAx] > 0.1834 && meanAbsDevInput[indexTotalGyro] <= 92.208787 &&
                    skewnessInput[indexAz] <= 1.049234 && kurtosisInput[indexGz] <= 3.672072 &&
                    covarianceAxAy <= 0.013157 && skewnessInput[indexRoll] <= -0.139916 &&
                    zeroCrossingRateInput[indexAx] <= 0.094444 && zeroCrossingRateInput[indexGz] <= 0.066667){
                gesture = "Right";              //Right (1977.0/4.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] <= 0.0212 &&
                    rootMeanSquareInput[indexTotalGyro] <= 166.982355 && varianceInput[indexAz] > 0.001432 &&
                    meanAbsDevInput[indexAx] > 0.1834 && meanAbsDevInput[indexTotalGyro] <= 92.208787 &&
                    skewnessInput[indexAz] <= 1.049234 && kurtosisInput[indexGz] <= 3.672072 &&
                    covarianceAxAy <= 0.013157 && skewnessInput[indexRoll] <= -0.139916 &&
                    zeroCrossingRateInput[indexAx] <= 0.094444 && zeroCrossingRateInput[indexGz] > 0.066667 &&
                    kurtosisInput[indexGx] > 2.189929){
                gesture = "Right";          //Right (104.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] <= 0.0212 &&
                    rootMeanSquareInput[indexTotalGyro] <= 166.982355 && varianceInput[indexAz] > 0.001432 &&
                    meanAbsDevInput[indexAx] > 0.1834 && meanAbsDevInput[indexTotalGyro] <= 92.208787 &&
                    skewnessInput[indexAz] <= 1.049234 && kurtosisInput[indexGz] <= 3.672072 &&
                    covarianceAxAy <= 0.013157 && skewnessInput[indexRoll] > -0.139916 &&
                    zeroCrossingRateInput[indexGz] <= 0.044444 && skewnessInput[indexAx] > -0.057045 &&
                    correlationGyGz <= 0.622726 &&
                    meanInput[indexGx] <= 5.260934){
                gesture = "Right";          //Right (119.0/3.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] > 0.0212 &&
                    covarianceAyAz <= 0.02894 && meanInput[indexGy] <= 4.311795 &&
                    skewnessInput[indexAx] <= -0.135797 && skewnessInput[indexGz] > -0.501983 &&
                    kurtosisInput[indexRoll] <= 2.109689 && meanInput[indexAx] <= 0.35454){
                gesture = "Left";       //Left (142.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] > 0.0212 &&
                    covarianceAyAz <= 0.02894 && meanInput[indexGy] <= 4.311795 &&
                    skewnessInput[indexAx] <= -0.135797 && skewnessInput[indexGz] > -0.501983 &&
                    kurtosisInput[indexRoll] > 2.109689 && kurtosisInput[indexGz] <= 3.098866){
                gesture = "Left";       //Left (1722.0/10.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] > 0.0212 &&
                    covarianceAyAz <= 0.02894 && meanInput[indexGy] <= 4.311795 &&
                    skewnessInput[indexAx] <= -0.135797 && skewnessInput[indexGz] > -0.501983 &&
                    kurtosisInput[indexRoll] > 2.109689 && kurtosisInput[indexGz] > 3.098866 &&
                    fftEnergyInput[indexGz] <= 24675.86406 && skewnessInput[indexAz] <= 1.682604 &&
                    fftMaxFreqInput[indexGz] <= 0 && covarianceAyAz > -0.005807){
                gesture = "Left";       //Left (146.0)
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] > 0.0212 &&
                    covarianceAyAz <= 0.02894 && meanInput[indexGy] <= 4.311795 &&
                    skewnessInput[indexAx] <= -0.135797 && skewnessInput[indexGz] > -0.501983 &&
                    kurtosisInput[indexRoll] > 2.109689 && kurtosisInput[indexGz] > 3.098866 &&
                    fftEnergyInput[indexGz] <= 24675.86406 && skewnessInput[indexAz] <= 1.682604 &&
                    fftMaxFreqInput[indexGz] > 0 && meanAbsDevInput[indexAz] <= 0.058297 &&
                    zeroCrossingRateInput[indexGz] > 0.027778 && varianceInput[indexAy] > 0.014549 &&
                    zeroCrossingRateInput[indexAx] <= 0.022222 && skewnessInput[indexAx] > -1.527938){
                gesture = "Left";       //Left (216.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] > 0.0212 &&
                    covarianceAyAz <= 0.02894 && meanInput[indexGy] <= 4.311795 &&
                    skewnessInput[indexAx] <= -0.135797 && skewnessInput[indexGz] > -0.501983 &&
                    kurtosisInput[indexRoll] > 2.109689 && kurtosisInput[indexGz] > 3.098866 &&
                    fftEnergyInput[indexGz] <= 24675.86406 && skewnessInput[indexAz] <= 1.682604 &&
                    fftMaxFreqInput[indexGz] > 0 && meanAbsDevInput[indexAz] > 0.058297){
                gesture = "Left";      //Left (205.0) &&
            } else if(meanAbsDevInput[indexGx] <= 40.975142 && standardDeviationInput[indexGz] > 90.961075 &&
                    rootMeanSquareInput[indexAz] > 0.790487 && skewnessInput[indexRoll] > 0.0212 &&
                    covarianceAyAz <= 0.02894 && meanInput[indexGy] <= 4.311795 &&
                    skewnessInput[indexAx] > -0.135797 && skewnessInput[indexGz] <= 0.64253 &&
                    zeroCrossingRateInput[indexAz] <= 0 && skewnessInput[indexGx] > -0.238308 &&
                    meanInput[indexPitch] <= 17.8427 && kurtosisInput[indexGx] > 2.363249){
                gesture = "Left";       //Left (177.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] <= 0.502853 && meanInput[indexTotalAcc] > 1.482707){
                gesture = "Clockwise";      //Clockwise (192.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] <= 2.325125 && covarianceGxGz <= -4899.059989 &&
                    correlationAyAz > 0.35966 && covarianceGxGy > -3156.717274 && meanInput[indexPitch] > -12.515987 &&
                    meanAbsDevInput[indexAz] > 0.202757 && fftEnergyInput[indexGz] <= 16802.53141 &&
                    skewnessInput[indexAz] <= 0.903492 && standardDeviationInput[indexPitch] > 30.676786){
                gesture = "CrossClockwise";     //CrossClockwise (197.0/2.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] <= 2.325125 && covarianceGxGz > -4899.059989 &&
                    fftEnergyInput[indexTotalGyro] <= 30305.53282 && skewnessInput[indexPitch] > -0.512851 &&
                    skewnessInput[indexGz] > -1.614944 && correlationGxGz > -0.712949 &&
                    meanAbsDevInput[indexPitch] > 18.512378 && skewnessInput[indexGx] <= 0.416463 &&
                    fftEnergyInput[indexGx] > 1712.606526 && rootMeanSquareInput[indexGz] <= 95.840885 &&
                    rootMeanSquareInput[indexAz] > 0.706754 &&  kurtosisInput[indexGx] <= 3.64155 &&
                    skewnessInput[indexRoll] > -1.255131 && meanInput[indexPitch] <= 26.655934 &&
                    zeroCrossingRateInput[indexAy] > 0.005556 && meanInput[indexPitch] <= 6.591121 &&
                    zeroCrossingRateInput[indexAy] <= 0.055556 && zeroCrossingRateInput[indexGz] <= 0.027778){
                gesture = "VLR";        //VLR (500.0/2.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] <= 2.325125 && covarianceGxGz > -4899.059989 &&
                    fftEnergyInput[indexTotalGyro] <= 30305.53282 && skewnessInput[indexPitch] > -0.512851 &&
                    skewnessInput[indexGz] > -1.614944 && correlationGxGz > -0.712949 &&
                    meanAbsDevInput[indexPitch] > 18.512378 && skewnessInput[indexGx] <= 0.416463 &&
                    fftEnergyInput[indexGx] > 1712.606526 && rootMeanSquareInput[indexGz] <= 95.840885 &&
                    rootMeanSquareInput[indexAz] > 0.706754 && kurtosisInput[indexGx] <= 3.64155 &&
                    skewnessInput[indexRoll] > -1.255131 &&  meanInput[indexPitch] <= 26.655934 &&
                    zeroCrossingRateInput[indexAy] > 0.005556 && meanInput[indexPitch] <= 6.591121 &&
                    zeroCrossingRateInput[indexAy] <= 0.055556 && zeroCrossingRateInput[indexGz] > 0.027778 &&
                    standardDeviationInput[indexGx] > 71.211221 && kurtosisInput[indexGy] > 2.229675){
                gesture = "VLR";            //VLR (183.0/4.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] <= 2.325125 && covarianceGxGz > -4899.059989 &&
                    fftEnergyInput[indexTotalGyro] <= 30305.53282 && skewnessInput[indexPitch] > -0.512851 &&
                    skewnessInput[indexGz] > -1.614944 && correlationGxGz > -0.712949 &&
                    meanAbsDevInput[indexPitch] > 18.512378 && skewnessInput[indexGx] <= 0.416463 &&
                    fftEnergyInput[indexGx] > 1712.606526 && rootMeanSquareInput[indexGz] <= 95.840885 &&
                    rootMeanSquareInput[indexAz] > 0.706754 && kurtosisInput[indexGx] <= 3.64155 &&
                    skewnessInput[indexRoll] > -1.255131 && meanInput[indexPitch] <= 26.655934 &&
                    zeroCrossingRateInput[indexAy] > 0.005556 && meanInput[indexPitch] > 6.591121){
                gesture = "VLR";            //VLR (1420.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] <= 2.325125 && covarianceGxGz > -4899.059989 &&
                    fftEnergyInput[indexTotalGyro] <= 30305.53282 && skewnessInput[indexPitch] > -0.512851 &&
                    skewnessInput[indexGz] > -1.614944 && correlationGxGz > -0.712949 &&
                    meanAbsDevInput[indexPitch] > 18.512378 && skewnessInput[indexGx] <= 0.416463 &&
                    fftEnergyInput[indexGx] > 1712.606526 && rootMeanSquareInput[indexGz] <= 95.840885 &&
                    rootMeanSquareInput[indexAz] > 0.706754 && kurtosisInput[indexGx] > 3.64155 &&
                    meanInput[indexGx] > -13.452572 && fftMaxFreqInput[indexGy] <= 0.625 &&
                    correlationGxGy > -0.602277 && rootMeanSquareInput[indexAz] > 0.832063 &&
                    kurtosisInput[indexGx] <= 4.337805 && fftMaxFreqInput[indexGz] > 0 &&
                    fftMaxFreqInput[indexGz] <= 0.3125 && meanAbsDevInput[indexAy] <= 0.531351 &&
                    fftEnergyInput[indexAx] > 0.034613 && rootMeanSquareInput[indexPitch] <= 33.315127){
                gesture = "VLR";            //VLR (138.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] > 2.325125 && skewnessInput[indexAz] > -0.881478 &&
                    meanAbsDevInput[indexTotalAcc] <= 0.382029 && varianceInput[indexAy] <= 0.214709 &&
                    standardDeviationInput[indexTotalGyro] > 51.442238 && skewnessInput[indexAz] <= 0.852416 &&
                    standardDeviationInput[indexGz] > 46.119652 && kurtosisInput[indexRoll] > 2.247572 &&
                    zeroCrossingRateInput[indexAx] > 0.011111 &&  kurtosisInput[indexAz] <= 4.900769 &&
                    meanInput[indexGy] <= -13.734394 && zeroCrossingRateInput[indexGx] <= 0.088889 &&
                    correlationAxAy <= 0.455193 && covarianceGxGz <= 1346.570389 &&
                    correlationAyAz > 0.138982 && meanInput[indexAy] > -0.068127){
                gesture = "CounterClockwise";           //CounterClockwise (613.0/3.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] > 2.325125 && skewnessInput[indexAz] > -0.881478 &&
                    meanAbsDevInput[indexTotalAcc] > 0.382029 && kurtosisInput[indexGz] <= 5.114782 &&
                    meanInput[indexGy] <= -52.081045 && fftMaxFreqInput[indexGx] <= 0.3125 &&
                    zeroCrossingRateInput[indexAx] > 0.027778){
                gesture = "CounterClockwise";           //CounterClockwise (131.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] > 2.325125 && skewnessInput[indexAz] > -0.881478 &&
                    meanAbsDevInput[indexTotalAcc] > 0.382029 && kurtosisInput[indexGz] <= 5.114782 &&
                    meanInput[indexGy] > -52.081045 && skewnessInput[indexRoll] > -0.172656 &&
                    covarianceGyGz <= 2327.459364 && kurtosisInput[indexTotalAcc] <= 3.48505 &&
                    fftEnergyInput[indexAx] <= 0.363189 && meanInput[indexGz] > -30.635622 &&
                    covarianceGyGz > -1109.671763 && zeroCrossingRateInput[indexAz] > 0 &&
                    meanAbsDevInput[indexAz] > 0.323917 && kurtosisInput[indexGz] <= 4.62646 &&
                    skewnessInput[indexGx] <= 0.557181 && standardDeviationInput[indexAz] <= 0.797575 &&
                    fftEnergyInput[indexGy] <= 5031.719021 && covarianceGyGz <= 2101.270371){
                gesture = "CrossCounterClockwise";          //CrossCounterClockwise (717.0/6.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] <= 104.316766 &&
                    kurtosisInput[indexPitch] > 2.325125 && skewnessInput[indexAz] > -0.881478 &&
                    meanAbsDevInput[indexTotalAcc] > 0.382029 && kurtosisInput[indexGz] <= 5.114782 &&
                    meanInput[indexGy] > -52.081045 && skewnessInput[indexRoll] > -0.172656 &&
                    covarianceGyGz > 2327.459364 && skewnessInput[indexAz] <= -0.02389 &&
                    skewnessInput[indexTotalAcc] > 0.071727 && kurtosisInput[indexAy] > 1.630367){
                gesture = "CounterClockwise";           //CounterClockwise (126.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] > 104.316766 &&
                    correlationGxGz > -0.783957 && varianceInput[indexAz] <= 0.294508 &&
                    fftEntropyInput[indexGx] <= -3492.815241 && fftMaxFreqInput[indexGy] <= 0 &&
                    rootMeanSquareInput[indexGx] > 96.053483 && zeroCrossingRateInput[indexGx] > 0.011111 &&
                    covarianceAyAz <= 0.124551 && meanInput[indexGx] <= -11.357583 &&
                    meanInput[indexGz] > -21.86381 && zeroCrossingRateInput[indexGz] <= 0.122222 &&
                    correlationGxGy <= 0.598138){
                gesture = "CounterClockwise";           //CounterClockwise (141.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] > 104.316766 &&
                    correlationGxGz > -0.783957 && varianceInput[indexAz] <= 0.294508 &&
                    fftEntropyInput[indexGx] <= -3492.815241 && fftMaxFreqInput[indexGy] <= 0 &&
                    rootMeanSquareInput[indexGx] > 96.053483 && zeroCrossingRateInput[indexGx] > 0.011111 &&
                    covarianceAyAz <= 0.124551 && meanInput[indexGx] > -11.357583){
                gesture = "CounterClockwise";       //CounterClockwise (1790.0/2.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] <= -12.566079 &&
                    meanInput[indexAz] > 0.502853 && standardDeviationInput[indexTotalGyro] > 104.316766 &&
                    correlationGxGz > -0.783957 && varianceInput[indexAz] > 0.294508 &&
                    rootMeanSquareInput[indexAx] > 0.461516 && kurtosisInput[indexAx] > 2.348591	){
                gesture = "CounterClockwise";           //CounterClockwise (149.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] <= 4.700512 && meanInput[indexAz] <= 0.536131 &&
                    meanInput[indexRoll] <= 39.930345 && fftMaxFreqInput[indexPitch] <= 0.9375 &&
                    rootMeanSquareInput[indexRoll] > 32.076075 && fftEntropyInput[indexAz] > 1.147759 &&
                    skewnessInput[indexAx] <= 3.825562){
                gesture = "NonGesture";     //NonGesture (712.0)
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] <= 4.700512 && meanInput[indexAz] > 0.536131 &&
                    meanInput[indexAx] <= 0.268002 && kurtosisInput[indexPitch] <= 3.698662 &&
                    rootMeanSquareInput[indexAy] <= 0.559365 && rootMeanSquareInput[indexGz] <= 22.338421 &&
                    rootMeanSquareInput[indexRoll] <= 12.11598 && meanInput[indexGz] <= 9.696113 &&
                    kurtosisInput[indexTotalGyro] <= 3.936111){
                gesture = "Down";           //Down (128.0/1.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] <= 4.700512 && meanInput[indexAz] > 0.536131 &&
                    meanInput[indexAx] <= 0.268002 && kurtosisInput[indexPitch] <= 3.698662 &&
                    rootMeanSquareInput[indexAy] <= 0.559365 && rootMeanSquareInput[indexGz] > 22.338421 &&
                    standardDeviationInput[indexGz] <= 103.549533 && skewnessInput[indexAy] > 0.431796 &&
                    correlationGxGz > -0.517363 && meanInput[indexGy] <= -2.81567 &&
                    kurtosisInput[indexGx] <= 3.709559 && zeroCrossingRateInput[indexAx] <= 0.122222 &&
                    zeroCrossingRateInput[indexGy] <= 0.183333 && meanInput[indexTotalAcc] <= 1.029784 &&
                    meanInput[indexAz] <= 0.884319){
                gesture = "VLR";                //VLR (139.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] <= 4.700512 && meanInput[indexAz] > 0.536131 &&
                    meanInput[indexAx] <= 0.268002 && kurtosisInput[indexPitch] <= 3.698662 &&
                    rootMeanSquareInput[indexAy] > 0.559365 && covarianceAxAy > -0.018386 &&
                    correlationAyAz <= 0.42869 && covarianceGyGz <= 1826.910146 &&
                    skewnessInput[indexGx] > -0.809608 && meanAbsDevInput[indexGy] <= 38.702308 &&
                    skewnessInput[indexGx] <= 0.973917 && kurtosisInput[indexGx] <= 3.827976 &&
                    covarianceAxAy <= 0.035796 && correlationAyAz <= 0.37174 &&
                    meanInput[indexAx] > -0.059639){
                gesture = "Down";               //Down (3286.0/20.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 && meanInput[indexGy] > 4.700512 &&
                    meanInput[indexTotalGyro] <= 246.359846 && meanAbsDevInput[indexTotalGyro] <= 86.855893 &&
                    rootMeanSquareInput[indexAz] > 0.777087 && meanAbsDevInput[indexAy] <= 0.349909 &&
                    kurtosisInput[indexPitch] > 1.58825 && standardDeviationInput[indexGz] > 38.915689 &&
                    correlationGxGy > -0.365705 && correlationGxGz <= 0.247051 &&
                    skewnessInput[indexTotalGyro] <= 0.303218	){
                gesture = "Clockwise";          //Clockwise (368.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] > 4.700512 && meanInput[indexTotalGyro] <= 246.359846 &&
                    meanAbsDevInput[indexTotalGyro] <= 86.855893 && rootMeanSquareInput[indexAz] > 0.777087 &&
                    meanAbsDevInput[indexAy] > 0.349909 && rootMeanSquareInput[indexRoll] <= 63.524042 &&
                    fftEnergyInput[indexGz] > 1400.273064 && rootMeanSquareInput[indexRoll] > 13.418681 &&
                    covarianceAyAz > 0.010634 && zeroCrossingRateInput[indexAy] <= 0.083333 &&
                    meanAbsDevInput[indexAz] > 0.183653 && kurtosisInput[indexGx] <= 3.950876 &&
                    standardDeviationInput[indexTotalGyro] > 61.468032 && skewnessInput[indexGz] <= 0.851174 &&
                    correlationGyGz > -0.153067 && skewnessInput[indexPitch] <= 1.03013 &&
                    kurtosisInput[indexGz] > 1.923428 && meanInput[indexGz] <= -8.018197 &&
                    fftEnergyInput[indexTotalAcc] <= 1.837082 && zeroCrossingRateInput[indexAy] <= 0.055556 &&
                    standardDeviationInput[indexTotalGyro] > 64.112393 && covarianceAyAz > 0.069539){
                gesture = "VRL";            //VRL (200.0/2.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] > 4.700512 &&  meanInput[indexTotalGyro] <= 246.359846 &&
                    meanAbsDevInput[indexTotalGyro] <= 86.855893 && rootMeanSquareInput[indexAz] > 0.777087 &&
                    meanAbsDevInput[indexAy] > 0.349909 && rootMeanSquareInput[indexRoll] <= 63.524042 &&
                    fftEnergyInput[indexGz] > 1400.273064 && rootMeanSquareInput[indexRoll] > 13.418681 &&
                    covarianceAyAz > 0.010634 && zeroCrossingRateInput[indexAy] <= 0.083333 &&
                    meanAbsDevInput[indexAz] > 0.183653 && kurtosisInput[indexGx] <= 3.950876 &&
                    standardDeviationInput[indexTotalGyro] > 61.468032 && skewnessInput[indexGz] <= 0.851174 &&
                    correlationGyGz > -0.153067 && skewnessInput[indexPitch] <= 1.03013 &&
                    kurtosisInput[indexGz] > 1.923428 && meanInput[indexGz] > -8.018197 &&
                    correlationGxGy <= 0.636961 && fftEntropyInput[indexGz] > -31641.51026 &&
                    skewnessInput[indexAz] > -0.424148 && fftEntropyInput[indexGz] <= -9521.985316 &&
                    zeroCrossingRateInput[indexGx] > 0.016667 && standardDeviationInput[indexGy] > 29.856683){
                gesture = "VRL";        //VRL (848.0/1.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] > 4.700512 && meanInput[indexTotalGyro] <= 246.359846 &&
                    meanAbsDevInput[indexTotalGyro] > 86.855893 && varianceInput[indexAz] <= 0.277733 &&
                    meanAbsDevInput[indexPitch] <= 49.077851 && meanInput[indexGy] > 16.820526 &&
                    meanInput[indexTotalGyro] > 100.902565 && skewnessInput[indexAx] <= 1.55209 &&
                    meanAbsDevInput[indexAz] <= 0.384208 && meanInput[indexAz] > 0.605374 &&
                    meanAbsDevInput[indexAy] > 0.159146 && skewnessInput[indexGz] > -0.736369 &&
                    skewnessInput[indexTotalGyro] <= 0.523443){
                gesture = "Clockwise";          //Clockwise (1365.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] > 4.700512 && meanInput[indexTotalGyro] <= 246.359846 &&
                    meanAbsDevInput[indexTotalGyro] > 86.855893 && varianceInput[indexAz] <= 0.277733 &&
                    meanAbsDevInput[indexPitch] <= 49.077851 && meanInput[indexGy] > 16.820526 &&
                    meanInput[indexTotalGyro] > 100.902565 && skewnessInput[indexAx] <= 1.55209 &&
                    meanAbsDevInput[indexAz] <= 0.384208 && meanInput[indexAz] > 0.605374 &&
                    meanAbsDevInput[indexAy] > 0.159146 && skewnessInput[indexGz] > -0.736369 &&
                    skewnessInput[indexTotalGyro] > 0.523443 && fftEnergyInput[indexTotalAcc] > 0.94103 &&
                    meanAbsDevInput[indexTotalGyro] <= 110.701473 && covarianceGyGz <= 5377.935841 &&
                    meanAbsDevInput[indexGz] <= 64.479063){
                gesture = "Clockwise";          //Clockwise (439.0/1.0)	 &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz <= 0.530134 &&
                    meanInput[indexGy] > 4.700512 && meanInput[indexTotalGyro] <= 246.359846 &&
                    meanAbsDevInput[indexTotalGyro] > 86.855893 && varianceInput[indexAz] > 0.277733 &&
                    standardDeviationInput[indexAy] <= 0.87533 && meanInput[indexAz] > 0.593903 &&
                    standardDeviationInput[indexPitch] <= 66.86734){
                gesture = "CrossClockwise";         //CrossClockwise (257.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 &&
                    correlationAyAz <= 0.530134 && meanInput[indexGy] > 4.700512 && meanInput[indexTotalGyro] > 246.359846 &&
                    fftMaxFreqInput[indexGy] <= 0.625){
                gesture = "CounterClokwise";      //CounterClockwise (146.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 &&
                    skewnessInput[indexAz] <= -0.549778 &&
                    meanAbsDevInput[indexAy] <= 0.44287 &&
                    meanInput[indexAy] <= 0.217537 &&
                    skewnessInput[indexAy] <= -0.172872 &&
                    skewnessInput[indexTotalGyro] <= 1.1562 &&
                    rootMeanSquareInput[indexGz] <= 66.959876 &&
                    meanInput[indexGz] <= 7.837592 && skewnessInput[indexGx] > -0.874231 &&
                    kurtosisInput[indexTotalAcc] <= 3.888292 &&
                    zeroCrossingRateInput[indexAy] <= 0.088889 && covarianceAxAy > -0.006621 && skewnessInput[indexAz] > -1.354658){
                gesture = "Up";                 //Up (2370.0/6.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 &&
                    skewnessInput[indexAz] > -0.549778 && meanAbsDevInput[indexGz] <= 21.612625 &&
                    kurtosisInput[indexAz] <= 2.593866 && meanInput[indexAy] <= -0.060713){
                gesture = "Up";                 //Up (438.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 &&
                    skewnessInput[indexAz] > -0.549778 && meanAbsDevInput[indexGz] > 21.612625 &&
                    rootMeanSquareInput[indexAy] <= 0.411969 && meanInput[indexTotalAcc] > 0.936176 &&
                    meanInput[indexGy] > 0.544357 && correlationGyGz <= 0.618342 &&
                    correlationGxGz <= 0.428194 && kurtosisInput[indexRoll] <= 3.812553 &&
                    kurtosisInput[indexRoll] > 3.812553 && skewnessInput[indexAy] > -0.766736 &&
                    skewnessInput[indexGx] <= 1.530087 && fftEntropyInput[indexGy] <= -3120.025611 &&
                    covarianceGyGz <= 1132.81995 && skewnessInput[indexTotalAcc] <= 1.263777){
                gesture = "Clockwise";          //Clockwise (439.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 && skewnessInput[indexAz] > -0.549778 &&
                    meanAbsDevInput[indexGz] > 21.612625 && rootMeanSquareInput[indexAy] > 0.411969 &&
                    meanInput[indexAy] <= 0.163159 && meanAbsDevInput[indexRoll] <= 56.082369 &&
                    meanAbsDevInput[indexTotalAcc] > 0.217634 && correlationGxGz <= -0.538152 &&
                    meanInput[indexAy] > -0.01013 && standardDeviationInput[indexAz] > 0.275796 &&
                    meanInput[indexRoll] > -27.423005 && skewnessInput[indexPitch] > -0.670294 &&
                    rootMeanSquareInput[indexAx] <= 0.470902 && correlationAxAy <= 0.858857 &&
                    standardDeviationInput[indexGy] <= 57.04856 && standardDeviationInput[indexPitch] > 27.121931 &&
                    kurtosisInput[indexRoll] > 2.553171 && meanAbsDevInput[indexAz] <= 0.411755 &&
                    rootMeanSquareInput[indexRoll] > 17.122216){
                gesture = "CrossClockwise";     //CrossClockwise (247.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 &&
                    skewnessInput[indexAz] > -0.549778 && meanAbsDevInput[indexGz] > 21.612625 &&
                    rootMeanSquareInput[indexAy] > 0.411969 && meanInput[indexAy] <= 0.163159 &&
                    meanAbsDevInput[indexRoll] <= 56.082369 && meanAbsDevInput[indexTotalAcc] > 0.217634 &&
                    correlationGxGz > -0.538152 && standardDeviationInput[indexTotalGyro] <= 94.784601 &&
                    kurtosisInput[indexAy] <= 4.040889 && kurtosisInput[indexPitch] <= 4.103628 &&
                    meanInput[indexAz] <= 0.943965 && covarianceGyGz > -768.091246 &&
                    fftEntropyInput[indexGz] <= -6907.750634 && meanInput[indexGy] > 0.973638 &&
                    meanInput[indexAy] > -0.262362 && meanInput[indexPitch] <= 4.048001 &&
                    meanInput[indexAx] > -0.269891){
                gesture = "VRL";                    //VRL (823.0/9.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 && skewnessInput[indexAz] > -0.549778 && meanAbsDevInput[indexGz] > 21.612625 && rootMeanSquareInput[indexAy] > 0.411969 && meanInput[indexAy] > 0.163159 &&
                    covarianceAxAy <= 0.138827 &&
                    rootMeanSquareInput[indexAz] <= 0.757661 && meanInput[indexGy] > 30.848906 && meanInput[indexAz] > 0.465072 &&
                    fftMaxFreqInput[indexGy] <= 0){
                gesture = "Clockwise";              //Clockwise (108.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 && skewnessInput[indexAz] > -0.549778 && meanAbsDevInput[indexGz] > 21.612625 && rootMeanSquareInput[indexAy] > 0.411969 && meanInput[indexAy] > 0.163159 &&
                    covarianceAxAy <= 0.138827 && rootMeanSquareInput[indexAz] > 0.757661 &&
                    meanInput[indexGy] <= -0.188743 &&
                    kurtosisInput[indexPitch] <= 2.755907 &&
                    correlationGxGz <= -0.575721 &&
                    skewnessInput[indexTotalGyro] <= 1.104177 && meanInput[indexTotalAcc] > 0.938953 && skewnessInput[indexGx] > -0.234692 &&
                    meanInput[indexPitch] <= 22.166086 && meanAbsDevInput[indexAz] > 0.127643	){
                gesture = "CrossClockwise";         //CrossClockwise (332.0/4.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 && skewnessInput[indexAz] > -0.549778 && meanAbsDevInput[indexGz] > 21.612625 && rootMeanSquareInput[indexAy] > 0.411969 && meanInput[indexAy] > 0.163159 &&
                    covarianceAxAy <= 0.138827 && rootMeanSquareInput[indexAz] > 0.757661 &&
                    meanInput[indexGy] <= -0.188743 &&
                    kurtosisInput[indexPitch] <= 2.755907 && correlationGxGz > -0.575721 && skewnessInput[indexGx] > -0.394953 && kurtosisInput[indexRoll] > 2.412863 &&
                    skewnessInput[indexTotalAcc] <= 0.605552 &&
                    skewnessInput[indexPitch] <= 0.089668 && meanAbsDevInput[indexAy] > 0.301025 && meanAbsDevInput[indexGz] > 27.64201 &&
                    varianceInput[indexAy] <= 0.464184 && standardDeviationInput[indexTotalGyro] > 47.969061 &&
                    kurtosisInput[indexGz] <= 3.461756){
                gesture = "CrossCounterClockwise"; //CrossCounterClockwise (200.0/2.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 &&
                    skewnessInput[indexAz] > -0.549778 && meanAbsDevInput[indexGz] > 21.612625 &&
                    rootMeanSquareInput[indexAy] > 0.411969 && meanInput[indexAy] > 0.163159 &&
                    covarianceAxAy <= 0.138827 && rootMeanSquareInput[indexAz] > 0.757661 &&
                    meanInput[indexGy] <= -0.188743 && kurtosisInput[indexPitch] > 2.755907 &&
                    meanInput[indexAx] > -0.200245 && standardDeviationInput[indexAz] > 0.242941 &&
                    covarianceAxAy <= 0.096905 && covarianceGxGz > -6836.966722 &&
                    kurtosisInput[indexAz] <= 3.823873 && kurtosisInput[indexAx] <= 4.335027 &&
                    standardDeviationInput[indexAz] > 0.286557 && fftEnergyInput[indexGz] <= 11320.35669 &&
                    skewnessInput[indexGx] > -0.340995 && meanAbsDevInput[indexGy] <= 44.805552 &&
                    kurtosisInput[indexPitch] > 2.935167 && correlationAxAy > -0.434208){
                gesture = "CrossCounterClockwise"; // CrossCounterClockwise (1650.0/1.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz <= 5444.151955 && correlationAyAz > 0.530134 &&
                    skewnessInput[indexAz] > -0.549778 && meanAbsDevInput[indexGz] > 21.612625 &&
                    rootMeanSquareInput[indexAy] > 0.411969 && meanInput[indexAy] > 0.163159 &&
                    covarianceAxAy <= 0.138827 && rootMeanSquareInput[indexAz] > 0.757661 &&
                    meanInput[indexGy] > -0.188743 && meanInput[indexGy] <= 18.350728 &&
                    skewnessInput[indexAy] > -0.389865 && skewnessInput[indexPitch] <= 0.092549 &&
                    skewnessInput[indexRoll] <= 1.769482 && meanInput[indexAz] <= 0.927262 &&
                    meanInput[indexAx] <= 0.309353 && meanInput[indexGx] > -31.396018 &&
                    meanInput[indexTotalAcc] > 0.932217 && covarianceGxGy > -5304.666558 &&
                    zeroCrossingRateInput[indexGx] <= 0.05 && skewnessInput[indexTotalGyro] <= 0.652652 &&
                    correlationAyAz <= 0.851624 && fftEnergyInput[indexAz] > 0.615427){
                gesture = "CrossClockwise"; //CrossClockwise (1766.0/8.0)
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079
                    && covarianceGxGz > 5444.151955 && skewnessInput[indexRoll] <= 0.100002 &&
                    zeroCrossingRateInput[indexAx] <= 0.044444 && skewnessInput[indexTotalGyro] <= 1.049496	){
                gesture = "Right"; //Right (667.0) &&
            } else if(meanAbsDevInput[indexGx] > 40.975142 && meanInput[indexGy] > -12.566079 &&
                    covarianceGxGz > 5444.151955 && skewnessInput[indexRoll] > 0.100002 &&
                    rootMeanSquareInput[indexAx] > 0.693705 && fftMaxFreqInput[indexAz] <= 0.3125 &&
                    kurtosisInput[indexGz] <= 3.604646 && kurtosisInput[indexPitch] > 2.794047 &&
                    covarianceAxAy <= -0.052118 && rootMeanSquareInput[indexPitch] > 34.323999){
                gesture = "Left"; //Left (311.0/2.0) &&
            }

        } else if(MODE == LOGISTIC_REGRESSION){
            double[] Class = new double[11];
            //Gesture Dependent Model
            Class[0] = -33.1618163 +  standardDeviationInput[indexAx] * -4.9775132 + standardDeviationInput[indexGz] * -0.0039074 + standardDeviationInput[indexRoll] * 0.0181695 + meanInput[indexAx] * 7.6565524 + meanInput[indexAy] * 2.3408094 + meanInput[indexAz] * 1.064188  + meanInput[indexGx] * 0.0168613 + meanInput[indexGy] * 0.0688201 + meanInput[indexGz] * -0.0358961 + meanInput[indexTotalAcc] * 6.4364108 + meanInput[indexPitch] * -0.0177401 + varianceInput[indexAz] * 5.0983396 + varianceInput[indexGx] * -0.0000787 + varianceInput[indexGz] * -0.0004 + varianceInput[indexRoll] * 0.0005201 + zeroCrossingRateInput[indexAx] * 8.2515769 + zeroCrossingRateInput[indexAz] * 56.0737149 + zeroCrossingRateInput[indexGx] * 8.9813828 + zeroCrossingRateInput[indexGz] * 2.9482977 + rootMeanSquareInput[indexAy] * -7.4212945 + rootMeanSquareInput[indexAz] * 22.0367706 + rootMeanSquareInput[indexGz] * -0.0065932 + skewnessInput[indexAx] * 0.2766235 + skewnessInput[indexAy] * -0.1800194 + skewnessInput[indexAz] * 0.1570729 + skewnessInput[indexGx] * -0.0291631 + skewnessInput[indexGy] * 0.0375429 + skewnessInput[indexGz] * -0.2185451 + skewnessInput[indexTotalGyro] * 0.6136045 + skewnessInput[indexRoll] * -0.1716495 + skewnessInput[indexPitch] * -0.1635413 + kurtosisInput[indexAx] * -0.0200416 + kurtosisInput[indexAy] * 0.0651308 + kurtosisInput[indexAz] * 0.0385621 + kurtosisInput[indexGx] * -1.3252252 + kurtosisInput[indexGy] * 0.0793252 + kurtosisInput[indexGz] * 0.165544  + kurtosisInput[indexTotalAcc] * -0.2444748 + kurtosisInput[indexPitch] * 0.0095022 + meanAbsDevInput[indexTotalAcc] * 1.0532142 + fftMaxFreqInput[indexAx] * 0.1986601 + fftMaxFreqInput[indexAy] * -0.9286147 + fftMaxFreqInput[indexAz] * 2.9569767 + fftMaxFreqInput[indexGx] * -0.6325286 + fftMaxFreqInput[indexGz] * -0.6347464 + fftMaxFreqInput[indexRoll] * 0.5792174 + fftMaxFreqInput[indexPitch] * -0.6420829 + fftEnergyInput[indexAx] * 7.9115491 + fftEnergyInput[indexAy] * -5.8969648 + fftEnergyInput[indexAz] * 3.0092845 + fftEnergyInput[indexGx] * -0.0000499 + fftEnergyInput[indexGy] * -0.0002001 + fftEnergyInput[indexTotalGyro] * -0.0000058 + fftEnergyInput[indexPitch] * -0.0000749 + fftEntropyInput[indexAx] * 0.0110326 + fftEntropyInput[indexAy] * 0.017708  + fftEntropyInput[indexAz] * 0.0414834 + fftEntropyInput[indexGz] * 0.0000515 + fftEntropyInput[indexTotalAcc] * -0.0228218 + fftEntropyInput[indexRoll] * -0.0000259 + fftEntropyInput[indexPitch] * 0.0000182 + covarianceAxAy * 0.8271569 + covarianceAyAz * 24.7388449 + covarianceGxGz * -0.0006715 + covarianceGyGz * 0.0004152 + correlationAxAy * 1.6069652 + correlationGxGz * 0.5507156 + correlationGyGz * -0.9204505 ;
            Class[1] = 0.0744981 +  standardDeviationInput[indexAx] * 12.0666188 + standardDeviationInput[indexGx] * 0.0502056 + standardDeviationInput[indexGy] * 0.0090954 + standardDeviationInput[indexTotalAcc] * 32.2908592 + standardDeviationInput[indexRoll] * -0.2667121 + meanInput[indexAz] * -6.7637689 + meanInput[indexGx] * -0.0485405 + meanInput[indexGy] * 0.0258286 + meanInput[indexGz] * -0.039886 + meanInput[indexTotalAcc] * -2.3982562 + meanInput[indexPitch] * 0.0476949 + varianceInput[indexAy] * 5.0258805 + varianceInput[indexAz] * -41.346417 + varianceInput[indexGy] * -0.000153 + varianceInput[indexGz] * -0.000975 + varianceInput[indexTotalGyro] * -0.0009473 + varianceInput[indexRoll] * -0.0059348 + zeroCrossingRateInput[indexAx] * -2.0458438 + zeroCrossingRateInput[indexAy] * -8.7997991 + zeroCrossingRateInput[indexGx] * -24.0328517 + zeroCrossingRateInput[indexGy] * -21.8270446 + zeroCrossingRateInput[indexGz] * 5.4104865 + zeroCrossingRateInput[indexPitch] * -3.5539671 + rootMeanSquareInput[indexAx] * 1.2264246 + rootMeanSquareInput[indexGx] * 0.0154884 + rootMeanSquareInput[indexGz] * -0.0344531 + rootMeanSquareInput[indexTotalAcc] * -1.0832532 + skewnessInput[indexAx] * -0.6668934 + skewnessInput[indexAy] * 2.1369081 + skewnessInput[indexAz] * -0.7232969 + skewnessInput[indexGy] * 0.2184519 + skewnessInput[indexTotalAcc] * -2.2557234 + kurtosisInput[indexAz] * -1.005426 + kurtosisInput[indexGy] * 0.1686768 + kurtosisInput[indexGz] * 0.1676642 + kurtosisInput[indexPitch] * -0.7658001 + meanAbsDevInput[indexAx] * -10.929301 + meanAbsDevInput[indexAy] * 5.7114369 + meanAbsDevInput[indexPitch] * 0.1314959 + fftMaxFreqInput[indexAx] * -3.0463018 + fftMaxFreqInput[indexAy] * 1.3063795 + fftMaxFreqInput[indexGx] * 0.8797659 + fftMaxFreqInput[indexGy] * -0.8474748 + fftMaxFreqInput[indexGz] * 1.7834735 + fftMaxFreqInput[indexRoll] * -0.6410277 + fftEnergyInput[indexAy] * -3.8358064 + fftEnergyInput[indexAz] * 3.0626343 + fftEnergyInput[indexGz] * -0.0001579 + fftEnergyInput[indexRoll] * -0.0012734 + fftEntropyInput[indexAy] * -0.0065414 + fftEntropyInput[indexAz] * -0.0431424 + fftEntropyInput[indexGx] * 0.0000205 + fftEntropyInput[indexGy] * -0.0002434 + fftEntropyInput[indexGz] * 0.0002798 + covarianceGxGy * 0.0004961 + covarianceGxGz * -0.0014099 + covarianceGyGz * -0.0003483 + correlationAxAy * 1.1516286 + correlationGxGz * -1.3402399 + correlationGyGz * -1.0923233 ;
            Class[2] = -38.285626 +  standardDeviationInput[indexAy] * 1.8376543 + standardDeviationInput[indexAz] * 5.6795028 + standardDeviationInput[indexGx] * 0.0189526 + standardDeviationInput[indexGy] * 0.0236946 + standardDeviationInput[indexGz] * 0.0466231 + standardDeviationInput[indexRoll] * 0.0544853 + meanInput[indexAx] * 10.1073979 + meanInput[indexAz] * 8.2737811 + meanInput[indexGy] * -0.0702193 + meanInput[indexGz] * -0.002851 + meanInput[indexTotalAcc] * 23.6611476 + varianceInput[indexAx] * -20.1093226 + varianceInput[indexAz] * -122.7821726 + varianceInput[indexGx] * -0.0008736 + varianceInput[indexGz] * 0.0002328 + varianceInput[indexTotalAcc] * -22.8071346 + varianceInput[indexTotalGyro] * -0.0001053 + zeroCrossingRateInput[indexAx] * 8.1429925 + zeroCrossingRateInput[indexGx] * 21.5902745 + zeroCrossingRateInput[indexGz] * 2.0953274 + zeroCrossingRateInput[indexPitch] * -1.2947365 + rootMeanSquareInput[indexAx] * 3.5830027 + rootMeanSquareInput[indexAy] * 4.5861543 + rootMeanSquareInput[indexGx] * -0.0108271 + skewnessInput[indexAx] * -1.520357 + skewnessInput[indexAy] * 0.6906282 + skewnessInput[indexGx] * 0.0855304 + skewnessInput[indexGy] * 0.4041753 + skewnessInput[indexGz] * -0.4938583 + skewnessInput[indexTotalAcc] * -0.2822861 + kurtosisInput[indexAz] * 0.0440194 + kurtosisInput[indexGx] * 0.0222665 + kurtosisInput[indexGy] * 0.0219068 + kurtosisInput[indexGz] * -0.2619157 + kurtosisInput[indexTotalAcc] * 0.4215047 + kurtosisInput[indexTotalGyro] * -0.155485 + kurtosisInput[indexRoll] * -0.7895571 + kurtosisInput[indexPitch] * 0.0946024 + meanAbsDevInput[indexAx] * 3.7349212 + meanAbsDevInput[indexAz] * -13.4443793 + meanAbsDevInput[indexGx] * 0.032784  + meanAbsDevInput[indexTotalGyro] * 0.0148891 + meanAbsDevInput[indexRoll] * 0.0241128 + fftMaxFreqInput[indexAx] * -0.891123 + fftMaxFreqInput[indexAy] * 1.2917509 + fftMaxFreqInput[indexGx] * 0.1440039 + fftMaxFreqInput[indexGz] * -0.4885648 + fftMaxFreqInput[indexRoll] * 0.2106708 + fftMaxFreqInput[indexPitch] * 0.7311252 + fftEnergyInput[indexAx] * -1.7062974 + fftEnergyInput[indexGz] * -0.0001124 + fftEnergyInput[indexTotalGyro] * -0.0000145 + fftEnergyInput[indexPitch] * -0.0001261 + fftEntropyInput[indexAx] * -0.0389961 + fftEntropyInput[indexAy] * -0.0117098 + fftEntropyInput[indexAz] * -0.0462306 + fftEntropyInput[indexGx] * -0.0001181 + fftEntropyInput[indexGy] * 0.0000162 + fftEntropyInput[indexGz] * -0.0000291 + fftEntropyInput[indexTotalAcc] * 0.035461  + fftEntropyInput[indexPitch] * 0.0004259 + covarianceAxAy * 6.0906675 + covarianceAyAz * 47.9449009 + covarianceGxGy * 0.0019696 + covarianceGyGz * 0.000276  + correlationAyAz * -1.0719843 + correlationGxGy * -2.1678636 + correlationGxGz * -0.4264067 ;
            Class[3] = 1.8590596 +  standardDeviationInput[indexAy] * -3.6411575 + standardDeviationInput[indexGy] * 0.0251467 + standardDeviationInput[indexTotalAcc] * -6.8005469 + standardDeviationInput[indexTotalGyro] * 0.0278697 + standardDeviationInput[indexRoll] * 0.1800705 + meanInput[indexAx] * -11.2403655 + meanInput[indexGx] * 0.091393  + meanInput[indexTotalAcc] * 18.0192772 + varianceInput[indexAx] * 11.1171986 + varianceInput[indexAy] * -20.6769831 + varianceInput[indexGz] * 0.0001367 + varianceInput[indexTotalAcc] * -25.462464 + zeroCrossingRateInput[indexAx] * -1.7946551 + zeroCrossingRateInput[indexAy] * 4.5150242 + zeroCrossingRateInput[indexGx] * 22.2920867 + zeroCrossingRateInput[indexGy] * 14.3442748 + zeroCrossingRateInput[indexGz] * -16.3051582 + zeroCrossingRateInput[indexRoll] * -11.5128177 + rootMeanSquareInput[indexAx] * 6.8058439 + rootMeanSquareInput[indexGx] * 0.0601955 + rootMeanSquareInput[indexRoll] * 0.0352709 + skewnessInput[indexAx] * 1.4015562 + skewnessInput[indexAy] * 0.3082684 + skewnessInput[indexAz] * 0.1176924 + skewnessInput[indexGx] * -1.0749324 + skewnessInput[indexGy] * 0.114342  + skewnessInput[indexRoll] * -1.011234 + skewnessInput[indexPitch] * -0.2020447 + kurtosisInput[indexAx] * -0.3320065 + kurtosisInput[indexAy] * -0.3021961 + kurtosisInput[indexAz] * -0.5491871 + kurtosisInput[indexGx] * 0.1558562 + kurtosisInput[indexGz] * -1.6416827 + kurtosisInput[indexTotalAcc] * -0.8806826 + kurtosisInput[indexTotalGyro] * -0.3841784 + kurtosisInput[indexRoll] * -1.2955926 + kurtosisInput[indexPitch] * 0.1399203 + meanAbsDevInput[indexAz] * 5.7547008 + meanAbsDevInput[indexGy] * -0.1030074 + meanAbsDevInput[indexGz] * 0.07906   + meanAbsDevInput[indexTotalAcc] * -7.5983003 + meanAbsDevInput[indexTotalGyro] * 0.0366832 + meanAbsDevInput[indexRoll] * -0.396317 + fftMaxFreqInput[indexAy] * -0.7267884 + fftMaxFreqInput[indexGx] * -0.6011689 + fftMaxFreqInput[indexGy] * 0.2018263 + fftMaxFreqInput[indexGz] * -1.0026687 + fftMaxFreqInput[indexRoll] * 0.9601587 + fftEnergyInput[indexAy] * -7.0775065 + fftEnergyInput[indexTotalAcc] * -9.6414927 + fftEntropyInput[indexAx] * -0.0255538 + fftEntropyInput[indexAy] * -0.0842956 + fftEntropyInput[indexAz] * 0.1292058 + fftEntropyInput[indexGy] * 0.0000884 + fftEntropyInput[indexTotalAcc] * 0.0384825 + fftEntropyInput[indexTotalGyro] * -0.0000201 + fftEntropyInput[indexPitch] * 0.0006275 + covarianceAxAy * -9.7689504 + covarianceAyAz * 28.0030839 + covarianceGxGy * 0.003862  + covarianceGxGz * 0.0003983 + correlationAxAy * -2.1685927 + correlationAyAz * -6.9784758 + correlationGxGy * -5.0678775 + correlationGxGz * 0.807836  ;
            Class[4] = -8.5170983 +  standardDeviationInput[indexAz] * 1.3516028 + standardDeviationInput[indexGx] * 0.0806839 + standardDeviationInput[indexGy] * 0.0596729 + standardDeviationInput[indexGz] * 0.0114447 + standardDeviationInput[indexTotalAcc] * 18.7214472 + standardDeviationInput[indexPitch] * 0.0099112 + meanInput[indexAy] * -0.5441386 + meanInput[indexAz] * 2.7403929 + meanInput[indexGx] * -0.0316397 + meanInput[indexGy] * -0.1067303 + meanInput[indexGz] * 0.0037324 + meanInput[indexTotalAcc] * -0.6947652 + meanInput[indexPitch] * 0.0588763 + varianceInput[indexAy] * -2.9348699 + varianceInput[indexGy] * 0.0002642 + varianceInput[indexGz] * -0.0002058 + varianceInput[indexTotalGyro] * -0.0010134 + zeroCrossingRateInput[indexAz] * -27.6496418 + zeroCrossingRateInput[indexGx] * 7.2958601 + zeroCrossingRateInput[indexGz] * -35.5325124 + zeroCrossingRateInput[indexRoll] * -6.0404401 + zeroCrossingRateInput[indexPitch] * -10.1846036 + rootMeanSquareInput[indexAx] * -1.0722733 + rootMeanSquareInput[indexAz] * 4.7565265 + rootMeanSquareInput[indexGx] * 0.0467137 + rootMeanSquareInput[indexTotalAcc] * -8.3860357 + rootMeanSquareInput[indexRoll] * -0.0333188 + skewnessInput[indexAx] * -0.403289 + skewnessInput[indexAy] * 2.666303  + skewnessInput[indexAz] * -2.0477032 + skewnessInput[indexGx] * -0.8000496 + skewnessInput[indexGy] * -0.6542969 + skewnessInput[indexGz] * 0.1094835 + skewnessInput[indexTotalAcc] * 0.1833243 + skewnessInput[indexPitch] * 0.9105375 + kurtosisInput[indexAx] * -0.6773913 + kurtosisInput[indexAy] * -0.0921537 + kurtosisInput[indexAz] * -0.3812389 + kurtosisInput[indexGz] * -0.3391355 + kurtosisInput[indexTotalAcc] * -0.1650762 + kurtosisInput[indexTotalGyro] * 0.2122005 + kurtosisInput[indexRoll] * -0.0268551 + kurtosisInput[indexPitch] * -0.6245017 + meanAbsDevInput[indexAy] * 2.5801499 + meanAbsDevInput[indexAz] * 3.3716014 + meanAbsDevInput[indexRoll] * 0.02166   + fftMaxFreqInput[indexAx] * 0.3921425 + fftMaxFreqInput[indexAy] * 1.2709498 + fftMaxFreqInput[indexGx] * 1.7945664 + fftMaxFreqInput[indexGy] * -0.8949982 + fftMaxFreqInput[indexGz] * -0.924021 + fftEnergyInput[indexAy] * -1.7236247 + fftEnergyInput[indexAz] * -0.4610519 + fftEnergyInput[indexGx] * -0.0001042 + fftEnergyInput[indexGy] * 0.0000555 + fftEnergyInput[indexGz] * -0.0005034 + fftEnergyInput[indexTotalGyro] * -0.0000144 + fftEntropyInput[indexAz] * 0.0265322 + fftEntropyInput[indexGx] * 0.0000299 + fftEntropyInput[indexGy] * -0.0001638 + fftEntropyInput[indexRoll] * 0.0000514 + covarianceAxAy * 9.9578132 + covarianceGxGy * 0.0001955 + covarianceGyGz * -0.0005287 + correlationAxAy * 1.8966078 + correlationAyAz * 0.2578225 + correlationGxGz * -0.6414749 + correlationGyGz * -0.3021783 ;
            Class[5] = -3.0382067 +  standardDeviationInput[indexAx] * 10.8641873 + standardDeviationInput[indexAy] * 13.7749587 + standardDeviationInput[indexGx] * 0.0080795 + standardDeviationInput[indexGz] * 0.0145675 + standardDeviationInput[indexTotalAcc] * 3.7869413 + standardDeviationInput[indexTotalGyro] * 0.0051969 + standardDeviationInput[indexPitch] * 0.0101114 + meanInput[indexAx] * -0.8638324 + meanInput[indexAy] * -9.5786531 + meanInput[indexAz] * 0.6950597 + meanInput[indexGx] * 0.0497295 + meanInput[indexGy] * 0.1909322 + meanInput[indexGz] * 0.004342  + meanInput[indexTotalAcc] * -3.0870566 + varianceInput[indexGx] * -0.0000175 + varianceInput[indexGy] * -0.0004409 + varianceInput[indexTotalGyro] * -0.0013068 + varianceInput[indexRoll] * -0.0032123 + varianceInput[indexPitch] * -0.0007277 + zeroCrossingRateInput[indexAx] * -4.2898242 + zeroCrossingRateInput[indexAy] * 16.9459328 + zeroCrossingRateInput[indexAz] * -125.5646576 + zeroCrossingRateInput[indexGx] * -29.1875138 + zeroCrossingRateInput[indexGy] * -18.2505365 + zeroCrossingRateInput[indexGz] * 20.1630391 + rootMeanSquareInput[indexAy] * -0.6695502 + rootMeanSquareInput[indexGy] * -0.0117177 + rootMeanSquareInput[indexTotalAcc] * -7.2939753 + rootMeanSquareInput[indexPitch] * -0.0103337 + skewnessInput[indexAx] * -0.9255902 + skewnessInput[indexAy] * 1.177878  + skewnessInput[indexAz] * 0.310915  + skewnessInput[indexGx] * 0.6617162 + skewnessInput[indexGy] * 0.4549779 + skewnessInput[indexGz] * -0.0936354 + skewnessInput[indexTotalAcc] * -0.2892696 + skewnessInput[indexTotalGyro] * -1.2562657 + skewnessInput[indexRoll] * -0.8895384 + skewnessInput[indexPitch] * 0.4599305 + kurtosisInput[indexAx] * 0.0748601 + kurtosisInput[indexAy] * -0.2865822 + kurtosisInput[indexAz] * -0.0438461 + kurtosisInput[indexGx] * -0.0732254 + kurtosisInput[indexGy] * -0.1413307 + kurtosisInput[indexGz] * 0.1213892 + kurtosisInput[indexTotalGyro] * -0.1433935 + kurtosisInput[indexRoll] * 0.105092  + kurtosisInput[indexPitch] * -0.4970196 + meanAbsDevInput[indexAy] * 4.5993685 + meanAbsDevInput[indexAz] * 25.7579849 + meanAbsDevInput[indexTotalAcc] * -1.2219332 + meanAbsDevInput[indexTotalGyro] * 0.0618882 + fftMaxFreqInput[indexAx] * -0.7696285 + fftMaxFreqInput[indexAy] * 1.1138137 + fftMaxFreqInput[indexGx] * 1.0147346 + fftMaxFreqInput[indexGy] * 0.3279434 + fftMaxFreqInput[indexRoll] * 0.277174  + fftMaxFreqInput[indexPitch] * -0.2047211 + fftEnergyInput[indexAx] * -6.3035509 + fftEnergyInput[indexGz] * -0.0000328 + fftEnergyInput[indexTotalGyro] * -0.000029 + fftEntropyInput[indexAy] * 0.0484599 + fftEntropyInput[indexAz] * 0.0516716 + fftEntropyInput[indexGy] * -0.0000155 + fftEntropyInput[indexRoll] * 0.0000455 + fftEntropyInput[indexPitch] * -0.0001403 + covarianceGxGy * -0.000341 + covarianceGxGz * -0.0000291 + covarianceGyGz * 0.0004304 + correlationAxAy * -0.2742002 + correlationAyAz * -0.8438933 + correlationGxGy * -2.9482065 + correlationGyGz * 0.8302832 ;
            Class[6] = -7.5751258 +  standardDeviationInput[indexAx] * 10.2729081 + standardDeviationInput[indexGy] * 0.0371842 + standardDeviationInput[indexTotalGyro] * -0.0064525 + standardDeviationInput[indexRoll] * 0.103219  + meanInput[indexGx] * 0.0233257 + meanInput[indexGy] * 0.0053178 + meanInput[indexGz] * 0.0780874 + meanInput[indexRoll] * -0.0466139 + meanInput[indexPitch] * -0.0063844 + varianceInput[indexAz] * -3.3373934 + varianceInput[indexGz] * -0.0002926 + varianceInput[indexTotalAcc] * -1.1789483 + varianceInput[indexPitch] * -0.002295 + zeroCrossingRateInput[indexAz] * 28.5060663 + zeroCrossingRateInput[indexGx] * -18.3931689 + zeroCrossingRateInput[indexGy] * -13.4782183 + zeroCrossingRateInput[indexGz] * 27.5065535 + zeroCrossingRateInput[indexRoll] * -3.0462762 + zeroCrossingRateInput[indexPitch] * -29.7397386 + rootMeanSquareInput[indexAx] * 0.8211245 + rootMeanSquareInput[indexAy] * 2.1152877 + rootMeanSquareInput[indexAz] * -1.7293084 + rootMeanSquareInput[indexGy] * -0.0855321 + rootMeanSquareInput[indexTotalGyro] * -0.0034991 + skewnessInput[indexAx] * -0.3701679 + skewnessInput[indexAz] * 0.4933804 + skewnessInput[indexGx] * 0.8492128 + skewnessInput[indexGy] * -0.3789221 + skewnessInput[indexGz] * -0.5596513 + skewnessInput[indexTotalAcc] * 0.8066531 + skewnessInput[indexTotalGyro] * 0.5491448 + kurtosisInput[indexAx] * -0.3155463 + kurtosisInput[indexAy] * 0.2795382 + kurtosisInput[indexAz] * -0.1376337 + kurtosisInput[indexGx] * -1.1333023 + kurtosisInput[indexGy] * -0.134204 + kurtosisInput[indexGz] * 0.0032038 + kurtosisInput[indexTotalGyro] * 0.5169006 + meanAbsDevInput[indexAy] * 9.1324358 + meanAbsDevInput[indexGx] * 0.0624353 + meanAbsDevInput[indexRoll] * -0.0474286 + fftMaxFreqInput[indexAx] * -0.2936297 + fftMaxFreqInput[indexAy] * -0.5211731 + fftMaxFreqInput[indexGy] * 0.4203542 + fftMaxFreqInput[indexGz] * 0.3029237 + fftMaxFreqInput[indexRoll] * 0.6963299 + fftMaxFreqInput[indexPitch] * -0.7379283 + fftEnergyInput[indexAx] * -8.1290349 + fftEnergyInput[indexGy] * -0.0002162 + fftEnergyInput[indexTotalAcc] * -0.7970064 + fftEnergyInput[indexRoll] * 0.0002113 + fftEnergyInput[indexPitch] * -0.0011798 + fftEntropyInput[indexAx] * 0.0632752 + fftEntropyInput[indexAy] * -0.0459953 + fftEntropyInput[indexAz] * 0.047297  + fftEntropyInput[indexGx] * -0.0000397 + fftEntropyInput[indexGy] * 0.0000872 + fftEntropyInput[indexGz] * 0.0000442 + fftEntropyInput[indexTotalAcc] * 0.031877  + covarianceAyAz * 43.4544309 + covarianceGxGz * -0.0005549 + covarianceGyGz * -0.0003148 + correlationAxAy * -2.2033404 + correlationAyAz * 3.173101  + correlationGxGy * -2.3480263 + correlationGxGz * 0.0709151 + correlationGyGz * 0.9930845 ;
            Class[7] = -12.275108 +  standardDeviationInput[indexGy] * 0.0280353 + standardDeviationInput[indexPitch] * 0.1234756 + meanInput[indexAy] * 1.5040424 + meanInput[indexAz] * 11.3099565 + meanInput[indexGz] * 0.0331667 + meanInput[indexTotalAcc] * -14.6550398 + meanInput[indexRoll] * -0.0411543 + varianceInput[indexAx] * -48.3440244 + varianceInput[indexAy] * -20.7310581 + varianceInput[indexAz] * -9.3152265 + varianceInput[indexGz] * -0.000135 + varianceInput[indexTotalAcc] * 2.3215992 + varianceInput[indexTotalGyro] * -0.0002742 + varianceInput[indexRoll] * -0.0003862 + zeroCrossingRateInput[indexAx] * 13.312236 + zeroCrossingRateInput[indexAz] * 103.9236691 + zeroCrossingRateInput[indexGx] * 22.1535876 + zeroCrossingRateInput[indexGy] * 2.0955277 + zeroCrossingRateInput[indexGz] * -7.1679718 + rootMeanSquareInput[indexAx] * 7.5877806 + rootMeanSquareInput[indexGy] * -0.0070592 + skewnessInput[indexAx] * 0.6966624 + skewnessInput[indexAy] * 1.1532911 + skewnessInput[indexAz] * -0.1135677 + skewnessInput[indexGy] * -0.4710987 + skewnessInput[indexGz] * 1.6784097 + skewnessInput[indexTotalAcc] * -0.0449908 + skewnessInput[indexTotalGyro] * 0.4855409 + skewnessInput[indexPitch] * -1.079 + kurtosisInput[indexAx] * -0.412012 + kurtosisInput[indexAy] * -0.1104242 + kurtosisInput[indexAz] * -0.216482 + kurtosisInput[indexGx] * -1.2676485 + kurtosisInput[indexGz] * -0.0538783 + kurtosisInput[indexTotalAcc] * -0.7394039 + kurtosisInput[indexRoll] * -0.1141577 + kurtosisInput[indexPitch] * -0.0137205 + meanAbsDevInput[indexGx] * 0.1081423 + meanAbsDevInput[indexTotalAcc] * 11.2999383 + meanAbsDevInput[indexPitch] * 0.0088058 + fftMaxFreqInput[indexAx] * -0.8050617 + fftMaxFreqInput[indexAy] * 0.4776222 + fftMaxFreqInput[indexGx] * 0.9455748 + fftMaxFreqInput[indexGy] * 0.1665453 + fftMaxFreqInput[indexGz] * -0.2565616 + fftMaxFreqInput[indexRoll] * -0.8409154 + fftEnergyInput[indexAx] * -7.1845806 + fftEnergyInput[indexAy] * -1.70667 + fftEnergyInput[indexAz] * 1.6275737 + fftEnergyInput[indexGy] * -0.0006294 + fftEnergyInput[indexGz] * -0.0002211 + fftEnergyInput[indexRoll] * -0.0001292 + fftEntropyInput[indexGx] * 0.0000267 + fftEntropyInput[indexGy] * 0.0000279 + fftEntropyInput[indexTotalAcc] * -0.0733771 + fftEntropyInput[indexPitch] * -0.0000539 + covarianceAyAz * 20.1107305 + correlationAxAy * -0.2060701 + correlationAyAz * 15.7176447 + correlationGxGy * -1.4169128 + correlationGxGz * 1.2457059 + correlationGyGz * -1.1621371 ;
            Class[8] = -17.9761214 +  standardDeviationInput[indexAx] * -1.9632509 + standardDeviationInput[indexAz] * 4.0935831 + standardDeviationInput[indexGy] * -0.0355582 + standardDeviationInput[indexPitch] * -0.0450493 + meanInput[indexAx] * 1.1704787 + meanInput[indexAy] * -7.2647343 + meanInput[indexAz] * -2.9644408 + meanInput[indexGx] * -0.032992 + meanInput[indexGy] * 0.0957564 + meanInput[indexGz] * 0.0114036 + meanInput[indexRoll] * -0.0563122 + varianceInput[indexAy] * 0.3972351 + varianceInput[indexGy] * 0.0005399 + varianceInput[indexTotalGyro] * -0.0001769 + varianceInput[indexRoll] * 0.0009491 + zeroCrossingRateInput[indexAx] * 3.4688496 + zeroCrossingRateInput[indexAz] * 31.7701758 + zeroCrossingRateInput[indexGx] * -23.7200613 + zeroCrossingRateInput[indexGy] * 11.5353003 + zeroCrossingRateInput[indexGz] * 2.8602731 + zeroCrossingRateInput[indexPitch] * 3.9673483 + rootMeanSquareInput[indexAz] * 12.1730959 + rootMeanSquareInput[indexGx] * -0.0101988 + rootMeanSquareInput[indexGy] * -0.0191312 + rootMeanSquareInput[indexGz] * 0.0316036 + rootMeanSquareInput[indexRoll] * 0.0111692 + rootMeanSquareInput[indexPitch] * -0.0386504 + skewnessInput[indexAy] * 0.5928714 + skewnessInput[indexAz] * 0.3645475 + skewnessInput[indexGy] * 0.2078969 + skewnessInput[indexTotalAcc] * -0.1979223 + kurtosisInput[indexAx] * 0.1055994 + kurtosisInput[indexAy] * -0.1552278 + kurtosisInput[indexAz] * -0.0959929 + kurtosisInput[indexGx] * 0.0718344 + kurtosisInput[indexGy] * -0.096925 + kurtosisInput[indexGz] * 0.395281  + kurtosisInput[indexTotalAcc] * -0.1873724 + kurtosisInput[indexTotalGyro] * -0.4347172 + kurtosisInput[indexRoll] * 0.055272  + kurtosisInput[indexPitch] * -0.0562135 + meanAbsDevInput[indexAy] * -1.2909141 + meanAbsDevInput[indexAz] * 10.2711041 + meanAbsDevInput[indexGz] * 0.0032933 + meanAbsDevInput[indexTotalAcc] * 10.719889 + meanAbsDevInput[indexTotalGyro] * 0.0343932 + fftMaxFreqInput[indexGy] * -0.6269334 + fftMaxFreqInput[indexGz] * 0.6370308 + fftMaxFreqInput[indexPitch] * -0.2357718 + fftEnergyInput[indexAx] * -1.6273865 + fftEnergyInput[indexAz] * 0.2530668 + fftEnergyInput[indexGx] * 0.0000077 + fftEnergyInput[indexGy] * 0.0001978 + fftEnergyInput[indexRoll] * -0.0003902 + fftEntropyInput[indexAx] * -0.043252 + fftEntropyInput[indexAz] * 0.029704  + fftEntropyInput[indexGz] * -0.0000652 + fftEntropyInput[indexTotalAcc] * -0.0282315 + covarianceAxAy * -7.7954504 + covarianceAyAz * -14.599646 + covarianceGxGy * 0.000374  + covarianceGyGz * 0.0002556 + correlationAxAy * 0.939918  + correlationAyAz * 0.3120748 + correlationGxGy * -0.1174988 + correlationGxGz * -0.3056378 + correlationGyGz * -2.984801 ;
            Class[9] = -11.8674147 +  standardDeviationInput[indexTotalGyro] * 0.0920444 + standardDeviationInput[indexPitch] * 0.0687652 + meanInput[indexAx] * 5.4601132 + meanInput[indexAy] * 1.8186989 + meanInput[indexGx] * 0.1234939 + meanInput[indexGy] * -0.1619896 + meanInput[indexGz] * 0.0406616 + meanInput[indexTotalAcc] * -10.8438097 + meanInput[indexRoll] * 0.0103527 + meanInput[indexPitch] * -0.0118325 + varianceInput[indexAy] * -15.1039799 + varianceInput[indexTotalGyro] * 0.0003156 + varianceInput[indexRoll] * -0.0004715 + varianceInput[indexPitch] * -0.0014964 + zeroCrossingRateInput[indexAx] * 26.7068631 + zeroCrossingRateInput[indexAy] * 24.3058827 + zeroCrossingRateInput[indexGx] * 6.0971877 + zeroCrossingRateInput[indexGy] * 14.4220472 + zeroCrossingRateInput[indexGz] * -8.2095952 + rootMeanSquareInput[indexAx] * 1.0960706 + rootMeanSquareInput[indexGz] * 0.0062808 + skewnessInput[indexAx] * -0.7970522 + skewnessInput[indexAy] * 0.7044145 + skewnessInput[indexAz] * 0.9282054 + skewnessInput[indexGx] * -0.6586258 + skewnessInput[indexGy] * -0.9069996 + skewnessInput[indexGz] * -0.1059777 + skewnessInput[indexTotalAcc] * 0.1448602 + skewnessInput[indexTotalGyro] * 0.176447  + skewnessInput[indexPitch] * -0.5223928 + kurtosisInput[indexAx] * 0.2119121 + kurtosisInput[indexAz] * -0.841927 + kurtosisInput[indexGx] * -0.1465657 + kurtosisInput[indexGy] * 0.1190116 + kurtosisInput[indexGz] * 0.2195405 + kurtosisInput[indexTotalAcc] * -0.4352824 + kurtosisInput[indexPitch] * -0.0118244 + meanAbsDevInput[indexAx] * -5.876165 + meanAbsDevInput[indexGy] * -0.0170529 + meanAbsDevInput[indexGz] * -0.0048939 + meanAbsDevInput[indexTotalAcc] * 16.9288546 + meanAbsDevInput[indexRoll] * -0.028618 + meanAbsDevInput[indexPitch] * -0.027298 + fftMaxFreqInput[indexAx] * -1.0125434 + fftMaxFreqInput[indexAy] * -0.7872528 + fftMaxFreqInput[indexGx] * 0.293728  + fftMaxFreqInput[indexRoll] * -0.9814722 + fftEnergyInput[indexAx] * 10.214635 + fftEnergyInput[indexAz] * 4.1340003 + fftEnergyInput[indexGx] * -0.0000621 + fftEnergyInput[indexPitch] * -0.0009478 + fftEntropyInput[indexAy] * -0.0810038 + fftEntropyInput[indexAz] * 0.1178407 + fftEntropyInput[indexGx] * -0.0000302 + fftEntropyInput[indexGz] * -0.0000491 + fftEntropyInput[indexRoll] * 0.0000147 + fftEntropyInput[indexPitch] * 0.0000164 + covarianceGxGy * 0.0004115 + covarianceGyGz * 0.0003894 + correlationAxAy * -0.5049017 + correlationAyAz * -1.1340888 + correlationGyGz * -1.7043808 ;
            Class[10] = 2.0147193 +  standardDeviationInput[indexAx] * -1.7224642 + standardDeviationInput[indexGy] * -0.0016865 + standardDeviationInput[indexGz] * -0.022051 + standardDeviationInput[indexTotalAcc] * -0.3088055 + standardDeviationInput[indexTotalGyro] * -0.0544805 + meanInput[indexAx] * -1.5668239 + meanInput[indexAz] * -5.4097229 + meanInput[indexGz] * -0.0081525 + meanInput[indexTotalAcc] * 3.5717462 + meanInput[indexTotalGyro] * -0.0013886 + meanInput[indexRoll] * 0.0044041 + varianceInput[indexGx] * -0.0002079 + varianceInput[indexGy] * 0.000293  + varianceInput[indexTotalGyro] * 0.0006774 + zeroCrossingRateInput[indexAz] * -10.6077441 + zeroCrossingRateInput[indexGz] * 4.7663537 + zeroCrossingRateInput[indexPitch] * -1.4445096 + rootMeanSquareInput[indexAx] * -0.5179482 + rootMeanSquareInput[indexGy] * 0.0027888 + rootMeanSquareInput[indexTotalAcc] * 4.044928  + rootMeanSquareInput[indexPitch] * 0.0252174 + skewnessInput[indexAx] * 0.0667823 + skewnessInput[indexGy] * -0.0472155 + skewnessInput[indexTotalGyro] * -1.5313529 + skewnessInput[indexRoll] * 0.0256419 + kurtosisInput[indexAy] * 0.0280061 + kurtosisInput[indexAz] * 0.3099629 + kurtosisInput[indexGx] * 0.0611455 + kurtosisInput[indexTotalAcc] * 0.3025037 + kurtosisInput[indexTotalGyro] * 0.4831543 + meanAbsDevInput[indexAy] * -3.0551309 + meanAbsDevInput[indexGx] * -0.0176948 + meanAbsDevInput[indexTotalAcc] * -5.0910568 + meanAbsDevInput[indexTotalGyro] * -0.0447785 + meanAbsDevInput[indexPitch] * -0.00434 + fftMaxFreqInput[indexAx] * 0.2953277 + fftMaxFreqInput[indexAy] * 0.1678222 + fftMaxFreqInput[indexGz] * 0.2192519 + fftMaxFreqInput[indexRoll] * -0.0756497 + fftEnergyInput[indexAx] * 3.4344532 + fftEnergyInput[indexAy] * 1.1305325 + fftEnergyInput[indexAz] * -0.3298807 + fftEnergyInput[indexGx] * 0.0000176 + fftEnergyInput[indexTotalAcc] * 1.6509949 + fftEnergyInput[indexTotalGyro] * 0.0000067 + fftEnergyInput[indexRoll] * 0.0002565 + fftEnergyInput[indexPitch] * 0.0000889 + fftEntropyInput[indexAx] * -0.0207207 + fftEntropyInput[indexAz] * 0.0548466 + covarianceGxGz * 0.0000327 + covarianceGyGz * -0.0001898 + correlationGxGy * 0.5994502 + correlationGyGz * 0.1257236;

            //Gesture Hybrid Model
            /*
            Class[0] = -15.2099158 +  standardDeviationInput[indexGx] * 0.0073174 + standardDeviationInput[indexTotalAcc] * -5.1401312 + standardDeviationInput[indexTotalGyro] * -0.0125584 + standardDeviationInput[indexRoll] * -0.0110749 + meanInput[indexAy] * -2.0183318 + meanInput[indexGy] * 0.0724881 + meanInput[indexGz] * -0.0251399 + meanInput[indexTotalAcc] * 19.1983604 + meanInput[indexTotalGyro] * 0.0566464 + meanInput[indexRoll] * -0.0319208 + meanInput[indexPitch] * 0.0020361 + varianceInput[indexAx] * 16.4974588 + varianceInput[indexAz] * 4.4192797 + varianceInput[indexGy] * 0.0002792 + varianceInput[indexGz] * -0.0007053 + varianceInput[indexTotalAcc] * -25.2485822 + varianceInput[indexTotalGyro] * -0.0000801 + varianceInput[indexPitch] * -0.0002175 + zeroCrossingRateInput[indexAz] * -2.655405 + zeroCrossingRateInput[indexGx] * 3.5316688 + zeroCrossingRateInput[indexGy] * -6.6617161 + zeroCrossingRateInput[indexGz] * 8.9367997 + zeroCrossingRateInput[indexPitch] * 4.7102232 + rootMeanSquareInput[indexAy] * -3.3934221 + rootMeanSquareInput[indexAz] * -3.7618767 + rootMeanSquareInput[indexGz] * -0.0408998 + rootMeanSquareInput[indexRoll] * 0.0092567 + rootMeanSquareInput[indexPitch] * 0.0630323 + skewnessInput[indexAx] * 0.416129  + skewnessInput[indexAy] * -0.866679 + skewnessInput[indexAz] * 0.7957154 + skewnessInput[indexGx] * -0.0973567 + skewnessInput[indexGy] * -0.1603618 + skewnessInput[indexTotalAcc] * 0.6365699 + skewnessInput[indexTotalGyro] * -0.4242956 + skewnessInput[indexRoll] * 0.0878943 + skewnessInput[indexPitch] * -0.0813058 + kurtosisInput[indexAx] * 0.0447197 + kurtosisInput[indexAz] * 0.0152216 + kurtosisInput[indexGx] * 0.0757301 + kurtosisInput[indexGy] * -0.0110505 + kurtosisInput[indexTotalAcc] * -0.0590743 + kurtosisInput[indexTotalGyro] * 0.1512014 + kurtosisInput[indexRoll] * 0.0340383 + meanAbsDevInput[indexAx] * -18.4266695 + meanAbsDevInput[indexAy] * 4.0635294 + meanAbsDevInput[indexAz] * 3.2794133 + meanAbsDevInput[indexGy] * -0.1368946 + meanAbsDevInput[indexGz] * -0.0283649 + meanAbsDevInput[indexTotalAcc] * 1.1238124 + meanAbsDevInput[indexRoll] * 0.0126495 + meanAbsDevInput[indexPitch] * 0.001892  + fftMaxFreqInput[indexAx] * 0.0602325 + fftMaxFreqInput[indexGx] * -0.1167103 + fftMaxFreqInput[indexGy] * 0.1382525 + fftMaxFreqInput[indexGz] * -0.2299537 + fftMaxFreqInput[indexRoll] * 0.2879725 + fftMaxFreqInput[indexPitch] * 0.4740931 + fftEnergyInput[indexAx] * -8.8943138 + fftEnergyInput[indexAy] * 1.504563  + fftEnergyInput[indexGy] * -0.0003176 + fftEnergyInput[indexGz] * -0.000162 + fftEnergyInput[indexRoll] * 0.0000157 + fftEnergyInput[indexPitch] * 0.0000751 + fftEntropyInput[indexAy] * 0.0484513 + fftEntropyInput[indexAz] * 0.0713863 + fftEntropyInput[indexGz] * 0.0000512 + fftEntropyInput[indexTotalAcc] * -0.0866653 + fftEntropyInput[indexPitch] * 0.0000046 + covarianceAxAy * 7.8448986 + covarianceAyAz * -0.4505915 + covarianceGxGy * -0.0005803 + covarianceGxGz * -0.000512 + covarianceGyGz * 0.0009359 + correlationAyAz * 1.2141335 + correlationGxGy * 0.4487862 + correlationGxGz * 0.6441598 + correlationGyGz * -1.1542303 ;
            Class[1] = 17.1230581 +  standardDeviationInput[indexAx] * -6.9296224 + standardDeviationInput[indexGx] * 0.0382044 + standardDeviationInput[indexGy] * 0.0124598 + standardDeviationInput[indexRoll] * -0.0535617 + standardDeviationInput[indexPitch] * 0.0298825 + meanInput[indexAx] * 7.7964717 + meanInput[indexAy] * 1.4425731 + meanInput[indexAz] * -1.7836046 + meanInput[indexGx] * -0.0086472 + meanInput[indexGy] * 0.0171927 + meanInput[indexGz] * -0.046246 + meanInput[indexTotalAcc] * -5.8158999 + meanInput[indexRoll] * 0.0544843 + meanInput[indexPitch] * 0.0310398 + varianceInput[indexAx] * 5.2865221 + varianceInput[indexAz] * -3.7841703 + varianceInput[indexGx] * -0.0000328 + varianceInput[indexGy] * 0.000103  + varianceInput[indexGz] * -0.0000369 + varianceInput[indexTotalAcc] * 9.7490753 + varianceInput[indexTotalGyro] * -0.0000957 + varianceInput[indexRoll] * 0.0017139 + zeroCrossingRateInput[indexAz] * 12.3042491 + zeroCrossingRateInput[indexGx] * -10.5634491 + zeroCrossingRateInput[indexGy] * -12.4612372 + zeroCrossingRateInput[indexGz] * 17.7302919 + zeroCrossingRateInput[indexPitch] * -2.1930216 + rootMeanSquareInput[indexAx] * 0.3670427 + rootMeanSquareInput[indexAz] * -8.6644129 + rootMeanSquareInput[indexGz] * -0.1927386 + rootMeanSquareInput[indexRoll] * -0.0108047 + rootMeanSquareInput[indexPitch] * 0.0093925 + skewnessInput[indexAx] * 0.0708902 + skewnessInput[indexAy] * 1.1743899 + skewnessInput[indexAz] * 0.2486043 + skewnessInput[indexGx] * -0.4348851 + skewnessInput[indexGy] * -0.0638506 + skewnessInput[indexGz] * -0.1900889 + skewnessInput[indexTotalAcc] * -1.2041215 + skewnessInput[indexTotalGyro] * 0.7907337 + skewnessInput[indexRoll] * 0.2605154 + skewnessInput[indexPitch] * 1.3483021 + kurtosisInput[indexAx] * 0.0341199 + kurtosisInput[indexAz] * -0.1101442 + kurtosisInput[indexGx] * -0.5677211 + kurtosisInput[indexGy] * 0.1298106 + kurtosisInput[indexGz] * 0.0614179 + kurtosisInput[indexTotalAcc] * -0.3742313 + kurtosisInput[indexTotalGyro] * -0.6530977 + kurtosisInput[indexRoll] * -0.0655129 + kurtosisInput[indexPitch] * -0.7334998 + meanAbsDevInput[indexAy] * 11.7908493 + meanAbsDevInput[indexGx] * 0.0147904 + meanAbsDevInput[indexGz] * -0.0258436 + meanAbsDevInput[indexTotalAcc] * 2.1255847 + meanAbsDevInput[indexRoll] * -0.1539271 + fftMaxFreqInput[indexAx] * -0.4577947 + fftMaxFreqInput[indexAy] * 0.4037069 + fftMaxFreqInput[indexGx] * 0.2184925 + fftMaxFreqInput[indexGz] * -0.0958431 + fftMaxFreqInput[indexRoll] * 0.7197489 + fftMaxFreqInput[indexPitch] * -1.0879777 + fftEnergyInput[indexAy] * -0.7710177 + fftEnergyInput[indexGy] * 0.0003365 + fftEnergyInput[indexTotalAcc] * -0.4169328 + fftEnergyInput[indexRoll] * -0.0004875 + fftEntropyInput[indexAy] * 0.013723  + fftEntropyInput[indexAz] * -0.073917 + fftEntropyInput[indexGx] * 0.000031  + fftEntropyInput[indexTotalAcc] * 0.0171679 + fftEntropyInput[indexTotalGyro] * -0.0000216 + covarianceAxAy * -3.0340191 + covarianceAyAz * -4.7361349 + covarianceGxGy * 0.0001142 + covarianceGxGz * -0.0008309 + covarianceGyGz * -0.0005841 + correlationAxAy * 0.1525901 + correlationAyAz * -1.702185 + correlationGxGy * -1.6435196 + correlationGxGz * -1.6108771 + correlationGyGz * -0.6829774 ;
            Class[2] = -17.8817965 +  standardDeviationInput[indexGy] * 0.0783124 + standardDeviationInput[indexGz] * 0.0290191 + standardDeviationInput[indexTotalAcc] * -7.9627934 + meanInput[indexAy] * 2.0634832 + meanInput[indexAz] * 1.1360547 + meanInput[indexGx] * -0.0231661 + meanInput[indexGy] * -0.0993576 + meanInput[indexTotalAcc] * 1.1113988 + meanInput[indexRoll] * 0.0027004 + meanInput[indexPitch] * 0.0592921 + varianceInput[indexAy] * 0.8623594 + varianceInput[indexAz] * 22.8342932 + varianceInput[indexGx] * 0.0000537 + varianceInput[indexGz] * 0.0002638 + varianceInput[indexTotalAcc] * 20.5186317 + varianceInput[indexRoll] * -0.0013276 + zeroCrossingRateInput[indexAx] * 3.4031973 + zeroCrossingRateInput[indexAz] * 62.0331939 + zeroCrossingRateInput[indexGx] * 15.9931892 + zeroCrossingRateInput[indexGy] * 8.4819722 + zeroCrossingRateInput[indexGz] * -2.2304626 + rootMeanSquareInput[indexAx] * 0.3279348 + rootMeanSquareInput[indexAy] * -2.0654934 + rootMeanSquareInput[indexAz] * 1.6521694 + rootMeanSquareInput[indexGz] * 0.042505  + rootMeanSquareInput[indexTotalAcc] * 9.848615  + rootMeanSquareInput[indexTotalGyro] * 0.0010515 + skewnessInput[indexAx] * -0.9818696 + skewnessInput[indexAy] * -0.5788617 + skewnessInput[indexAz] * 0.0894332 + skewnessInput[indexGy] * 0.1676444 + skewnessInput[indexGz] * 0.2442058 + skewnessInput[indexTotalAcc] * 0.1589457 + skewnessInput[indexTotalGyro] * 0.3891029 + skewnessInput[indexRoll] * 0.3940821 + skewnessInput[indexPitch] * -0.3496998 + kurtosisInput[indexAx] * 0.0349979 + kurtosisInput[indexAy] * 0.1575517 + kurtosisInput[indexAz] * -0.1301507 + kurtosisInput[indexGx] * 0.1760041 + kurtosisInput[indexGy] * 0.1275875 + kurtosisInput[indexGz] * -0.3326906 + kurtosisInput[indexTotalAcc] * 0.0727369 + kurtosisInput[indexTotalGyro] * -0.0089142 + kurtosisInput[indexRoll] * -0.6259912 + meanAbsDevInput[indexAx] * 0.198247  + meanAbsDevInput[indexAy] * 0.687658  + meanAbsDevInput[indexAz] * -31.0815934 + meanAbsDevInput[indexGx] * -0.0033482 + meanAbsDevInput[indexTotalAcc] * -48.7555326 + meanAbsDevInput[indexTotalGyro] * -0.0205862 + fftMaxFreqInput[indexAx] * -0.2546789 + fftMaxFreqInput[indexAz] * -20.5086406 + fftMaxFreqInput[indexGx] * -0.2233337 + fftMaxFreqInput[indexGz] * 0.2438554 + fftMaxFreqInput[indexPitch] * -0.2136938 + fftEnergyInput[indexAx] * 1.6886055 + fftEnergyInput[indexAy] * -5.1080406 + fftEnergyInput[indexGx] * 0.0000285 + fftEnergyInput[indexGy] * -0.0005884 + fftEnergyInput[indexGz] * -0.0001114 + fftEnergyInput[indexTotalAcc] * 2.2504697 + fftEntropyInput[indexAx] * 0.0109206 + fftEntropyInput[indexAz] * -0.0745462 + fftEntropyInput[indexGy] * -0.0000232 + fftEntropyInput[indexGz] * -0.0000046 + fftEntropyInput[indexRoll] * 0.0000744 + fftEntropyInput[indexPitch] * -0.0002151 + covarianceAxAy * -4.0161257 + covarianceAyAz * 8.2032658 + covarianceGxGy * 0.0001195 + covarianceGxGz * 0.0000968 + covarianceGyGz * -0.0002494 + correlationAxAy * 0.6140326 + correlationGxGy * -0.1576962 + correlationGxGz * 0.5161036 + correlationGyGz * 1.5765352 ;
            Class[3] = -36.5261283 +  standardDeviationInput[indexAy] * -1.5317217 + standardDeviationInput[indexAz] * 5.3842477 + standardDeviationInput[indexGy] * 0.0421197 + standardDeviationInput[indexGz] * 0.1036391 + standardDeviationInput[indexTotalAcc] * -12.7474236 + meanInput[indexAx] * 0.4076779 + meanInput[indexAy] * -0.7513556 + meanInput[indexAz] * 2.3618234 + meanInput[indexGx] * -0.0228111 + meanInput[indexGy] * 0.0047776 + meanInput[indexTotalAcc] * 34.0714537 + meanInput[indexPitch] * -0.0043194 + varianceInput[indexAx] * -10.876624 + varianceInput[indexAy] * -8.0227542 + varianceInput[indexGx] * -0.0001458 + varianceInput[indexGz] * -0.0003194 + varianceInput[indexTotalAcc] * -12.8563568 + varianceInput[indexTotalGyro] * -0.0004542 + zeroCrossingRateInput[indexAx] * 5.2879217 + zeroCrossingRateInput[indexAz] * 18.2574957 + zeroCrossingRateInput[indexGx] * 8.4782769 + zeroCrossingRateInput[indexGy] * 7.1475002 + zeroCrossingRateInput[indexGz] * -9.2752785 + zeroCrossingRateInput[indexPitch] * -1.1633398 + rootMeanSquareInput[indexAx] * 2.1232021 + rootMeanSquareInput[indexAy] * 3.680611  + rootMeanSquareInput[indexPitch] * 0.0329149 + skewnessInput[indexAx] * 2.1960757 + skewnessInput[indexAy] * 0.7590701 + skewnessInput[indexAz] * -0.7653457 + skewnessInput[indexGz] * -1.2866002 + skewnessInput[indexTotalAcc] * -0.0542073 + skewnessInput[indexTotalGyro] * 0.5213831 + skewnessInput[indexRoll] * -0.7243605 + kurtosisInput[indexAx] * -0.5498338 + kurtosisInput[indexAy] * -0.2171974 + kurtosisInput[indexAz] * 0.0331303 + kurtosisInput[indexGx] * -0.0825463 + kurtosisInput[indexGy] * 0.2139633 + kurtosisInput[indexGz] * -0.8937335 + kurtosisInput[indexTotalAcc] * 0.0380429 + kurtosisInput[indexTotalGyro] * -0.5486272 + kurtosisInput[indexRoll] * -0.5288438 + kurtosisInput[indexPitch] * -0.0193574 + meanAbsDevInput[indexAx] * 4.4140987 + meanAbsDevInput[indexAy] * -3.8505617 + meanAbsDevInput[indexAz] * 0.8896787 + meanAbsDevInput[indexGx] * -0.0189729 + meanAbsDevInput[indexGz] * 0.0603049 + meanAbsDevInput[indexTotalAcc] * -14.8554831 + fftMaxFreqInput[indexAy] * 0.2722719 + fftMaxFreqInput[indexAz] * -7.8073567 + fftMaxFreqInput[indexGx] * -0.0948815 + fftMaxFreqInput[indexGy] * 0.1527191 + fftMaxFreqInput[indexGz] * -0.3513466 + fftMaxFreqInput[indexRoll] * 0.1488319 + fftMaxFreqInput[indexPitch] * 0.7017957 + fftEnergyInput[indexAy] * -4.188224 + fftEnergyInput[indexGx] * -0.0000199 + fftEnergyInput[indexGy] * -0.0005454 + fftEnergyInput[indexTotalGyro] * -0.0001101 + fftEntropyInput[indexAx] * 0.01045   + fftEntropyInput[indexAy] * -0.0752788 + fftEntropyInput[indexAz] * 0.0649136 + fftEntropyInput[indexGx] * 0.0000557 + fftEntropyInput[indexGz] * -0.0000537 + fftEntropyInput[indexTotalAcc] * -0.0863877 + fftEntropyInput[indexPitch] * -0.0000349 + covarianceAxAy * -22.6180481 + covarianceAyAz * 0.528286  + covarianceGxGy * -0.0006422 + covarianceGxGz * 0.0004031 + correlationAxAy * -0.1639051 + correlationGxGy * 0.4229403 + correlationGxGz * -1.5489255 + correlationGyGz * -1.1101616 ;
            Class[4] = 2.3098227 +  standardDeviationInput[indexAx] * 4.9691189 + standardDeviationInput[indexAy] * 1.6741012 + standardDeviationInput[indexGx] * 0.0859112 + standardDeviationInput[indexGy] * 0.0054632 + standardDeviationInput[indexGz] * 0.031171  + standardDeviationInput[indexTotalAcc] * 1.3785931 + standardDeviationInput[indexRoll] * 0.0385877 + meanInput[indexAx] * -0.6693015 + meanInput[indexAy] * -2.346534 + meanInput[indexAz] * 1.4880912 + meanInput[indexGx] * -0.0153665 + meanInput[indexGy] * -0.1324998 + meanInput[indexGz] * -0.0364436 + meanInput[indexTotalAcc] * -7.2577031 + meanInput[indexRoll] * -0.0984808 + meanInput[indexPitch] * 0.0045603 + varianceInput[indexGy] * -0.0000908 + varianceInput[indexGz] * -0.0007051 + varianceInput[indexTotalGyro] * -0.0005167 + varianceInput[indexPitch] * -0.0002074 + zeroCrossingRateInput[indexAy] * 3.7797418 + zeroCrossingRateInput[indexAz] * -67.4418487 + zeroCrossingRateInput[indexGx] * -2.3986008 + zeroCrossingRateInput[indexGy] * -6.4969481 + zeroCrossingRateInput[indexGz] * -10.4834985 + rootMeanSquareInput[indexAx] * -4.6575583 + rootMeanSquareInput[indexAz] * 4.4761627 + skewnessInput[indexAx] * 0.5189591 + skewnessInput[indexAy] * 2.1036626 + skewnessInput[indexGx] * -0.7143724 + skewnessInput[indexGy] * -0.2150197 + skewnessInput[indexGz] * 0.0947738 + skewnessInput[indexTotalAcc] * 0.4375931 + skewnessInput[indexTotalGyro] * 0.5536052 + skewnessInput[indexRoll] * 0.0973153 + skewnessInput[indexPitch] * 0.7115767 + kurtosisInput[indexAx] * -0.0161991 + kurtosisInput[indexAy] * -1.1162172 + kurtosisInput[indexAz] * -0.7117147 + kurtosisInput[indexGx] * -0.6654347 + kurtosisInput[indexTotalGyro] * 0.4287465 + kurtosisInput[indexRoll] * -0.0710093 + kurtosisInput[indexPitch] * 0.0429337 + meanAbsDevInput[indexAx] * 4.1170702 + meanAbsDevInput[indexAy] * 8.8607363 + meanAbsDevInput[indexGx] * -0.027647 + meanAbsDevInput[indexTotalGyro] * -0.0595164 + meanAbsDevInput[indexPitch] * -0.063568 + fftMaxFreqInput[indexAx] * 0.1631311 + fftMaxFreqInput[indexAy] * 1.3238418 + fftMaxFreqInput[indexGx] * 0.9496947 + fftMaxFreqInput[indexGz] * -0.1807692 + fftMaxFreqInput[indexPitch] * -0.5973283 + fftEnergyInput[indexAx] * 4.0883686 + fftEnergyInput[indexAy] * -0.6432712 + fftEnergyInput[indexGz] * -0.0001066 + fftEnergyInput[indexTotalGyro] * -0.000046 + fftEnergyInput[indexPitch] * -0.0000506 + fftEntropyInput[indexAx] * -0.0161664 + fftEntropyInput[indexAy] * -0.0189321 + fftEntropyInput[indexAz] * 0.015318  + fftEntropyInput[indexGx] * -0.0000135 + fftEntropyInput[indexGy] * -0.0000188 + fftEntropyInput[indexGz] * -0.0000328 + fftEntropyInput[indexTotalGyro] * -0.0000268 + covarianceAxAy * 14.6841026 + covarianceAyAz * 1.3558717 + covarianceGxGy * 0.0001791 + covarianceGxGz * 0.0000257 + covarianceGyGz * -0.0006488 + correlationAxAy * -0.6908407 + correlationAyAz * -1.6985051 + correlationGxGy * -0.2535685 + correlationGxGz * 0.4388712 + correlationGyGz * 1.7737169 ;
            Class[5] = 2.5945564 +  standardDeviationInput[indexGx] * 0.0070425 + standardDeviationInput[indexGz] * 0.0087766 + standardDeviationInput[indexTotalAcc] * -0.6049968 + meanInput[indexAx] * -1.0242897 + meanInput[indexAy] * -2.804291 + meanInput[indexAz] * -2.3768629 + meanInput[indexGx] * 0.0136868 + meanInput[indexGy] * 0.087918  + meanInput[indexGz] * 0.0091669 + meanInput[indexTotalAcc] * -1.5059144 + meanInput[indexRoll] * -0.0020306 + meanInput[indexPitch] * -0.0108936 + varianceInput[indexAx] * 4.3953177 + varianceInput[indexAy] * -14.5348774 + varianceInput[indexGy] * 0.0007463 + varianceInput[indexGz] * -0.0001247 + varianceInput[indexTotalAcc] * 7.9455445 + varianceInput[indexTotalGyro] * -0.0006309 + varianceInput[indexRoll] * -0.0010512 + varianceInput[indexPitch] * -0.0003884 + zeroCrossingRateInput[indexAx] * 4.3320034 + zeroCrossingRateInput[indexAz] * 46.1674777 + zeroCrossingRateInput[indexGx] * -11.3012861 + zeroCrossingRateInput[indexGz] * -12.0778457 + zeroCrossingRateInput[indexPitch] * -15.4001837 + rootMeanSquareInput[indexGy] * -0.0710253 + rootMeanSquareInput[indexTotalGyro] * -0.0007971 + rootMeanSquareInput[indexRoll] * -0.0060487 + rootMeanSquareInput[indexPitch] * 0.0199313 + skewnessInput[indexAx] * -0.5306066 + skewnessInput[indexAy] * 0.0541507 + skewnessInput[indexAz] * 0.3283286 + skewnessInput[indexGx] * 0.221873  + skewnessInput[indexGy] * 0.2590452 + skewnessInput[indexTotalAcc] * 0.1045235 + skewnessInput[indexTotalGyro] * 0.1681541 + skewnessInput[indexPitch] * 0.8504392 + kurtosisInput[indexAy] * -0.3103752 + kurtosisInput[indexAz] * -0.4249081 + kurtosisInput[indexGx] * 0.1001302 + kurtosisInput[indexGy] * -0.0262163 + kurtosisInput[indexGz] * 0.10802   + kurtosisInput[indexTotalAcc] * -0.0168399 + kurtosisInput[indexTotalGyro] * 0.2204825 + kurtosisInput[indexRoll] * -0.0500326 + kurtosisInput[indexPitch] * 0.0191444 + meanAbsDevInput[indexAx] * 4.2160741 + meanAbsDevInput[indexAy] * 20.0702857 + meanAbsDevInput[indexAz] * 3.8405456 + meanAbsDevInput[indexGy] * -0.0093937 + meanAbsDevInput[indexRoll] * -0.0455778 + fftMaxFreqInput[indexAx] * -0.3221868 + fftMaxFreqInput[indexAy] * 1.0472422 + fftMaxFreqInput[indexAz] * -4.8699411 + fftMaxFreqInput[indexGy] * 0.1436197 + fftMaxFreqInput[indexRoll] * 0.4437495 + fftMaxFreqInput[indexPitch] * -0.4358752 + fftEnergyInput[indexGy] * -0.0001027 + fftEnergyInput[indexGz] * -0.0000632 + fftEnergyInput[indexRoll] * -0.0000271 + fftEntropyInput[indexAx] * 0.0146128 + fftEntropyInput[indexAy] * 0.0149285 + fftEntropyInput[indexGz] * -0.0000333 + fftEntropyInput[indexTotalAcc] * -0.0141477 + fftEntropyInput[indexPitch] * -0.0000793 + covarianceAyAz * 5.61773   + covarianceGxGy * 0.0001858 + covarianceGyGz * 0.0003683 + correlationAxAy * 0.0832072 + correlationAyAz * -2.5597269 + correlationGxGy * -0.6977018 + correlationGxGz * 0.657137  + correlationGyGz * 0.755977  ;
            Class[6] = 1.5701406 +  standardDeviationInput[indexAx] * 0.3027297 + standardDeviationInput[indexAy] * 0.1939976 + standardDeviationInput[indexRoll] * 0.0053028 + standardDeviationInput[indexPitch] * 0.0066708 + meanInput[indexAx] * -2.9910801 + meanInput[indexAy] * 1.094858  + meanInput[indexAz] * 3.2045445 + meanInput[indexGx] * 0.018564  + meanInput[indexGy] * 0.0416693 + meanInput[indexGz] * 0.0440837 + meanInput[indexTotalAcc] * -1.7979552 + meanInput[indexRoll] * 0.0388594 + meanInput[indexPitch] * 0.0349696 + varianceInput[indexAx] * 17.1241671 + varianceInput[indexAy] * -4.4161927 + varianceInput[indexGx] * -0.0000916 + varianceInput[indexGz] * 0.0000638 + varianceInput[indexTotalAcc] * 0.5805166 + varianceInput[indexTotalGyro] * 0.0000678 + varianceInput[indexPitch] * -0.0007468 + zeroCrossingRateInput[indexAx] * -0.7490653 + zeroCrossingRateInput[indexAy] * -11.5149265 + zeroCrossingRateInput[indexAz] * -23.8542568 + zeroCrossingRateInput[indexGx] * -18.4283916 + zeroCrossingRateInput[indexGz] * -3.8391416 + rootMeanSquareInput[indexAx] * 2.3050033 + rootMeanSquareInput[indexAy] * -0.3144457 + rootMeanSquareInput[indexAz] * 0.9763248 + rootMeanSquareInput[indexGx] * -0.0010301 + rootMeanSquareInput[indexGy] * -0.001315 + skewnessInput[indexAx] * 0.5202656 + skewnessInput[indexAy] * 1.5174768 + skewnessInput[indexAz] * 0.5023059 + skewnessInput[indexGx] * 0.9016292 + skewnessInput[indexGy] * 0.1531581 + skewnessInput[indexGz] * -0.0971158 + skewnessInput[indexTotalAcc] * 0.1356312 + skewnessInput[indexTotalGyro] * 1.0843735 + kurtosisInput[indexAx] * 0.1572219 + kurtosisInput[indexAy] * -1.0661779 + kurtosisInput[indexGx] * -1.8696576 + kurtosisInput[indexGy] * 0.0398372 + kurtosisInput[indexGz] * 0.2059323 + kurtosisInput[indexTotalAcc] * -0.5249087 + kurtosisInput[indexTotalGyro] * 0.0512095 + kurtosisInput[indexRoll] * -0.154873 + kurtosisInput[indexPitch] * 0.0392583 + meanAbsDevInput[indexAx] * -14.0713146 + meanAbsDevInput[indexGx] * 0.0429627 + meanAbsDevInput[indexGy] * 0.001956  + meanAbsDevInput[indexGz] * -0.0399275 + meanAbsDevInput[indexPitch] * 0.1680051 + fftMaxFreqInput[indexAx] * -0.0986238 + fftMaxFreqInput[indexAy] * -0.4474056 + fftMaxFreqInput[indexAz] * -3.0379505 + fftMaxFreqInput[indexGx] * 0.2201288 + fftMaxFreqInput[indexGz] * -0.1637825 + fftMaxFreqInput[indexRoll] * -0.1958005 + fftEnergyInput[indexAx] * 0.5064005 + fftEnergyInput[indexAy] * 0.6908674 + fftEnergyInput[indexGx] * 0.0000255 + fftEnergyInput[indexGz] * 0.0000093 + fftEnergyInput[indexPitch] * -0.0001172 + fftEntropyInput[indexAx] * -0.000542 + fftEntropyInput[indexAy] * -0.0107942 + fftEntropyInput[indexGy] * 0.0000177 + fftEntropyInput[indexTotalAcc] * 0.0031719 + fftEntropyInput[indexTotalGyro] * 0.0000906 + fftEntropyInput[indexRoll] * -0.000004 + covarianceAxAy * -13.130858 + covarianceAyAz * -2.2924598 + covarianceGxGy * 0.00014   + covarianceGxGz * -0.0003604 + covarianceGyGz * -0.0007694 + correlationAxAy * -0.979909 + correlationAyAz * 5.4486182 + correlationGxGz * -0.1058425 + correlationGyGz * 2.387233  ;
            Class[7] = 0.1353749 +  standardDeviationInput[indexTotalAcc] * 13.3604293 + meanInput[indexAy] * 1.8712952 + meanInput[indexAz] * 2.542238  + meanInput[indexGx] * -0.0184856 + meanInput[indexGy] * -0.0737135 + meanInput[indexTotalAcc] * -5.8164913 + meanInput[indexRoll] * 0.0231897 + varianceInput[indexAx] * 0.5005724 + varianceInput[indexAy] * -22.4677664 + varianceInput[indexAz] * -4.5907748 + varianceInput[indexGx] * -0.0000342 + varianceInput[indexGy] * 0.0007967 + varianceInput[indexGz] * 0.0000155 + varianceInput[indexRoll] * 0.0003271 + varianceInput[indexPitch] * -0.00002 + zeroCrossingRateInput[indexAx] * 0.5476717 + zeroCrossingRateInput[indexAz] * -21.9788563 + zeroCrossingRateInput[indexGx] * -2.0740867 + zeroCrossingRateInput[indexGy] * 6.9988631 + zeroCrossingRateInput[indexGz] * -2.6191496 + zeroCrossingRateInput[indexPitch] * -5.7756205 + rootMeanSquareInput[indexAx] * -3.3557439 + rootMeanSquareInput[indexAy] * 1.8341988 + rootMeanSquareInput[indexGy] * -0.1169355 + rootMeanSquareInput[indexPitch] * 0.0329029 + skewnessInput[indexAx] * 0.2399691 + skewnessInput[indexAy] * -0.4333174 + skewnessInput[indexAz] * 0.8196572 + skewnessInput[indexGx] * 0.082685  + skewnessInput[indexGy] * -0.3578281 + skewnessInput[indexGz] * 0.6105312 + skewnessInput[indexTotalAcc] * -0.6836307 + skewnessInput[indexTotalGyro] * 1.5080974 + skewnessInput[indexRoll] * 0.441026  + skewnessInput[indexPitch] * -0.6380388 + kurtosisInput[indexAx] * -0.1195935 + kurtosisInput[indexAy] * 0.0043079 + kurtosisInput[indexAz] * -0.1935005 + kurtosisInput[indexGx] * -0.7903552 + kurtosisInput[indexGy] * 0.1062677 + kurtosisInput[indexGz] * -0.1895829 + kurtosisInput[indexTotalAcc] * -0.2098429 + kurtosisInput[indexRoll] * 0.0283844 + kurtosisInput[indexPitch] * -0.0779699 + meanAbsDevInput[indexAy] * 10.6644043 + meanAbsDevInput[indexGx] * 0.0245849 + meanAbsDevInput[indexGz] * 0.0030693 + meanAbsDevInput[indexTotalAcc] * 3.7784687 + meanAbsDevInput[indexPitch] * 0.0628054 + fftMaxFreqInput[indexAx] * -0.5816562 + fftMaxFreqInput[indexAy] * 0.4872516 + fftMaxFreqInput[indexAz] * -1.0344084 + fftMaxFreqInput[indexGx] * 0.5413338 + fftMaxFreqInput[indexGy] * 0.0844728 + fftMaxFreqInput[indexRoll] * 0.260257  + fftMaxFreqInput[indexPitch] * 0.5215516 + fftEnergyInput[indexAx] * -1.6527961 + fftEnergyInput[indexGx] * -0.0000329 + fftEnergyInput[indexGy] * -0.0000531 + fftEnergyInput[indexGz] * 0.0000066 + fftEnergyInput[indexRoll] * -0.0000731 + fftEntropyInput[indexAx] * -0.0653466 + fftEntropyInput[indexAy] * -0.0221192 + fftEntropyInput[indexGx] * 0.0000218 + fftEntropyInput[indexTotalAcc] * -0.0130513 + fftEntropyInput[indexTotalGyro] * 0.0000174 + fftEntropyInput[indexPitch] * 0.0000078 + covarianceAxAy * -15.5522814 + covarianceAyAz * 5.4794972 + covarianceGxGy * -0.0002165 + covarianceGxGz * 0.000033  + covarianceGyGz * -0.0001344 + correlationAxAy * 1.7105243 + correlationAyAz * 0.3530163 + correlationGxGy * 0.3455513 + correlationGxGz * 1.7198179 + correlationGyGz * -0.4255119 ;
            Class[8] = -3.3972459 +  standardDeviationInput[indexAx] * 9.0421251 + standardDeviationInput[indexGy] * -0.0805333 + standardDeviationInput[indexGz] * 0.018168  + meanInput[indexAz] * 0.2228902 + meanInput[indexGx] * 0.0167373 + meanInput[indexGy] * 0.0147767 + meanInput[indexGz] * 0.014171  + meanInput[indexTotalAcc] * 2.6098956 + meanInput[indexRoll] * -0.0182362 + meanInput[indexPitch] * -0.0219252 + varianceInput[indexAx] * -5.5769012 + varianceInput[indexAy] * 11.5855687 + varianceInput[indexAz] * -3.8581879 + varianceInput[indexGx] * -0.000017 + varianceInput[indexGy] * -0.0000829 + varianceInput[indexGz] * -0.0001647 + varianceInput[indexTotalAcc] * -0.4907512 + varianceInput[indexPitch] * 0.0000767 + zeroCrossingRateInput[indexAy] * -9.3584324 + zeroCrossingRateInput[indexAz] * 13.1097825 + zeroCrossingRateInput[indexGx] * 2.4251387 + zeroCrossingRateInput[indexGy] * 5.3552944 + zeroCrossingRateInput[indexGz] * -1.1324975 + rootMeanSquareInput[indexAx] * -0.6661562 + rootMeanSquareInput[indexGy] * 0.0775975 + rootMeanSquareInput[indexPitch] * -0.0040607 + skewnessInput[indexAx] * 0.2156989 + skewnessInput[indexAz] * 0.3619108 + skewnessInput[indexGx] * 0.13537   + skewnessInput[indexGy] * 1.4344752 + skewnessInput[indexGz] * -0.111885 + skewnessInput[indexTotalAcc] * -0.2767497 + skewnessInput[indexTotalGyro] * -1.9681154 + skewnessInput[indexRoll] * -0.1743344 + skewnessInput[indexPitch] * -0.1883421 + kurtosisInput[indexAx] * 0.1150677 + kurtosisInput[indexAy] * 0.1925195 + kurtosisInput[indexAz] * -0.1836798 + kurtosisInput[indexGx] * -0.2393401 + kurtosisInput[indexGy] * -0.116914 + kurtosisInput[indexGz] * 0.2076556 + kurtosisInput[indexTotalAcc] * -0.3066442 + kurtosisInput[indexTotalGyro] * 0.0250294 + kurtosisInput[indexRoll] * 0.021465  + kurtosisInput[indexPitch] * -0.0766696 + meanAbsDevInput[indexAy] * -19.0807461 + meanAbsDevInput[indexAz] * 0.6573299 + meanAbsDevInput[indexGy] * 0.0300874 + meanAbsDevInput[indexTotalGyro] * 0.0048112 + fftMaxFreqInput[indexAy] * 0.5193097 + fftMaxFreqInput[indexGx] * -0.4902031 + fftMaxFreqInput[indexGy] * 0.0560373 + fftMaxFreqInput[indexGz] * 0.3619416 + fftMaxFreqInput[indexRoll] * 0.4495913 + fftMaxFreqInput[indexPitch] * 0.1621223 + fftEnergyInput[indexAx] * -1.4767953 + fftEnergyInput[indexAy] * 1.4440913 + fftEnergyInput[indexGx] * -0.0000184 + fftEnergyInput[indexGy] * 0.0001115 + fftEntropyInput[indexAx] * 0.0398527 + fftEntropyInput[indexAy] * 0.0072565 + fftEntropyInput[indexGx] * -0.0000057 + fftEntropyInput[indexGy] * -0.0000241 + fftEntropyInput[indexRoll] * 0.000011  + fftEntropyInput[indexPitch] * -0.0000076 + covarianceAxAy * 14.4869073 + covarianceGxGy * 0.0001722 + covarianceGxGz * 0.0000302 + covarianceGyGz * 0.0000426 + correlationAxAy * -0.6087329 + correlationAyAz * 0.8121621 + correlationGxGy * -0.7578244 + correlationGxGz * -0.1143072 ;
            Class[9] = 2.1589777 +  standardDeviationInput[indexAy] * 0.3927688 + standardDeviationInput[indexAz] * 0.7742635 + standardDeviationInput[indexGx] * 0.0023762 + standardDeviationInput[indexGz] * 0.0136554 + meanInput[indexAx] * -3.8212991 + meanInput[indexAz] * -0.7714662 + meanInput[indexGx] * -0.0090773 + meanInput[indexGz] * 0.0077782 + meanInput[indexRoll] * -0.0325776 + meanInput[indexPitch] * 0.0083166 + varianceInput[indexAx] * 7.3594148 + varianceInput[indexAz] * 0.8337098 + varianceInput[indexGy] * -0.0000868 + varianceInput[indexTotalAcc] * -2.9124793 + varianceInput[indexTotalGyro] * 0.0001482 + varianceInput[indexRoll] * 0.0002951 + varianceInput[indexPitch] * 0.0005997 + zeroCrossingRateInput[indexAx] * 4.2942262 + zeroCrossingRateInput[indexAz] * -15.374939 + zeroCrossingRateInput[indexGx] * -5.9756945 + zeroCrossingRateInput[indexGy] * -0.8722785 + zeroCrossingRateInput[indexGz] * 9.4596505 + zeroCrossingRateInput[indexPitch] * -3.4033009 + rootMeanSquareInput[indexAx] * -4.0248667 + rootMeanSquareInput[indexGy] * 0.0966193 + rootMeanSquareInput[indexTotalAcc] * -2.9322733 + skewnessInput[indexAx] * -0.8292826 + skewnessInput[indexAy] * 0.3344885 + skewnessInput[indexAz] * -0.3009944 + skewnessInput[indexGx] * -0.2486586 + skewnessInput[indexGy] * -1.5023449 + skewnessInput[indexGz] * 0.0324353 + skewnessInput[indexTotalAcc] * 0.2731294 + skewnessInput[indexTotalGyro] * -1.9709177 + skewnessInput[indexRoll] * -0.0837894 + skewnessInput[indexPitch] * -0.7308852 + kurtosisInput[indexAx] * 0.0185495 + kurtosisInput[indexAy] * 0.1894983 + kurtosisInput[indexAz] * -0.2032993 + kurtosisInput[indexGx] * 0.0683781 + kurtosisInput[indexGy] * -0.0895441 + kurtosisInput[indexGz] * 0.2031835 + kurtosisInput[indexTotalAcc] * -0.0739615 + kurtosisInput[indexTotalGyro] * 0.1675606 + kurtosisInput[indexRoll] * -0.0514638 + kurtosisInput[indexPitch] * -0.0100986 + meanAbsDevInput[indexAx] * -0.3522429 + meanAbsDevInput[indexAy] * -0.5825959 + meanAbsDevInput[indexGy] * -0.0813255 + meanAbsDevInput[indexGz] * -0.0174769 + meanAbsDevInput[indexTotalGyro] * 0.0059664 + meanAbsDevInput[indexRoll] * -0.0823393 + meanAbsDevInput[indexPitch] * -0.1628521 + fftMaxFreqInput[indexAx] * 0.1128412 + fftMaxFreqInput[indexAy] * 0.1553739 + fftMaxFreqInput[indexGx] * -0.1348189 + fftMaxFreqInput[indexGy] * -0.1848476 + fftMaxFreqInput[indexGz] * 0.2160961 + fftMaxFreqInput[indexRoll] * 0.261954  + fftMaxFreqInput[indexPitch] * 0.1375676 + fftEnergyInput[indexAx] * 2.2048486 + fftEnergyInput[indexAz] * -0.7783239 + fftEnergyInput[indexGx] * -0.0000099 + fftEnergyInput[indexGy] * 0.0000986 + fftEntropyInput[indexAy] * -0.0145296 + fftEntropyInput[indexGx] * -0.0000246 + fftEntropyInput[indexGz] * -0.0000126 + fftEntropyInput[indexTotalGyro] * 0.0000359 + fftEntropyInput[indexRoll] * -0.0000068 + covarianceAxAy * 14.5866491 + covarianceAyAz * 10.5987592 + covarianceGxGy * -0.0001743 + covarianceGxGz * -0.0000731 + covarianceGyGz * 0.0000728 + correlationAxAy * -1.9291778 + correlationAyAz * 0.5225208 + correlationGxGy * 3.7894572 + correlationGxGz * -0.3259268 + correlationGyGz * 0.9361323 ;
            Class[10] = 7.287089  +  standardDeviationInput[indexAx] * -4.2901512 + standardDeviationInput[indexGx] * -0.0599246 + standardDeviationInput[indexGz] * -0.0607195 + standardDeviationInput[indexRoll] * -0.0130372 + standardDeviationInput[indexPitch] * -0.0174815 + meanInput[indexAz] * -6.7358467 + meanInput[indexGx] * -0.0033035 + meanInput[indexGy] * 0.004787  + meanInput[indexGz] * -0.0124288 + meanInput[indexTotalAcc] * 5.2647015 + varianceInput[indexAx] * -3.5709743 + varianceInput[indexAy] * 2.0931643 + varianceInput[indexAz] * 0.8648896 + varianceInput[indexGx] * 0.0002295 + varianceInput[indexGy] * 0.0002587 + varianceInput[indexGz] * 0.0000059 + varianceInput[indexTotalGyro] * 0.0000927 + varianceInput[indexRoll] * -0.0002994 + zeroCrossingRateInput[indexAy] * 1.9663124 + zeroCrossingRateInput[indexAz] * -4.1893531 + zeroCrossingRateInput[indexGy] * -4.7581712 + zeroCrossingRateInput[indexRoll] * -0.8144756 + rootMeanSquareInput[indexAx] * 1.5540243 + rootMeanSquareInput[indexRoll] * 0.0133126 + skewnessInput[indexAy] * -0.3928919 + skewnessInput[indexAz] * -0.0798966 + skewnessInput[indexGx] * -0.0607035 + skewnessInput[indexGy] * -0.0951775 + skewnessInput[indexGz] * 0.0619721 + skewnessInput[indexTotalAcc] * -0.0726867 + skewnessInput[indexTotalGyro] * 1.0438847 + kurtosisInput[indexAx] * -0.0134166 + kurtosisInput[indexAy] * 0.0666885 + kurtosisInput[indexAz] * 0.0784939 + kurtosisInput[indexGx] * 0.0133101 + kurtosisInput[indexTotalAcc] * 0.0420539 + kurtosisInput[indexTotalGyro] * 0.2088036 + kurtosisInput[indexRoll] * 0.009011  + kurtosisInput[indexPitch] * 0.0036217 + meanAbsDevInput[indexAx] * 2.6668884 + meanAbsDevInput[indexAz] * -0.5856734 + meanAbsDevInput[indexGx] * -0.0243558 + meanAbsDevInput[indexGy] * 0.002065  + meanAbsDevInput[indexGz] * 0.0347136 + meanAbsDevInput[indexTotalAcc] * -4.9527215 + meanAbsDevInput[indexTotalGyro] * -0.0194504 + meanAbsDevInput[indexRoll] * 0.02939   + fftMaxFreqInput[indexAx] * 0.0693838 + fftMaxFreqInput[indexAy] * 0.2451302 + fftMaxFreqInput[indexAz] * 0.2649721 + fftMaxFreqInput[indexGx] * 0.1439263 + fftMaxFreqInput[indexGz] * 0.2077897 + fftMaxFreqInput[indexRoll] * 0.1151894 + fftMaxFreqInput[indexPitch] * -0.1462154 + fftEnergyInput[indexAy] * 0.3973519 + fftEnergyInput[indexAz] * 0.4300235 + fftEnergyInput[indexGx] * 0.0000725 + fftEnergyInput[indexGy] * -0.0000138 + fftEnergyInput[indexGz] * 0.0000309 + fftEnergyInput[indexRoll] * 0.000084  + fftEntropyInput[indexAx] * -0.016466 + fftEntropyInput[indexAy] * 0.0044635 + fftEntropyInput[indexAz] * 0.0528842 + fftEntropyInput[indexGz] * 0.0000107 + fftEntropyInput[indexTotalAcc] * -0.0146168 + fftEntropyInput[indexTotalGyro] * -0.0000131 + covarianceAxAy * -6.2536845 + correlationAyAz * 0.0740869 + correlationGxGy * 0.4211941 + correlationGxGz * -0.1913266 + correlationGyGz * -0.1333964;
            */
            gesture = getMaxLikelihood(Class);
        }
        return gesture;
    }

    private String getMaxLikelihood(double[] allClassProbability){
        String gesture = "";
        double max =0;
        double totalClass = 0;
        double[] classCore = new double[allClassProbability.length];

        for(int i=0; i< allClassProbability.length; i++){
            totalClass = totalClass + Math.exp(allClassProbability[i]);
        }
        for(int j=0; j<allClassProbability.length; j++){
            classCore[j]=Math.exp(allClassProbability[j])/totalClass;
        }
        //Math.exp(class0) + Math.exp(class1) + Math.exp(class2)+ Math.exp(class3) + Math.exp(class4) + Math.exp(class5) + Math.exp(class6) + Math.exp(class7) + Math.exp(class8) + Math.exp(class9) + Math.exp(class10);
        for (int counter = 0; counter < classCore.length; counter++)
        {
            if (classCore[counter] > max)
            {
                max = classCore[counter];
            }
        }

        if(classCore[0]==max){
            gesture = "Up";
        } else if(classCore[1]==max){
            gesture = "Down";
        } else if(classCore[2]==max){
            gesture = "Left";
        } else if(classCore[3]==max){
            gesture = "Right";
        } else if(classCore[4]==max){
            gesture = "VLR";
        } else if(classCore[5]==max){
            gesture = "VRL";
        } else if(classCore[6]==max){
            gesture = "CrossClockwise";
        } else if(classCore[7]==max){
            gesture = "CrossCounterClockwise";
        } else if(classCore[8]==max){
            gesture = "Clockwise";
        } else if(classCore[9]==max){
            gesture = "CounterClockwise";
        } else if(classCore[10]==max){
            gesture = "NonGesture";
        }
        return gesture;
    }
}
