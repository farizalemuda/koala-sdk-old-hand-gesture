/**
 * Thingnergy Android API
 * Copyright (c) 2015 Chun-Ting Ding <thingnergy@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Some open source application is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thingnergy.api;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.UUID;

public class TNGService extends Service {

    private final static String TAG = TNGService.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;

    public final static String ACTION_GATT_CONNECTED = "ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_GATT_RSSI = "ACTION_GATT_RSSI";
    public final static String ACTION_DATA_AVAILABLE = "ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA = "EXTRA_DATA";

    public final static UUID UUID_MOTION_PARAM_CHANGE_CHARACTERISTIC = UUID
            .fromString(TNGGattAttributes.THINGNERGY_MOTION_PARAM_CHANGE_CHARACTERISTIC_UUID);
  /*  public final static UUID UUID_MOTION_MEASUREMENT_CHARACTERISTIC = UUID
            .fromString(TNGGattAttributes.THINGNERGY_MOTION_MEASUREMENT_CHARACTERISTIC_UUID);
    public final static UUID UUID_THINGNERGY_MOTNIO_SERVICE = UUID
            .fromString(TNGGattAttributes.THINGNERGY_MOTNIO_SERVICE_UUID);*/

    public final static UUID UUID_THINGNERGY_MOTNIO_SERVICE = UUID.fromString("eb371600-347c-fe94-1600-8295a1e42b09");
    public static final UUID UUID_MOTION_MEASUREMENT_CHARACTERISTIC = UUID.fromString("eb371601-347c-fe94-1600-8295a1e42b09");
    public static final UUID CCCD = UUID
            .fromString("00002902-0000-1000-8000-00805f9b34fb");

    public static final byte MOTION_WRITE_RATE_OPCODE            =     0x02;
    public static final byte MOTION_WRITE_ACC_SCALE_OPCODE       =    0x03;
    public static final byte MOTION_WRITE_GYRO_SCALE_OPCODE      =     0x04;

    public static final int MOTION_ACCEL_SCALE_2G      =     0x00;
    public static final int MOTION_ACCEL_SCALE_4G       =    0x01;
    public static final int MOTION_ACCEL_SCALE_8G      =     0x02;
    public static final int MOTION_ACCEL_SCALE_16G      =    0x03;

    public static final int MOTION_GYRO_SCALE_250      =     0x00;
    public static final int MOTION_GYRO_SCALE_500       =    0x01;
    public static final int MOTION_GYRO_SCALE_1000      =     0x02;
    public static final int MOTION_GYRO_SCALE_2000      =    0x03;
   /**
     * The callback function to handle the connection stage of BLE
     */
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                            int newState) {
            String intentAction;

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:"
                        + mBluetoothGatt.discoverServices());
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_RSSI, rssi);
            } else {
                Log.w(TAG, "onReadRemoteRssi received: " + status);
            }
        };

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent2 = new Intent(action);
        sendBroadcast(intent2);
    }

    private void broadcastUpdate(final String action, int rssi) {
        final Intent intent2 = new Intent(action);
        intent2.putExtra(EXTRA_DATA, String.valueOf(rssi));
        sendBroadcast(intent2);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent2 = new Intent(action);
        // This is special handling for the Motion Raw Data Measurement profile. Data
        if (UUID_MOTION_MEASUREMENT_CHARACTERISTIC.equals(characteristic.getUuid())) {
            final byte[] rx = characteristic.getValue();
            intent2.putExtra(EXTRA_DATA, rx);
        }

        sendBroadcast(intent2);
    }

    public class LocalBinder extends Binder {
        public TNGService getService() {
            return TNGService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent2) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent2) {
        // After using a given device, you should make sure that
        // BluetoothGatt.close() is called
        // such that resources are cleaned up properly. In this particular
        // example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent2);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter
        // through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
         * Connects to the GATT server hosted on the Bluetooth LE device.
         *
         * @param address
         *            The device address of the destination device.
         *
         * @return Return true if the connection is initiated successfully. The
         *         connection result is reported asynchronously through the
         *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
         *         callback.
         */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG,
                    "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device. Try to reconnect.
        if (mBluetoothDeviceAddress != null
                && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG,
                    "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter
                .getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the
        // autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;

        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The
     * disconnection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure
     * resources are released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    public void readRssi() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        mBluetoothGatt.readRemoteRssi();
    }

    /**
     * Enable the notification of raw data services
     */
    public void enableMotionRawService(){
        BluetoothGattService motionService = mBluetoothGatt.getService(UUID_THINGNERGY_MOTNIO_SERVICE);
        if (motionService == null) {
            Log.w(TAG, "motion service not found!");
            return;
        }

        BluetoothGattCharacteristic motionRawDataCharacteristic = motionService.getCharacteristic(UUID_MOTION_MEASUREMENT_CHARACTERISTIC);
        if (motionRawDataCharacteristic == null) {
            Log.w(TAG, "motion characteristic not found!");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(motionRawDataCharacteristic,true);

        BluetoothGattDescriptor descriptor = motionRawDataCharacteristic.getDescriptor(CCCD);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
        motionRawDataCharacteristic.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
    }

   /**
     * Disable the notification of raw data services
     */
    public void disableMotionRawService(){
        BluetoothGattService motionService = mBluetoothGatt.getService(UUID_THINGNERGY_MOTNIO_SERVICE);
        if (motionService == null) {
            Log.w(TAG, "motion service not found!");
            return;
        }

        BluetoothGattCharacteristic motionRawDataCharacteristic = motionService.getCharacteristic(UUID_MOTION_MEASUREMENT_CHARACTERISTIC);
        if (motionRawDataCharacteristic == null) {
            Log.w(TAG, "motion characteristic not found!");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(motionRawDataCharacteristic,false);

        BluetoothGattDescriptor descriptor = motionRawDataCharacteristic.getDescriptor(CCCD);
        descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
        motionRawDataCharacteristic.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
    }

    /**
     * Configuration the scale of accel sensor
     *
     * @param scale
     *            int 0 MOTION_ACCEL_SCALE_2G: 2g
     *            int 1 MOTION_ACCEL_SCALE_4G:4g
     *            int 2 MOTION_ACCEL_SCALE_8G: 8g
     *             int 3 MOTION_ACCEL_SCALE_16G  16g
     */
    public void setMotionAccelScale(int scale){
        BluetoothGattService motionService = mBluetoothGatt.getService(UUID_THINGNERGY_MOTNIO_SERVICE);
        if (motionService == null) {
            Log.w(TAG, "motion service not found!");
            return;
        }

        BluetoothGattCharacteristic motionParamCharacteristic = motionService.getCharacteristic(UUID_MOTION_PARAM_CHANGE_CHARACTERISTIC);
        if (motionParamCharacteristic == null) {
            Log.w(TAG, "motion characteristic not found!");
            return;
        }

        byte[] value = new byte[]{ 0x00,0x00,0x00,0x00};
        value[0] = MOTION_WRITE_ACC_SCALE_OPCODE;
        switch (scale) {
            case MOTION_ACCEL_SCALE_2G:
                value[1] = 0x00;
                break;
            case MOTION_ACCEL_SCALE_4G:
                value[1] = 0x01;
                break;
            case MOTION_ACCEL_SCALE_8G:
                value[1] = 0x02;
                break;
            case MOTION_ACCEL_SCALE_16G:
                value[1] = 0x03;
                break;
            default:
                value[1] = 0x00;
                break;
        }
        motionParamCharacteristic.setValue(value);
        boolean status = mBluetoothGatt.writeCharacteristic(motionParamCharacteristic);
    }

   /**
     * Configuration the scale of gyro sensor
     *
     * @param scale
     *            int 0 MOTION_GYRO_SCALE_250: 250
    *            int 1 MOTION_GYRO_SCALE_500:500
    *            int 2 MOTION_GYRO_SCALE 1000: 1000
    *            int3MOTION_GYRO_SCALE_2000  2000
     */
    public void setMotionGyroScale(int scale){
        BluetoothGattService motionService = mBluetoothGatt.getService(UUID_THINGNERGY_MOTNIO_SERVICE);
        if (motionService == null) {
            Log.w(TAG, "motion service not found!");
            return;
        }

        BluetoothGattCharacteristic motionParamCharacteristic = motionService.getCharacteristic(UUID_MOTION_PARAM_CHANGE_CHARACTERISTIC);
        if (motionParamCharacteristic == null) {
            Log.w(TAG, "motion characteristic not found!");
            return;
        }

        byte[] value = new byte[]{ 0x00,0x00,0x00,0x00};
        value[0] = MOTION_WRITE_GYRO_SCALE_OPCODE;
        switch (scale) {
            case MOTION_GYRO_SCALE_250:
                value[1] = 0x00;
                break;
            case MOTION_GYRO_SCALE_500:
                value[1] = 0x01;
                break;
            case MOTION_GYRO_SCALE_1000:
                value[1] = 0x02;
                break;
            case MOTION_GYRO_SCALE_2000:
                value[1] = 0x03;
                break;
            default:
                value[1] = 0x00;
                break;
        }
        motionParamCharacteristic.setValue(value);
        boolean status = mBluetoothGatt.writeCharacteristic(motionParamCharacteristic);
    }


}
