/**
 * Thingnergy Android API
 * Copyright (c) 2015 Chun-Ting Ding <thingnergy@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Some open source application is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.thingnergy.api;


import android.bluetooth.BluetoothDevice;

public class TNGDevices {

    private BluetoothDevice mDevices = null;
    private int rssi;
    private byte[] scanRecord;

    public TNGDevices(BluetoothDevice mDevices, int rssi, byte[] scanRecord) {
       this.mDevices = mDevices;
       this.rssi = rssi;
       this.scanRecord = scanRecord;
    }

    public BluetoothDevice getDevice() {
        return this.mDevices;
    }

    public int getRssi(){
        return this.rssi;
    }

    public void setRssi(int r){
        this.rssi= r;
    }

    public byte[] getScanRecordData() {
        return  this.scanRecord;
    }




}
