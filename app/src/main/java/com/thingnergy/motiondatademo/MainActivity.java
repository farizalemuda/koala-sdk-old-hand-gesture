/**
 * Thingnergy Android API
 * Copyright (c) 2015 Chun-Ting Ding <thingnergy@gmail.com>
 * <p/>
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * <p/>
 * Some open source application is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thingnergy.motiondatademo;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
// Import Thingnergy API
import com.cwt.FeatureExtraction;
import com.cwt.GestureRecognition;
import com.philips.lighting.hue.listener.PHLightListener;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHBridgeResource;
import com.philips.lighting.model.PHHueError;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;
import com.thingnergy.api.TNGDevices;
import com.thingnergy.api.TNGService;

import com.wowwee.bluetoothrobotcontrollib.sdk.MipRobot;
import com.wowwee.bluetoothrobotcontrollib.sdk.MipRobotFinder;
import com.wowwee.bluetoothrobotcontrollib.sdk.MipRobot.MipRobotInterface;
//import com.opencsv.CSVWriter;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends FragmentActivity implements MipRobotInterface{
    private final static String TAG = MainActivity.class.getSimpleName();

    private TNGService mBluetoothLeService; // the main service to control the ble device
    private BluetoothAdapter mBluetoothAdapter2;
    public static ArrayList<TNGDevices> mDevice = new ArrayList<TNGDevices>();  // Manage the devices

    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 4000;
    public static final int REQUEST_CODE = 30;
    private String mDeviceAddress;
    private String mDeviceName;
    private boolean flag = true;
    private boolean connState = false;
    private boolean storeDataFlag = false;

    Button scanAllBtn;
    Button buttonUp, buttonDown, buttonLeft, buttonRight, buttonClockwise, buttonCounterClockwise;
    Button buttonVLeftRight, buttonVRightLeft, buttonCrossCW, buttonCrossCCW;
    Button Jancuk;

    private Switch modeSwitch;
    private LinearLayout layDebugMIP;
    private LinearLayout layTrainingButton;
    private TextView txtMode;
    private TextView txtGesture;
    // ---- MIP Robot ----- //
    private TextView robotStatus;
    private BluetoothAdapter mBluetoothAdapter;
    protected float[] forwardMovement = new float[]{ 0, (float)32.0};
    protected float[] backwardMovement = new float[]{ 0, (float)-10.0};
    protected float[] leftMovement = new float[]{ 1, (float)0};
    protected float[] rightMovement = new float[]{ -1, (float)0};
    protected float[] CWMovement = new float[]{ 1, (float)2};
    protected float[] CCWMovement = new float[]{ -1, (float)2};
    Button forward = null;
    Button backward = null;
    Button counterClockwise = null;
    Button clockwise = null;

    // ---- END ---- //
    TextView uuidTv = null;
    TextView rawDataText = null;
    TextView lastUuid = null;

    String motionType = "";//no motion
    int counter = 0;
    int dataSampling = 180;
    int filterSampling = 10;
    int overlappingFactor = 10;
    // 6 sensor + 2 total every axis of those 2 sensors + roll + pitch
    int numberSensorData = 10;
    double SDFilterThreshold = 0.2;

    String rawDataString;
    String featureData;
    //String[] gestureLabel = new String[dataSampling];
    String[] gestureLabel = {"NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture",
            "NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture","NonGesture,NonGesture"};
    // Initialize Indexing Variable
    private static final int indexAx = 0;
    private static final int indexAy = 1;
    private static final int indexAz = 2;
    private static final int indexGx = 3;
    private static final int indexGy = 4;
    private static final int indexGz = 5;
    private static final int indexTotalAcc = 6;
    private static final int indexTotalGyro = 7;
    private static final int indexRoll = 8;
    private static final int indexPitch = 9;

    private static final int indexAxAy = 0;
    private static final int indexAxAz = 1;
    private static final int indexAyAz = 2;

    private static final int indexGxGy = 0;
    private static final int indexGxGz = 1;
    private static final int indexGyGz = 2;

    private static final int indexAPolyConst = 4;
    private static final int indexBPolyConst = 3;
    private static final int indexCPolyConst = 2;
    private static final int indexDPolyConst = 1;
    private static final int indexEPolyConst = 0;

    private static final int koalaSamplingRate = 80; //in Hz
    int count = 0;
    int gestureLabelNumber = 0;

    double[][] rawData = new double[numberSensorData][dataSampling];

    double totalAcc = 0;
    double totalGyro = 0;
    double roll = 0;
    double pitch = 0;

    // Features Variables declaration
    double[] sampledStandardDeviation = new double[numberSensorData];
    double[] standardDeviation = new double[numberSensorData];
    double[] mean = new double[numberSensorData];
    double[] variance = new double[numberSensorData];
    double covarianceAxAy = 0;
    double covarianceAxAz = 0;
    double covarianceAyAz = 0;
    double covarianceGxGy = 0;
    double covarianceGxGz = 0;
    double covarianceGyGz = 0;
    double correlationAxAy = 0;
    double correlationAxAz = 0;
    double correlationAyAz = 0;
    double correlationGxGy = 0;
    double correlationGxGz = 0;
    double correlationGyGz = 0;
    double[] covariance = new double[6];
    double[] correlation = new double[6];
    double[] zeroCrossingRate = new double[numberSensorData];
    double[] rootMeanSquare = new double[numberSensorData];
    double[] skewness = new double[numberSensorData];
    double[] kurtosis = new double[numberSensorData];
    double[] meanAbsDev = new double[numberSensorData];
    double[] fftMaxFreq = new double[numberSensorData];
    double[] fftEnergy = new double[numberSensorData];
    double[] fftEntropy = new double[numberSensorData];

    File excel;
    FileWriter writeExcel;
    /*
    // Platform Variable Part
    Request request;
    RequestBody body;
    MediaType mediaType;
    OkHttpClient client;
    Response response;
    String serverResponse;
    */
    String[] featureLabel = {"standardDeviation","mean","variance","zeroCrossingRate","rootMeanSquare","skewness","kurtosis","meanAbsDev","fftMaxFreq","fftEnergy","fftEntropy","covariance","correlation"};
    double[][] test = new double[][]{
            {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180},
            {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180},
            {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180},
            {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180},
            {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180},
            {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180}
    };
    private MediaPlayer homeMusic;
    // Hue Smartlight
    private PHHueSDK phHueSDK;

    int gestureMode =1;

    MediaPlayer musicHomeOn;
    MediaPlayer musicHomeOff;
    MediaPlayer musicHomeRock;
    MediaPlayer musicHomeRandB;
    MediaPlayer musicHomePop;
    MediaPlayer musicHomeOrchestra;

    boolean masterLoop = false;
    boolean loopUp = false;
    boolean loopDown = false;
    boolean loopLeft = false;
    boolean loopRight = false;

    private FeatureExtraction koalaFeature;
    private GestureRecognition koalaGesture;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder service) {
            mBluetoothLeService = ((TNGService.LocalBinder) service)
                    .getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent2) {
            final String action = intent2.getAction();

            if (TNGService.ACTION_GATT_CONNECTED.equals(action)) {
                flag = true;
                connState = true;

                Toast.makeText(getApplicationContext(), "Connected",
                        Toast.LENGTH_SHORT).show();

                lastUuid.setText(mDeviceName + " ( " + mDeviceAddress + " )");
                scanAllBtn.setText("Disconnect");
                startReadRssi();
                startToReadData();


            } else if (TNGService.ACTION_GATT_DISCONNECTED.equals(action)) {
                flag = false;
                connState = false;

                Toast.makeText(getApplicationContext(), "Disconnected",
                        Toast.LENGTH_SHORT).show();
                scanAllBtn.setText("Scan All");
                uuidTv.setText("");


            } else if (TNGService.ACTION_GATT_RSSI.equals(action)) {
                displayData(intent2.getStringExtra(TNGService.EXTRA_DATA));

            } else if (TNGService.ACTION_DATA_AVAILABLE.equals(action)) {

                final byte[] value = intent2.getByteArrayExtra(TNGService.EXTRA_DATA);

                short s_accel_x = (short) ((short) value[1] * 256 + value[2]);
                short s_accel_y = (short) ((short) value[3] * 256 + value[4]);
                short s_accel_z = (short) ((short) value[5] * 256 + value[6]);
                short s_gyro_x = (short) ((short) value[7] * 256 + value[8]);
                short s_gyro_y = (short) ((short) value[9] * 256 + value[10]);
                short s_gyro_z = (short) ((short) value[11] * 256 + value[12]);

                double accel_x = s_accel_x * 4.0d / Math.pow(2, 16); // +-2g
                double accel_y = s_accel_y * 4.0d / Math.pow(2, 16);
                double accel_z = s_accel_z * 4.0d / Math.pow(2, 16);

                double gyro_x = s_gyro_x * 500.0d / Math.pow(2, 16);  //+-250
                double gyro_y = s_gyro_y * 500.0d / Math.pow(2, 16);
                double gyro_z = s_gyro_z * 500.0d / Math.pow(2, 16);

                totalAcc = Math.sqrt(accel_x * accel_x + accel_y * accel_y + accel_z * accel_z);
                totalGyro = Math.sqrt(gyro_x * gyro_x + gyro_y * gyro_y + gyro_z * gyro_z);
                roll = Math.atan2(-accel_x, accel_z) * 180 / Math.PI;
                pitch = -(Math.atan2(-accel_y,accel_z)*180)/Math.PI;

                rawDataText.setText(String.format(Locale.ENGLISH,"%.4f %.4f %.4f %.4f %.4f %.4f", accel_x, accel_y, accel_z, gyro_x, gyro_y, gyro_z));

                Calendar c = Calendar.getInstance();
                int years = c.get(Calendar.YEAR);
                int dates = c.get(Calendar.DATE);
                int months = c.get(Calendar.MONTH);
                months++;
                int hours = c.get(Calendar.HOUR_OF_DAY);
                int seconds = c.get(Calendar.SECOND);
                int minutes = c.get(Calendar.MINUTE);
                int milliseconds = c.get(Calendar.MILLISECOND);

                for (int a = 0; a < numberSensorData; a++) {
                    for (int b = 0; b < (dataSampling - 1); b++) {
                        rawData[a][b] = rawData[a][b + 1];
                    }
                }

                for(int d = 0; d <(dataSampling - 1); d++){
                    gestureLabel[d]=gestureLabel[d+1];
                }

                rawData[indexAx][dataSampling - 1] = accel_x;
                rawData[indexAy][dataSampling - 1] = accel_y;
                rawData[indexAz][dataSampling - 1] = accel_z;
                rawData[indexGx][dataSampling - 1] = gyro_x;
                rawData[indexGy][dataSampling - 1] = gyro_y;
                rawData[indexGz][dataSampling - 1] = gyro_z;
                rawData[indexTotalAcc][dataSampling - 1] = totalAcc;
                rawData[indexTotalGyro][dataSampling - 1] = totalGyro;
                rawData[indexRoll][dataSampling - 1] = roll;
                rawData[indexPitch][dataSampling - 1] = pitch;

                if(!motionType.equals("")){
                    gestureLabel[dataSampling - 1] = "Gesture,"+motionType;
                    rawDataString = "Gesture,"+motionType+",";
                } else {
                    gestureLabel[dataSampling - 1] = "NonGesture,NonGesture";
                    rawDataString = "NonGesture,NonGesture,";
                }

                rawDataString = rawDataString + String.format(Locale.ENGLISH,"%d-%d-%d, %d, %d, %d, %d,",years,months,dates,hours,minutes,seconds,milliseconds);
                for(int i = 0; i < numberSensorData; i++){
                    if(i == numberSensorData-1 ){
                        rawDataString = rawDataString + String.format(Locale.ENGLISH,"%.4f\n",rawData[i][dataSampling - 1]);
                    }
                    else{
                        rawDataString = rawDataString + String.format(Locale.ENGLISH,"%.4f,",rawData[i][dataSampling - 1]);
                    }
                }

                if(storeDataFlag) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            writeToSDCard(rawDataString, "LAO Raw Data");
                            rawDataString = "";
                        }
                    });
                }

                if (count >= overlappingFactor) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            koalaFeature.setData(rawData);
                            koalaFeature.setFFTSamplingFreq(80);

                            standardDeviation = koalaFeature.getStandardDeviation2D();
                            mean = koalaFeature.getMean2D();
                            variance = koalaFeature.getVariance2D();
                            zeroCrossingRate = koalaFeature.getZeroCrossingRate2D();
                            rootMeanSquare = koalaFeature.getRootMeanSquare2D();
                            skewness = koalaFeature.getSkewness2D();
                            kurtosis = koalaFeature.getKurtosis2D();
                            meanAbsDev = koalaFeature.getMeanAbsDev2D();

                            fftMaxFreq = koalaFeature.getFFTMaxFreq2D();
                            fftEnergy = koalaFeature.getFFTEnergy2D();
                            fftEntropy = koalaFeature.getFFTEntropy2D();

                            covariance = koalaFeature.getCovariance2D();
                            correlation = koalaFeature.getCorrelation2D();


                        }
                    });


                    if(storeDataFlag) {                                                        // index A & B
                        gestureLabelNumber = countGestureLabel(gestureLabel);
                        if(gestureLabelNumber>80&&!getDominantGestureLabel(gestureLabel).equals("NonGesture")){
                            featureData = "Gesture,"+getDominantGestureLabel(gestureLabel)+",";
                        } else {
                            featureData = "NonGesture,NonGesture,";
                        }

                        // index C, D, E, F, G
                        featureData = featureData + String.format(Locale.ENGLISH,"%d-%d-%d, %d, %d, %d, %d,",years,months,dates,hours,minutes,seconds,milliseconds);

                        featureData = featureData + putCommaInBetween(standardDeviation) + ","; //index H - Q
                        featureData = featureData + putCommaInBetween(mean) + ","; // R - AA
                        featureData = featureData + putCommaInBetween(variance) + ","; // AB - AK
                        featureData = featureData + putCommaInBetween(zeroCrossingRate) + ","; // AL - AU
                        featureData = featureData + putCommaInBetween(rootMeanSquare) + ","; // AV - BE
                        featureData = featureData + putCommaInBetween(skewness) + ","; // BF - BO
                        featureData = featureData + putCommaInBetween(kurtosis) + ","; // BP - BY
                        featureData = featureData + putCommaInBetween(meanAbsDev) + ","; // BZ - CI
                        featureData = featureData + putCommaInBetween(fftMaxFreq) + ","; // CJ - CS
                        featureData = featureData + putCommaInBetween(fftEnergy) + ","; // CT - DC
                        featureData = featureData + putCommaInBetween(fftEntropy) + ","; // DD - DM

                        featureData = featureData + String.format(Locale.ENGLISH,"%.6f,%.6f,%.6f,",covarianceAxAy,covarianceAxAz,covarianceAyAz); // DN, DO, DP
                        featureData = featureData + String.format(Locale.ENGLISH,"%.6f,%.6f,%.6f,",covarianceGxGy,covarianceGxGz,covarianceGyGz); //DQ, DR, DS
                        featureData = featureData + String.format(Locale.ENGLISH,"%.6f,%.6f,%.6f,",correlationAxAy,correlationAxAz,correlationAyAz); //DT, DU. DV
                        featureData = featureData + String.format(Locale.ENGLISH,"%.6f,%.6f,%.6f\n",correlationGxGy,correlationGxGz,correlationGyGz);//DW, DX, DY

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                writeToSDCard(featureData, "LAO Feature Data");
                                featureData = "";
                            }
                        });
                    }



                    String gestureDecision = "";
                    if(meanAbsDev[indexTotalGyro] <= 28.849161 &&
                            fftEnergy[indexGx] <= 2078.767342 &&
                            kurtosis[indexTotalGyro] > 1.986365 &&
                            mean[indexGx] <= 5.636681 &&
                            rootMeanSquare[indexTotalAcc] > 0.917384 &&
                            meanAbsDev[indexGz] <= 30.382468 &&
                            fftEntropy[indexPitch] <= 25.042913 &&
                            fftEnergy[indexAz] <= 0.95461 &&
                            fftEntropy[indexAy] > -18.359861){
                        //Log.d("AHOY","BUKAN GESTURE");
                        gestureDecision = "NonGesture";
                    }else{
                        /*
                        AsyncT sendDataToServer = new AsyncT();
                        sendDataToServer.execute(featureLabel[0],Arrays.toString(standardDeviation).replace("[", "").replace("]", ""),featureLabel[1],Arrays.toString(mean).replace("[", "").replace("]", ""),
                                featureLabel[2],Arrays.toString(variance).replace("[", "").replace("]", ""),featureLabel[3],Arrays.toString(zeroCrossingRate).replace("[", "").replace("]", ""),
                                featureLabel[4],Arrays.toString(rootMeanSquare).replace("[", "").replace("]", ""),featureLabel[5],Arrays.toString(skewness).replace("[", "").replace("]", ""),
                                featureLabel[6],Arrays.toString(kurtosis).replace("[", "").replace("]", ""),featureLabel[7],Arrays.toString(meanAbsDev).replace("[", "").replace("]", ""),
                                featureLabel[8],Arrays.toString(fftMaxFreq).replace("[", "").replace("]", ""),featureLabel[9],Arrays.toString(fftEnergy).replace("[", "").replace("]", ""),
                                featureLabel[10],Arrays.toString(fftEntropy).replace("[", "").replace("]", ""),
                                featureLabel[11],Arrays.toString(covariance).replace("[", "").replace("]", ""),featureLabel[12],Arrays.toString(correlation).replace("[", "").replace("]", ""));
                        */


                        koalaGesture.setFeatures(standardDeviation,mean,variance,zeroCrossingRate,
                        rootMeanSquare,skewness,kurtosis,meanAbsDev,fftMaxFreq,fftEnergy,
                        fftEntropy,covariance,correlation);
                        gestureDecision = koalaGesture.getGesture(GestureRecognition.LOGISTIC_REGRESSION);
                        Log.d("CWTLAO","Phone"+gestureDecision);

                    }

                    demoFunction(gestureDecision);
                    count = 0;
                }
                count++;
            }
        }
    };

    private void demoFunction(String gesture){
        final Random randValue = new Random();
        final int maxBri = 254;
        final int minBri = 0;
        final int maxHue = 360;
        final int minHue = 0;
        final int maxSat = 254;
        final int minSat = 0;

        switch (gesture){
            case "Clockwise":
                setHueOn(1,true);
                setHueOn(2,true);
                setHueOn(3,true);
                masterLoop = true;
                if (!musicHomeOn.isPlaying()) {
                    musicHomeOn.start();
                }
                if (musicHomePop.isPlaying()) {
                    musicHomePop.stop();
                }
                if (musicHomeRandB.isPlaying()) {
                    musicHomeRandB.stop();
                }
                if (musicHomeOrchestra.isPlaying()) {
                    musicHomeOrchestra.stop();
                }
                if (musicHomeRock.isPlaying()) {
                    musicHomeRock.stop();
                }
                break;

            case "CounterClockwise":
                setHueOn(1,false);
                setHueOn(2,false);
                setHueOn(3,false);
                masterLoop = false;
                if (!musicHomeOff.isPlaying()) {
                    musicHomeOff.start();
                }
                if (musicHomePop.isPlaying()) {
                    musicHomePop.stop();
                }
                if (musicHomeRandB.isPlaying()) {
                    musicHomeRandB.stop();
                }
                if (musicHomeOrchestra.isPlaying()) {
                    musicHomeOrchestra.stop();
                }
                if (musicHomeRock.isPlaying()) {
                    musicHomeRock.stop();
                }
                break;

            case "Up":
                loopDown = false;
                loopLeft = false;
                loopRight = false;
                loopUp = true;

                if(masterLoop) {
                    if (musicHomePop.isPlaying()) {
                        musicHomePop.stop();
                    } if (musicHomeRandB.isPlaying()) {
                        musicHomeRandB.stop();
                    } if (musicHomeOrchestra.isPlaying()) {
                        musicHomeOrchestra.stop();
                    } if (!musicHomeRock.isPlaying()) {
                        musicHomeRock.start();
                    }
                }

                new Thread( new Runnable(){
                    @Override
                    public void run(){
                        Looper.prepare();
                        //do work here
                        while(loopUp&&masterLoop){
                            int briValue =randValue.nextInt((maxBri-minBri)+1)+minBri;
                            int hueValue =randValue.nextInt((maxHue-minHue)+1)+minHue;
                            int satValue = randValue.nextInt((maxSat-minSat)+1)+minSat;

                            setHueColor(3,hueValue,satValue,briValue);

                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Log.d("AHOY", "LOOP UP");
                        }
                    }
                }).start();

                new Thread( new Runnable(){
                    @Override
                    public void run(){
                        Looper.prepare();
                        //do work here
                        while(loopUp&&masterLoop){
                            for(int i = 0; i<5; i++){
                                mipDrive(forwardMovement);
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int j = 0; j<100; j++) {
                                mipDrive(leftMovement);
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int i = 0; i<5; i++){
                                mipDrive(forwardMovement);
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int j = 0; j<50; j++) {
                                mipDrive(rightMovement);
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            for(int j = 0; j<100; j++) {
                                mipDrive(leftMovement);
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.d("AHOY", "LOOP UP2");
                        }
                    }
                }).start();


                break;

            case "Down":
                loopUp = false;
                loopLeft = false;
                loopRight = false;
                loopDown = true;
                new Thread( new Runnable(){
                    @Override
                    public void run(){
                        Looper.prepare();
                        //do work here
                        while(loopDown&&masterLoop){
                            int briValue =randValue.nextInt((maxBri-minBri)+1)+minBri;
                            int hueValue =randValue.nextInt((maxHue-minHue)+1)+minHue;
                            int satValue = randValue.nextInt((maxSat-minSat)+1)+minSat;

                            setHueColor(3,hueValue,satValue,briValue);

                            try {
                                Thread.sleep(4000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Log.d("AHOY", "LOOP DOWN");
                        }
                    }
                }).start();

                new Thread( new Runnable(){
                    @Override
                    public void run(){
                        Looper.prepare();
                        //do work here
                        while(loopDown&&masterLoop){
                            for(int i = 0; i<5; i++){
                                mipDrive(forwardMovement);
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int j = 0; j<100; j++) {
                                mipDrive(leftMovement);
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int i = 0; i<5; i++){
                                mipDrive(forwardMovement);
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int j = 0; j<50; j++) {
                                mipDrive(rightMovement);
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            for(int j = 0; j<100; j++) {
                                mipDrive(leftMovement);
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.d("AHOY", "LOOP DOWN2");
                        }
                    }
                }).start();


                if(masterLoop) {
                    if (musicHomePop.isPlaying()) {
                        musicHomePop.stop();
                    } if (musicHomeRock.isPlaying()) {
                        musicHomeRock.stop();
                    } if (musicHomeOrchestra.isPlaying()) {
                        musicHomeOrchestra.stop();
                    } if (!musicHomeRandB.isPlaying()) {
                        musicHomeRandB.start();
                    }
                }
                break;

            case "Left":
                loopUp = false;
                loopDown = false;
                loopRight = false;
                loopLeft = true;

                new Thread( new Runnable(){
                    @Override
                    public void run(){
                        Looper.prepare();
                        //do work here
                        while(loopLeft&&masterLoop){
                            int briValue =randValue.nextInt((maxBri-minBri)+1)+minBri;
                            int hueValue =randValue.nextInt((maxHue-minHue)+1)+minHue;
                            int satValue = randValue.nextInt((maxSat-minSat)+1)+minSat;

                            setHueColor(3,hueValue,satValue,briValue);

                            try {
                                Thread.sleep(8000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Log.d("AHOY", "LOOP UP");
                        }
                    }
                }).start();

                new Thread( new Runnable(){
                    @Override
                    public void run(){
                        Looper.prepare();
                        //do work here
                        while(loopLeft&&masterLoop){
                            for(int i = 0; i<5; i++){
                                mipDrive(forwardMovement);
                                try {
                                    Thread.sleep(400);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int j = 0; j<100; j++) {
                                mipDrive(leftMovement);
                                try {
                                    Thread.sleep(400);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int i = 0; i<5; i++){
                                mipDrive(forwardMovement);
                                try {
                                    Thread.sleep(400);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int j = 0; j<50; j++) {
                                mipDrive(rightMovement);
                                try {
                                    Thread.sleep(400);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            for(int j = 0; j<400; j++) {
                                mipDrive(leftMovement);
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.d("AHOY", "LOOP UP2");
                        }
                    }
                }).start();

                if(masterLoop) {
                    if (musicHomeRandB.isPlaying()) {
                        musicHomeRandB.stop();
                    } else if (musicHomeRock.isPlaying()) {
                        musicHomeRock.stop();
                    } else if (musicHomeOrchestra.isPlaying()) {
                        musicHomeOrchestra.stop();
                    } else if (!musicHomePop.isPlaying()) {
                        musicHomePop.start();
                    }
                }
                break;

            case "Right":
                loopUp = false;
                loopDown = false;
                loopLeft = false;
                loopRight = true;

                new Thread( new Runnable(){
                    @Override
                    public void run(){
                        Looper.prepare();
                        //do work here
                        while(loopRight&&masterLoop){
                            int briValue =randValue.nextInt((maxBri-minBri)+1)+minBri;
                            int hueValue =randValue.nextInt((maxHue-minHue)+1)+minHue;
                            int satValue = randValue.nextInt((maxSat-minSat)+1)+minSat;

                            setHueColor(3,hueValue,satValue,briValue);

                            try {
                                Thread.sleep(16000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();

                new Thread( new Runnable(){
                    @Override
                    public void run(){
                        Looper.prepare();
                        //do work here
                        while(loopUp&&masterLoop){
                            for(int i = 0; i<5; i++){
                                mipDrive(forwardMovement);
                                try {
                                    Thread.sleep(800);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int j = 0; j<100; j++) {
                                mipDrive(leftMovement);
                                try {
                                    Thread.sleep(800);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int i = 0; i<5; i++){
                                mipDrive(forwardMovement);
                                try {
                                    Thread.sleep(800);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            for(int j = 0; j<50; j++) {
                                mipDrive(rightMovement);
                                try {
                                    Thread.sleep(800);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            for(int j = 0; j<100; j++) {
                                mipDrive(leftMovement);
                                try {
                                    Thread.sleep(800);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }).start();

                if(masterLoop) {
                    if (musicHomePop.isPlaying()) {
                        musicHomePop.stop();
                    } else if (musicHomeRock.isPlaying()) {
                        musicHomeRock.stop();
                    } else if (musicHomeRandB.isPlaying()) {
                        musicHomeRandB.stop();
                    } else if (!musicHomeOrchestra.isPlaying()) {
                        musicHomeOrchestra.start();
                    }
                }
                break;

            default:
                break;
        }
    }

    class AsyncT extends AsyncTask<String, Void, Void> {
        Response response;
        String serverResponse;
        String[] splitServerResponse;
        Long requestTime;
        Long responseTime;
        Long latency;
        protected Void doInBackground(String... features) {

            try {
                //requestTime = System.currentTimeMillis();
                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/xml;ty=4");
                String openHeader = String.format("<om2m:cin xmlns:om2m=\"http://www.onem2m.org/xml/protocols\">\n    <cnf>message</cnf>\n    <con>\n      &lt;obj&gt;\n" +
                        "        &lt;str name=&quot;appId&quot; val=&quot;LAO&quot;/&gt;\n        &lt;str name=&quot;category&quot; val=&quot;Features &quot;/&gt;\n");
                String feature = "";
                for(int i=0; i<features.length; i=i+2){
                    feature = feature + String.format("&lt;int name=&quot;%s&quot; val=&quot;%s&quot;/&gt;\n",features[i],features[i+1]);
                }

                String closeHeader = String.format("      &lt;/obj&gt;\n    </con>\n</om2m:cin>");
                String completeRequest = openHeader + feature+ closeHeader;
                RequestBody body = RequestBody.create(mediaType, completeRequest);
                Request request = new Request.Builder()
                        .url("http://140.113.214.222:8080/~/in-cse/in-name/LAO/DATA")
                        .post(body)
                        .addHeader("x-m2m-origin", "admin:admin")
                        .addHeader("content-type", "application/xml;ty=4")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("postman-token", "c8db5cfe-baa8-c49c-bc91-1942cb2238cf")
                        .build();

                response = client.newCall(request).execute();
                //responseTime = System.currentTimeMillis();
                serverResponse = response.body().string();


            } catch (IOException e ) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(serverResponse!= null){
                splitServerResponse = serverResponse.split("</m2m:cin>\n");
                txtGesture.setText(splitServerResponse[1]);
                Log.d("CWTLAO","Server "+splitServerResponse[1]);
                //latency = responseTime - requestTime;
                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("CWTLAO","T Req: "+requestTime+" T Res: "+responseTime+" Latency: "+latency);
                        writeToSDCard(requestTime+","+responseTime+","+latency+"\n", "LAO Gesture Specific Latency");
                    }
                });*/

            }
        }
    }

    private void displayData(String data) {
        if (data != null) {
            uuidTv.setText("RSSI:" + data);
        }
    }

    private void startToReadData() {
        new Thread() {
            public void run() {
                try {
                    sleep(250);   // update every 500ms
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // we enable the raw data notification here
                mBluetoothLeService.enableMotionRawService();
            }

        }.start();
    }

    private void startReadRssi() {
        new Thread() {
            public void run() {
                while (flag) {
                    mBluetoothLeService.readRssi();
                    try {
                        sleep(1000);   // update every 500ms
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_main);

        musicHomeOn = MediaPlayer.create(this,R.raw.homeon);
        musicHomeOff = MediaPlayer.create(this,R.raw.homeoff);

        musicHomeRock = MediaPlayer.create(this,R.raw.rock);
        musicHomeRandB = MediaPlayer.create(this,R.raw.randb);
        musicHomePop = MediaPlayer.create(this,R.raw.pop);
        musicHomeOrchestra = MediaPlayer.create(this,R.raw.mozart);

        koalaFeature = new FeatureExtraction();
        koalaGesture = new GestureRecognition();
        // --- MIP Robot --- //
        forward = (Button) findViewById(R.id.btnForward);
        backward = (Button) findViewById(R.id.btnBackward);
        counterClockwise = (Button) findViewById(R.id.btnCCW2);
        clockwise =	(Button) findViewById(R.id.btnCW2);
        final BluetoothManager bluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Set BluetoothAdapter to MipRobotFinder
        MipRobotFinder.getInstance().setBluetoothAdapter(mBluetoothAdapter);

        // Set Context to MipRobotFinder
        MipRobotFinder finder = MipRobotFinder.getInstance();
        finder.setApplicationContext(getApplicationContext());

        homeMusic = MediaPlayer.create(this,R.raw.rock);
        forward.setOnTouchListener(new Button.OnTouchListener() {
            @Override

            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("AHOY","EKSEKUSI");
                    if (!homeMusic.isPlaying()) {
                        homeMusic.start();
                    }
                    for(int i = 0; i<40; i++){
                        mipDrive(forwardMovement);
                    }
                    for(int j = 0; j<400; j++) {
                        mipDrive(leftMovement);
                    }
                    for(int k = 0; k<40; k++){
                        mipDrive(forwardMovement);
                    }
                    for(int l = 0; l<400; l++) {
                        mipDrive(leftMovement);
                    }
                    for(int m = 0; m<40; m++){
                        mipDrive(forwardMovement);
                    }
                    for(int n = 0; n<400; n++) {
                        mipDrive(leftMovement);
                    }
                    for(int o = 0; o<40; o++){
                        mipDrive(forwardMovement);
                    }
                    for(int p = 0; p<400; p++) {
                        mipDrive(leftMovement);
                    }

                }
                return false;
            }
        });

        backward.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                Log.d("AHOY","EKSEKUSI");
                mipDrive(leftMovement);
                return false;
            }
        });

        counterClockwise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mipDrive(CCWMovement);
            }
        });

        clockwise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mipDrive(CWMovement);
            }
        });

        // --- END --- //
        counter = 0;
        excel = new File("/sdcard/LAG.csv");
        if (!excel.exists()) {
            //if there is no this file, create one.
            try {
                excel.createNewFile();
                Log.d(TAG, "creating file succeed");
            } catch (IOException e) {
                Log.e(TAG, "creating file fail");
            }
        }

        // grab the UI component
        uuidTv = (TextView) findViewById(R.id.uuid);
        lastUuid = (TextView) findViewById(R.id.lastDevice);
        rawDataText = (TextView) findViewById(R.id.rawdata);
        scanAllBtn = (Button) findViewById(R.id.ScanAll);

        buttonUp = (Button) findViewById(R.id.btnUp);
        buttonDown = (Button) findViewById(R.id.btnDown);
        buttonRight = (Button) findViewById(R.id.btnRight);
        buttonLeft = (Button) findViewById(R.id.btnLeft);
        buttonClockwise = (Button) findViewById(R.id.btnCW);
        buttonCounterClockwise = (Button) findViewById(R.id.btnCCW);
        buttonVLeftRight = (Button) findViewById(R.id.btnVLR);
        buttonVRightLeft = (Button) findViewById(R.id.btnVRL);
        buttonCrossCW = (Button) findViewById(R.id.btnCrossCW);
        buttonCrossCCW = (Button) findViewById(R.id.btnCrossCCW);

        modeSwitch = (Switch) findViewById(R.id.modeSwitch);
        layTrainingButton = (LinearLayout) findViewById(R.id.layTrainingButton);
        txtMode = (TextView) findViewById(R.id.txtMode);
        txtGesture = (TextView) findViewById(R.id.tvGesture);
        modeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    layTrainingButton.setVisibility(View.VISIBLE);
                    txtMode.setText("Training");
                    storeDataFlag = true;
                } else {
                    layTrainingButton.setVisibility(View.GONE);
                    txtMode.setText("Testing");
                    storeDataFlag = false;
                }
            }
        });

        //check the current state before we display the screen
        if (modeSwitch.isChecked()) {
            layTrainingButton.setVisibility(View.VISIBLE);
            txtMode.setText("Training");
            storeDataFlag = true;
        } else {
            layTrainingButton.setVisibility(View.GONE);
            txtMode.setText("Testing");
            storeDataFlag = false;
        }

        layDebugMIP = (LinearLayout) findViewById(R.id.layDebugMIP);
        Switch debugMIPSwitch = (Switch) findViewById(R.id.debugMIPSwitch);

        debugMIPSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    layDebugMIP.setVisibility(View.VISIBLE);
                } else {
                    layDebugMIP.setVisibility(View.GONE);
                }
            }
        });

        //check the current state before we display the screen
        if (modeSwitch.isChecked()) {
            layDebugMIP.setVisibility(View.VISIBLE);
        } else {
            layDebugMIP.setVisibility(View.GONE);
        }

        final LinearLayout layDebugHue = (LinearLayout) findViewById(R.id.layDebugHue);
        Switch switchDebugHue = (Switch) findViewById(R.id.switchDebugHue);

        switchDebugHue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    layDebugHue.setVisibility(View.VISIBLE);
                } else {
                    layDebugHue.setVisibility(View.GONE);
                }
            }
        });

        //check the current state before we display the screen
        if (switchDebugHue.isChecked()) {
            layDebugHue.setVisibility(View.VISIBLE);
        } else {
            layDebugHue.setVisibility(View.GONE);
        }

        final LinearLayout layDebugMusic = (LinearLayout) findViewById(R.id.layDebugMusic);
        Switch switchDebugMusic = (Switch) findViewById(R.id.switchDebugMusic);

        switchDebugMusic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    layDebugMusic.setVisibility(View.VISIBLE);
                } else {
                    layDebugMusic.setVisibility(View.GONE);
                }
            }
        });

        //check the current state before we display the screen
        if (switchDebugMusic.isChecked()) {
            layDebugMusic.setVisibility(View.VISIBLE);
        } else {
            layDebugMusic.setVisibility(View.GONE);
        }

        Button btnPlayMusic = (Button) findViewById(R.id.btnPlayMusic);
        Button btnStopMusic = (Button) findViewById(R.id.btnStopMusic);

        homeMusic = MediaPlayer.create(this,R.raw.rock);
        btnPlayMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!homeMusic.isPlaying()) {
                    homeMusic.start();
                }
            }
        });
        btnStopMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (homeMusic.isPlaying()) {
                    homeMusic.stop();
                }
            }
        });

        phHueSDK = PHHueSDK.create();
        setHueOn(1,false);
        setHueOn(2,false);
        setHueOn(3,false);
        Button btnRandHue1 = (Button) findViewById(R.id.btnRandHue1);
        btnRandHue1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int maxBri = 254;
                final int minBri = 0;
                final int maxHue = 360;
                final int minHue = 0;
                final int maxSat = 254;
                final int minSat = 0;
                final Random randValue = new Random();
                int briValue =randValue.nextInt((maxBri-minBri)+1)+minBri;
                int hueValue =randValue.nextInt((maxHue-minHue)+1)+minHue;
                int satValue = randValue.nextInt((maxSat-minSat)+1)+minSat;

                setHueColor(1,hueValue,satValue,briValue);

            }
        });
        Button btnRandHue2 = (Button) findViewById(R.id.btnRandHue2);
        btnRandHue2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int maxBri = 254;
                final int minBri = 0;
                final int maxHue = 360;
                final int minHue = 0;
                final int maxSat = 254;
                final int minSat = 0;
                final Random randValue = new Random();
                int briValue =randValue.nextInt((maxBri-minBri)+1)+minBri;
                int hueValue =randValue.nextInt((maxHue-minHue)+1)+minHue;
                int satValue = randValue.nextInt((maxSat-minSat)+1)+minSat;

                setHueColor(2,hueValue,satValue,briValue);
            }
        });

        Button btnRandHue3 = (Button) findViewById(R.id.btnRandHue3);
        btnRandHue3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int maxBri = 254;
                final int minBri = 0;
                final int maxHue = 360;
                final int minHue = 0;
                final int maxSat = 254;
                final int minSat = 0;
                final Random randValue = new Random();
                int briValue =randValue.nextInt((maxBri-minBri)+1)+minBri;
                int hueValue =randValue.nextInt((maxHue-minHue)+1)+minHue;
                int satValue = randValue.nextInt((maxSat-minSat)+1)+minSat;

                setHueColor(3,hueValue,satValue,briValue);
            }
        });

        Jancuk = (Button) findViewById(R.id.btnTest);
        Jancuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int a=0; a<200; a++){
                    AsyncT sendDataToServer = new AsyncT();
                    sendDataToServer.execute(featureLabel[0],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",featureLabel[1],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",
                            featureLabel[2],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",featureLabel[3],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",
                            featureLabel[4],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",featureLabel[5],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",
                            featureLabel[6],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",featureLabel[7],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",
                            featureLabel[8],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",featureLabel[9],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",
                            featureLabel[10],"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0",
                            featureLabel[11],"1.0,2.0,3.0,4.0,5.0,6.0",featureLabel[12],"1.0,2.0,3.0,4.0,5.0,6.0");
                }



                //Log.d("AHOY","1"+Arrays.toString(ale));
                //Log.d("AHOY","2"+Arrays.toString(ale2));
                //setHueColor(2,85,100,254);
            }
        });



        scanAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!connState) {
                    // mDevice = new ArrayList<TNGDevices>();

                    mDevice.clear();
                    // Start to scan the ble device
                    scanLeDevice2();

                    try {
                        Thread.sleep(SCAN_PERIOD);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    // Go to the list view
                    Intent intent2 = new Intent(getApplicationContext(),
                            DevicesListActivity.class);
                    startActivityForResult(intent2, REQUEST_CODE);
                } else {
                    mBluetoothLeService.disconnect();
                    mBluetoothLeService.close();
                    scanAllBtn.setText("Scan All");
                    uuidTv.setText("");
                    rawDataText.setText("");
                    lastUuid.setText("");
                    connState = false;
                }
            }
        });

        buttonUp.setOnTouchListener(new Button.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press buttonUp");
                    motionType = "Up";

                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release buttonUp");
                    motionType = "";

                }
                return false;
            }
        });

        buttonDown.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press buttonDown");
                    motionType = "Down";

                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release buttonDown");
                    motionType = "";
                }
                return false;
            }
        });

        buttonRight.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d(TAG, "press buttonRight");
                    motionType = "Right";
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release buttonRight");
                    motionType = "";
                }
                return false;
            }
        });

        buttonLeft.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press buttonLeft");
                    motionType = "Left";
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release buttonLeft");
                    motionType = "";
                }
                return false;
            }
        });

        buttonVRightLeft.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press V buttonRight-to-buttonLeft");
                    motionType = "VRL";
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release buttonRight-to-buttonLeft");
                    motionType = "";
                }
                return false;
            }
        });

        buttonVLeftRight.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press V buttonLeft-to-buttonRight");
                    motionType = "VLR";
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release buttonLeft-to-buttonRight");
                    motionType = "";
                }
                return false;
            }
        });

        buttonClockwise.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press buttonClockwise");
                    motionType = "Clockwise";
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release buttonClockwise");
                    motionType = "";
                }
                return false;
            }
        });

        buttonCounterClockwise.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press Counter buttonClockwise");
                    motionType = "CounterClockwise";
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release Counter buttonClockwise");
                    motionType = "";
                }
                return false;
            }
        });

        buttonCrossCW.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press Cross buttonClockwise");
                    motionType = "CrossClockwise";
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release Cross buttonClockwise");
                    motionType = "";
                }
                return false;
            }
        });

        buttonCrossCCW.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    //press
                    Log.d(TAG, "press Cross Counter buttonClockwise");
                    motionType = "CrossCounterClockwise";
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    //release
                    Log.d(TAG, "release Cross Counter buttonClockwise");
                    motionType = "";
                }
                return false;
            }
        });

        Log.i(TAG, "getPackageManager");

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        final BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter2 = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter2 == null) {
            Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        Intent gattServiceIntent = new Intent(MainActivity.this, TNGService.class);

        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

    }



    @Override
    protected void onResume() {
        super.onResume();

        if (!mBluetoothAdapter2.isEnabled()) {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

        // ---- MIP Robot ---- //
        this.registerReceiver(mMipFinderBroadcastReceiver, MipRobotFinder.getMipRobotFinderIntentFilter());
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                TextView noBtText = (TextView)this.findViewById(R.id.no_bt_text);
                noBtText.setVisibility(View.VISIBLE);
            }
        }

        // Search for mip
        MipRobotFinder.getInstance().clearFoundMipList();
        scanLeDevice(false);
//		updateMipList();
        scanLeDevice(true);
        // --- END ---- //
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(TNGService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(TNGService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(TNGService.ACTION_GATT_RSSI);
        intentFilter.addAction(TNGService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(TNGService.ACTION_DATA_AVAILABLE);

        return intentFilter;
    }

    private ArrayList<TNGDevices> reduceDeviceForConnection() {
        ArrayList<TNGDevices> devs = mDevice;
        if (devs.size() <= 0)
            return devs;

        ArrayList<TNGDevices> newArray = new ArrayList<TNGDevices>();
        for (int i = 0, size = devs.size(); i < size; i++) {
            if (newArray.size() == 0) {
                newArray.add(devs.get(i));
            } else {
                for (int j = 0, size_j = newArray.size(); j < size_j; j++) {
                    if (newArray.get(j).getDevice().getAddress().equals(devs.get(i).getDevice().getAddress())) {
                        newArray.get(j).setRssi(newArray.get(j).getRssi() + devs.get(i).getRssi() / 2);
                    } else {
                        newArray.add(devs.get(i));
                    }
                }
            }
        }
        return newArray;
    }

    private void scanLeDevice2() {
        new Thread() {

            @Override
            public void run() {
                mBluetoothAdapter2.startLeScan(mLeScanCallback);

                try {
                    Thread.sleep(SCAN_PERIOD);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                mBluetoothAdapter2.stopLeScan(mLeScanCallback);
            }
        }.start();

        mDevice = this.reduceDeviceForConnection();
    }

    /**
     * The event callback function to handle the found of near le devices
     *
     * @param address
     * The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The
     * connection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,
                             final byte[] scanRecord) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (device != null) {
                        TNGDevices p = new TNGDevices(device, rssi, scanRecord);
                        mDevice.add(p);

                        /*
                        String addr = p.getDevice().getAddress();

                        }*/
                    }
                }
            });
        }
    };

    @Override
    protected void onStop() {
        super.onStop();

        flag = false;

        unregisterReceiver(mGattUpdateReceiver);

        this.unregisterReceiver(mMipFinderBroadcastReceiver);
        for(MipRobot mip : MipRobotFinder.getInstance().getMipsConnected()) {
            mip.readMipHardwareVersion();
            mip.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mServiceConnection != null)
            unbindService(mServiceConnection);

        System.exit(0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT
                && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        } else if (requestCode == REQUEST_CODE
                && resultCode == DevicesListActivity.RESULT_CODE) {
            mDeviceAddress = data.getStringExtra(DevicesListActivity.EXTRA_DEVICE_ADDRESS);
            mDeviceName = data.getStringExtra(DevicesListActivity.EXTRA_DEVICE_NAME);
            mBluetoothLeService.connect(mDeviceAddress);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void mipDrive(float[] vector) {
        List<MipRobot> mips = MipRobotFinder.getInstance().getMipsConnected();

        for (MipRobot mip : mips) {
            mip.mipDrive(vector);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/buttonUp button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void writeToSDCard(String inputString,String fileName) {
        try {
            writeExcel = new FileWriter(Environment.getExternalStorageDirectory().getPath()+String.format(Locale.ENGLISH,"/%s.csv",fileName), true);

            writeExcel.append(inputString);
            writeExcel.flush();
            writeExcel.close();
            Log.d(TAG, "Writing file Succeed");
        } catch (IOException error) {
            Log.e(TAG, "Writing file Fail");
        }
        //s = "";
        //counter = 0;
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            MipRobotFinder.getInstance().scanForMips();
        } else {
            MipRobotFinder.getInstance().stopScanForMips();
        }
    }

    public void updateMipList()
    {
        //connect to first found mip
        List<MipRobot> mipFoundList = MipRobotFinder.getInstance().getMipsFoundList();
        for(MipRobot mipRobot : mipFoundList) {
            connectToMip(mipRobot);
            break;
        }
    }

    private void connectToMip(final MipRobot mip) {
        Log.d("AHOY","CONNECT");
        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mip.setCallbackInterface(MainActivity.this);
                mip.connect(MainActivity.this.getApplicationContext());
                TextView connectionView = (TextView)MainActivity.this.findViewById(R.id.connect_text);
                connectionView.setText("Connecting: "+mip.getName());
            }
        });

    }

    //	@Override
    public void mipDeviceReady(MipRobot sender) {
        Log.d("AHOY","READY");
        final MipRobot robot = sender;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView connectionView = (TextView)MainActivity.this.findViewById(R.id.connect_text);
                connectionView.setText("Connected: "+robot.getName());
            }
        });

    }


    @Override
    public void mipDeviceDisconnected(MipRobot mipRobot) {
        final MipRobot cwt = mipRobot;
        Log.d("AHOY","DISCONNECTED");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView connectionView = (TextView)MainActivity.this.findViewById(R.id.connect_text);
                connectionView.setText("Disconnected: "+cwt.getName());
            }
        });
    }

    @Override
    public void mipRobotDidReceiveBatteryLevelReading(MipRobot mipRobot, int i) {

    }

    @Override
    public void mipRobotDidReceivePosition(MipRobot mipRobot, byte b) {

    }

    @Override
    public void mipRobotDidReceiveHardwareVersion(int i, int i1) {

    }

    @Override
    public void mipRobotDidReceiveSoftwareVersion(Date date, int i) {

    }

    @Override
    public void mipRobotDidReceiveVolumeLevel(int i) {

    }

    @Override
    public void mipRobotDidReceiveIRCommand(ArrayList<Byte> arrayList, int i) {

    }

    @Override
    public void mipRobotDidReceiveWeightReading(byte value,
                                                boolean leaningForward) {
        final String weightLevel = "level " + value + "!";
        updateWeightButtonLable(weightLevel);
        // TODO Auto-generated method stub

    }

    @Override
    public void mipRobotIsCurrentlyInBootloader(MipRobot mipRobot, boolean b) {

    }

    private final BroadcastReceiver mMipFinderBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (MipRobotFinder.MipRobotFinder_MipFound.equals(action)) {
                // Connect to mip
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        List<MipRobot> mipFoundList = MipRobotFinder.getInstance().getMipsFoundList();
                        if (mipFoundList != null && mipFoundList.size() > 0){
                            MipRobot selectedMipRobot = mipFoundList.get(0);
                            if (selectedMipRobot != null){
                                connectToMip(selectedMipRobot);
                            }
                        }
                    }
                }, 1);
            }
        }
    };

    private final void updateWeightButtonLable(final String string)
    {
        try {
            runOnUiThread(new Runnable() {
                final	Button button = (Button) findViewById(R.id.weight);
                @Override
                public void run() {

                    Log.d("MainMenuActivity", "mipRobotDidReceiveWeightReading " + string);

                    button.setText(string);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable(){
                        @Override
                        public void run(){
                            if(button.getText().toString().compareTo("Weight Level") !=0)
                            {
                                updateWeightButtonLable("Weight Level");
                            }
                        }
                    }, 5);
                }
            });
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String putCommaInBetween(double[] input){
        int n = input.length;
        String output = Double.toString(input[0]);
        for(int i=1 ; i < n; i++){
            output = output + "," + Double.toString(input[i]);
        }
        return output;
    }

    private int countGestureLabel(String[] input){
        int n = input.length;
        int gestureLabelNumber =0;
        String[][] splitInput = new String[n][];

        for(int j=0; j < n; j++){
            splitInput[j] = input[j].split(",");
        }

        for(int i=0; i <n; i++){
            if(splitInput[i][0].equals("Gesture")){
                gestureLabelNumber++;
            }
        }
        return gestureLabelNumber;
    }

    private String getDominantGestureLabel(String[] input){
        String dominantGestureLabel = "";
        int n = input.length;
        String[][] splitInput = new String[n][];

        int[] gestureRecapt = new int[11];

        for(int j=0; j < n; j++){
            splitInput[j] = input[j].split(",");
        }

        for(int i =0; i < n; i++){
            switch(splitInput[i][1]){
                case "Up":
                    gestureRecapt[0]++;
                    break;
                case "Down":
                    gestureRecapt[1]++;
                    break;
                case "Left":
                    gestureRecapt[2]++;
                    break;
                case "Right":
                    gestureRecapt[3]++;
                    break;
                case "Clockwise":
                    gestureRecapt[4]++;
                    break;
                case "CounterClockwise":
                    gestureRecapt[5]++;
                    break;
                case "CrossClockwise":
                    gestureRecapt[6]++;
                    break;
                case "CrossCounterClockwise":
                    gestureRecapt[7]++;
                    break;
                case "VLR":
                    gestureRecapt[8]++;
                    break;
                case "VRL":
                    gestureRecapt[9]++;
                    break;
                case "NonGesture":
                    gestureRecapt[10]++;
                    break;
            }
        }
        int max =0;

        for (int counter = 0; counter < gestureRecapt.length; counter++)
        {
            if (gestureRecapt[counter] > max)
            {
                max = gestureRecapt[counter];
            }
        }

        if(gestureRecapt[0]==max){
            dominantGestureLabel = "Up";
        } else if(gestureRecapt[1]==max){
            dominantGestureLabel = "Down";
        } else if(gestureRecapt[2]==max){
            dominantGestureLabel = "Left";
        } else if(gestureRecapt[3]==max){
            dominantGestureLabel = "Right";
        } else if(gestureRecapt[4]==max){
            dominantGestureLabel = "Clockwise";
        } else if(gestureRecapt[5]==max){
            dominantGestureLabel = "CounterClockwise";
        } else if(gestureRecapt[6]==max){
            dominantGestureLabel = "CrossClockwise";
        } else if(gestureRecapt[7]==max){
            dominantGestureLabel = "CrossCounterClockwise";
        } else if(gestureRecapt[8]==max){
            dominantGestureLabel = "VLR";
        } else if(gestureRecapt[9]==max){
            dominantGestureLabel = "VRL";
        } else if(gestureRecapt[10]==max){
            dominantGestureLabel = "NonGesture";
        }
        //Log.d("AHOY","Biggest number: " + max);

        // Get the biggest values' variable
        //if(up > down)
        //Log.d("AHOY",Integer.toString(Math.max(up,down)));

        return dominantGestureLabel;
    }

    public void setHueColor(int selectLight, int hueIndex, int satIndex, int brightness) {
        PHBridge bridge = phHueSDK.getSelectedBridge();
        List<PHLight> allLights = bridge.getResourceCache().getAllLights();

        for (PHLight light : allLights) {
            PHLightState lightState = new PHLightState();
            // To validate your lightstate is valid (before sending to the bridge) you can use:
            // String validState = lightState.validateState();
            if (light.getIdentifier().equals(Integer.toString(selectLight))) {
                hueIndex = (int) (182.041 * hueIndex); //from 65535 divided by 360
                satIndex = (int) (2.54 * satIndex);    //from 254 divided by 100
                lightState.setHue(hueIndex); //Hue
                lightState.setSaturation(satIndex); // Saturation 0 to 254
                lightState.setBrightness(brightness); //brightness should be a value between 0 and 254
                bridge.updateLightState(light, lightState, listener);
            }
        }
    }

    public void setHueOn(int selectLight, boolean lightStatus) {
        PHBridge bridge = phHueSDK.getSelectedBridge();
        List<PHLight> allLights = bridge.getResourceCache().getAllLights();

        for (PHLight light : allLights) {
            PHLightState lightState = new PHLightState();
            // To validate your lightstate is valid (before sending to the bridge) you can use:
            // String validState = lightState.validateState();
            if (light.getIdentifier().equals(Integer.toString(selectLight))) {
                lightState.setOn(lightStatus);
                bridge.updateLightState(light, lightState, listener);
            }
        }
    }

    // If you want to handle the response from the bridge, create a PHLightListener object.

    PHLightListener listener = new PHLightListener() {

        @Override
        public void onSuccess() {
        }

        @Override
        public void onStateUpdate(Map<String, String> arg0, List<PHHueError> arg1) {
            Log.w(TAG, "Light has updated");
        }

        @Override
        public void onError(int arg0, String arg1) {
        }

        @Override
        public void onReceivingLightDetails(PHLight arg0) {
        }

        @Override
        public void onReceivingLights(List<PHBridgeResource> arg0) {
        }

        @Override
        public void onSearchComplete() {
        }
    };

}
